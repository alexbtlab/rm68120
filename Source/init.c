#include "init.h"

extern DMA_InitTypeDef  DMA_InitStructure;

TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
TIM_OCInitTypeDef  TIM_OCInitStructure;

uint16_t PrescalerValue = 0;
uint16_t CCR1_Val = 100;
uint16_t CCR2_Val = 249;
uint16_t CCR3_Val = 166;
uint16_t CCR4_Val = 83;

void PORT_init(void){
	
//	GPIO_InitTypeDef 	GPIO_InitStructLED;
	GPIO_InitTypeDef 	GPIO_InitStructEXTIPE1;
	GPIO_InitTypeDef 	GPIO_InitStructLampCtrl; 
	GPIO_InitTypeDef      GPIO_RTC;
	
//	GPIO_InitTypeDef 	GPIO_InitStructPA3_BL;
//	GPIO_InitTypeDef 	GPIO_InitStructPA7_SDI;

//	GPIO_InitTypeDef 	GPIO_InitStructPC13_RST;
//	GPIO_InitTypeDef 	GPIO_InitStructPC14_DC;			//RS - pin
//	GPIO_InitTypeDef 	GPIO_InitStructPA5_WR_SCK;
//	GPIO_InitTypeDef 	GPIO_InitStructPE6_RD;
//	
//		GPIO_InitTypeDef 	GPIO_InitStructPA1_CS;
//	GPIO_InitTypeDef 	GPIO_InitStructPF0;
////	GPIO_InitTypeDef 	GPIO_InitStructPA8_MCO;

//	
//	// ������ ����� �� ���� 
	RCC_AHB1PeriphClockCmd (RCC_AHB1Periph_GPIOA, ENABLE);

//  RCC_AHB1PeriphClockCmd (RCC_AHB1Periph_GPIOC, ENABLE);
//	RCC_AHB1PeriphClockCmd (RCC_AHB1Periph_GPIOD, ENABLE);
	RCC_AHB1PeriphClockCmd (RCC_AHB1Periph_GPIOE, ENABLE);
	RCC_AHB1PeriphClockCmd (RCC_AHB1Periph_GPIOB, ENABLE);
//	RCC_AHB1PeriphClockCmd (RCC_AHB1Periph_GPIOF, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG,ENABLE);	
	


  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);  

  /* Configure RTC  pin as output ******************************/
  GPIO_RTC.GPIO_Pin = GPIO_Pin_13;
  GPIO_RTC.GPIO_Mode = GPIO_Mode_AF;
  GPIO_RTC.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOC, &GPIO_RTC);
		
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource13, GPIO_AF_RTC_50Hz);
	/*	EXTI-line PF4 pin config 	*/
	
	GPIO_StructInit(&GPIO_InitStructEXTIPE1);
	GPIO_InitStructEXTIPE1.GPIO_Pin 			= GPIO_Pin_0;														
	GPIO_InitStructEXTIPE1.GPIO_Speed 		= GPIO_Speed_50MHz;										
	GPIO_InitStructEXTIPE1.GPIO_Mode 			= GPIO_Mode_OUT;
	GPIO_InitStructEXTIPE1.GPIO_OType 		=	GPIO_OType_PP;
	GPIO_InitStructEXTIPE1.GPIO_PuPd 			= GPIO_PuPd_UP;
	
	GPIO_Init(GPIOA, &GPIO_InitStructEXTIPE1);


	GPIO_StructInit(&GPIO_InitStructLampCtrl);
	GPIO_InitStructLampCtrl.GPIO_Pin 			= GPIO_Pin_14 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13;														
	GPIO_InitStructLampCtrl.GPIO_Speed 		= GPIO_Speed_50MHz;										
	GPIO_InitStructLampCtrl.GPIO_Mode 			= GPIO_Mode_OUT;
	GPIO_InitStructLampCtrl.GPIO_OType 		=	GPIO_OType_PP;
	GPIO_InitStructLampCtrl.GPIO_PuPd 			= GPIO_PuPd_DOWN;
	
	GPIO_Init(GPIOB, &GPIO_InitStructLampCtrl);


//	/*	EXTI-line PF4 pin config 	*/
//	GPIO_InitStructPF0.GPIO_Pin 			= GPIO_Pin_0;														
//	GPIO_InitStructPF0.GPIO_Speed 		= GPIO_Speed_50MHz;										
//	GPIO_InitStructPF0.GPIO_Mode 			= GPIO_Mode_OUT;												
//	GPIO_Init(GPIOF, &GPIO_InitStructPF0);
//	
//	/*	LED pin config	*/
//	GPIO_InitStructLED.GPIO_Pin 					= GPIO_Pin_8|GPIO_Pin_9|GPIO_Pin_10|GPIO_Pin_11|GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15;		// ����� ����� �����
//	GPIO_InitStructLED.GPIO_Speed					= GPIO_Speed_50MHz;												
//	GPIO_InitStructLED.GPIO_Mode					= GPIO_Mode_OUT;	
//  GPIO_InitStructLED.GPIO_OType 				= GPIO_OType_PP;
//  GPIO_InitStructLED.GPIO_PuPd  				= GPIO_PuPd_DOWN;	
//	GPIO_Init(GPIOE, &GPIO_InitStructLED);
//	
//	/*	BL pin config - ���������� ���������� ������� ��� LCD	*/
//	GPIO_InitStructPA3_BL.GPIO_Pin 				= GPIO_Pin_3;		
//	GPIO_InitStructPA3_BL.GPIO_Speed			= GPIO_Speed_50MHz;												
//	GPIO_InitStructPA3_BL.GPIO_Mode 			= GPIO_Mode_OUT;	
//  GPIO_InitStructPA3_BL.GPIO_OType 			= GPIO_OType_PP;
//  GPIO_InitStructPA3_BL.GPIO_PuPd  			= GPIO_PuPd_DOWN;	
//	GPIO_Init(GPIOA, &GPIO_InitStructPA3_BL);
//	
//	/*	WR_SCK pin config	- SCK Serial interface */
//	GPIO_InitStructPA5_WR_SCK.GPIO_Pin 		= GPIO_Pin_5;		
//	GPIO_InitStructPA5_WR_SCK.GPIO_Speed	= GPIO_Speed_50MHz;												
//	GPIO_InitStructPA5_WR_SCK.GPIO_Mode 	= GPIO_Mode_OUT;	
//  GPIO_InitStructPA5_WR_SCK.GPIO_OType 	= GPIO_OType_PP;
////  GPIO_InitStructPC15_WR_SCK.GPIO_PuPd  = GPIO_PuPd_DOWN;	
//	GPIO_Init(GPIOA, &GPIO_InitStructPA5_WR_SCK);
//	
//	/*	SDI pin config	*/
//	GPIO_InitStructPA7_SDI.GPIO_Pin 			= GPIO_Pin_7;	
//	GPIO_InitStructPA7_SDI.GPIO_Speed			= GPIO_Speed_50MHz;												
//	GPIO_InitStructPA7_SDI.GPIO_Mode 			= GPIO_Mode_OUT;
//  GPIO_InitStructPA7_SDI.GPIO_OType 		= GPIO_OType_PP;
////  GPIO_InitStructPA7_SDI.GPIO_PuPd  		= GPIO_PuPd_DOWN;	
//	GPIO_Init(GPIOA,																																 &GPIO_InitStructPA7_SDI);
//	

//	
//	/*	RST pin config	*/
//	GPIO_InitStructPC13_RST.GPIO_Pin 			= GPIO_Pin_13;	
//	GPIO_InitStructPC13_RST.GPIO_Speed		= GPIO_Speed_50MHz;												
//	GPIO_InitStructPC13_RST.GPIO_Mode 		= GPIO_Mode_OUT;	
//  GPIO_InitStructPC13_RST.GPIO_OType 		= GPIO_OType_PP;
//  GPIO_InitStructPC13_RST.GPIO_PuPd  		= GPIO_PuPd_DOWN;	
//	GPIO_Init(GPIOC, &GPIO_InitStructPC13_RST);
//	
//	/*	RS pin config	- LOW->Command HIGH->Display Data*/
//	GPIO_InitStructPC14_DC.GPIO_Pin 			= GPIO_Pin_14;		
//	GPIO_InitStructPC14_DC.GPIO_Speed			= GPIO_Speed_50MHz;												
//	GPIO_InitStructPC14_DC.GPIO_Mode 			= GPIO_Mode_OUT;	
//  GPIO_InitStructPC14_DC.GPIO_OType 		= GPIO_OType_PP;
//  GPIO_InitStructPC14_DC.GPIO_PuPd  		= GPIO_PuPd_DOWN;	
//	GPIO_Init(GPIOC, &GPIO_InitStructPC14_DC);
//	

//	/*	RD pin config	- LCD station read enable. �Low� active.*/
//	GPIO_InitStructPE6_RD.GPIO_Pin 				= GPIO_Pin_6;		
//	GPIO_InitStructPE6_RD.GPIO_Speed			= GPIO_Speed_50MHz;												
//	GPIO_InitStructPE6_RD.GPIO_Mode 			= GPIO_Mode_OUT;
//  GPIO_InitStructPE6_RD.GPIO_OType 			= GPIO_OType_PP;
//  GPIO_InitStructPE6_RD.GPIO_PuPd  			= GPIO_PuPd_DOWN;	
//	GPIO_Init(GPIOE, &GPIO_InitStructPE6_RD);
//	
//	/*	CS pin config	- Chip select. �Low� active. */
//	GPIO_InitStructPA1_CS.GPIO_Pin 				= GPIO_Pin_1;		
//	GPIO_InitStructPA1_CS.GPIO_Speed			= GPIO_Speed_50MHz;												
//	GPIO_InitStructPA1_CS.GPIO_Mode 			= GPIO_Mode_OUT;
//  GPIO_InitStructPA1_CS.GPIO_OType 			= GPIO_OType_PP;
//  GPIO_InitStructPA1_CS.GPIO_PuPd 			= GPIO_PuPd_DOWN;	
//	GPIO_Init(GPIOA, &GPIO_InitStructPA1_CS);
//	
//	//GPIO_PinAFConfig(GPIOA, GPIO_PinSource8,  GPIO_AF_0);	 									//�������������� ���-�� ����� PA8 - ���
//	
//	//GPIO_InitStructPA8_MCO.GPIO_Pin 	= GPIO_Pin_8;
//	//GPIO_InitStructPA8_MCO.GPIO_Speed = GPIO_Speed_50MHz;
//	//GPIO_InitStructPA8_MCO.GPIO_Mode 	= GPIO_Mode_AF;
//	//GPIO_Init(GPIOA, &GPIO_InitStructPA8_MCO);															// ������������� ����� MCO
// 
//	//RCC_MCOConfig(RCC_MCOSource_PLLCLK_Div2);																//����� PLLCLK_Div2 �� MCO
}
void EXTI_PF4_init(void){

	EXTI_InitTypeDef	EXTI_InitPORTA0;
	EXTI_DeInit();
	EXTI_StructInit(&EXTI_InitPORTA0);
	
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);
	
	EXTI_InitPORTA0.EXTI_Line	=	EXTI_Line0;
	EXTI_InitPORTA0.EXTI_LineCmd	=	ENABLE;
	EXTI_InitPORTA0.EXTI_Mode	=	EXTI_Mode_Interrupt;
	EXTI_InitPORTA0.EXTI_Trigger	=	EXTI_Trigger_Falling;
	
	EXTI_Init(&EXTI_InitPORTA0);
	NVIC_EnableIRQ(EXTI0_IRQn);
//__enable_irq();
	                                                                                               
	EXTI_GenerateSWInterrupt(EXTI_Line0);
}
void SysTick_init(void){
	
	SysTick->LOAD = TimerTick;		// �������� ��������
	SysTick->VAL  = TimerTick;		// �������� ������� � �����. �������, �������? 
 
	SysTick->CTRL=	SysTick_CTRL_CLKSOURCE_Msk |
									SysTick_CTRL_TICKINT_Msk   |
									SysTick_CTRL_ENABLE_Msk;
}
void init_LED(void){
	
	GPIO_InitTypeDef GPIO_PD12_LED;
	GPIO_InitTypeDef GPIO_PA0_EXT;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	
	GPIO_StructInit(&GPIO_PD12_LED);
	
	GPIO_PD12_LED.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_PD12_LED.GPIO_OType = GPIO_OType_PP;
	GPIO_PD12_LED.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13| GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_PD12_LED.GPIO_PuPd = GPIO_PuPd_DOWN;

	GPIO_Init(GPIOD , &GPIO_PD12_LED);
	
	
	
	
	GPIO_StructInit(&GPIO_PA0_EXT);
	
	GPIO_PA0_EXT.GPIO_Mode = GPIO_Mode_IN;
	GPIO_PA0_EXT.GPIO_OType = GPIO_OType_PP;
	GPIO_PA0_EXT.GPIO_Pin = GPIO_Pin_0;
	GPIO_PA0_EXT.GPIO_PuPd = GPIO_PuPd_UP;

	GPIO_Init(GPIOA , &GPIO_PA0_EXT);
}
void init_PE1(void){
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
	GPIO_InitTypeDef GPIO_PE1;
	
	GPIO_StructInit(&GPIO_PE1);
	
	GPIO_PE1.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_PE1.GPIO_OType = GPIO_OType_PP;
	GPIO_PE1.GPIO_Pin = GPIO_Pin_1;
	GPIO_PE1.GPIO_PuPd = GPIO_PuPd_DOWN;
  GPIO_PE1.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOE , &GPIO_PE1);
}
void init_MCO(void){
	
//	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure2;
	
//	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE); //    �������� ������������ ����� A
//		GPIO_InitStructure.GPIO_Pin 	= GPIO_Pin_8; //    ����������� PA8 �� ����� ������������ � �������� ����
//		GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_OUT; //  �������������� ����� � ���������
//		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz; //    ���������������� ������ 50���
//	GPIO_Init(GPIOA, &GPIO_InitStructure); //   ������������� ������ �����
//	RCC_MCO1Config(RCC_MCO1Source_HSI, RCC_MCO1Div_1); //   �������� ����� ����������� � �������� ���� �� ����� MCO (RA8)


	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE); //    �������� ������������ ����� A
		GPIO_InitStructure2.GPIO_Pin 	= GPIO_Pin_9; //    ����������� PA8 �� ����� ������������ � �������� ����
		GPIO_InitStructure2.GPIO_Mode 	= GPIO_Mode_AF; //  �������������� ����� � ���������
		GPIO_InitStructure2.GPIO_Speed = GPIO_Speed_100MHz; //    ���������������� ������ 50���
	GPIO_Init(GPIOC, &GPIO_InitStructure2); //   ������������� ������ �����
	RCC_MCO2Config(RCC_MCO2Source_HSE, RCC_MCO2Div_4); //   �������� ����� ����������� � �������� ���� �� ����� MCO (RA8)
}
void init_FSMC(void){
	
	// �������� ����� �16 ������������ ��� ������ RS ��� ��� 
	// LCD /CS is NE1 - Bank 1 of NOR/SRAM Bank 1~4 

	FSMC_NORSRAMInitTypeDef FSMC_NORSRAMInitStructure;
	FSMC_NORSRAMTimingInitTypeDef p;
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_GPIOE |RCC_AHB1Periph_GPIOC, ENABLE);
	// ���� ������ , ������� ������ ������ NOE--> RD_TFT NWE--> WR_TFT 
	// PD.00(D2), PD.01(D3), PD.04(RD), PD.5(WR), PD.7 NE1(CS), PD.8(D13), PD.9(D14), PD.10(D15), 
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1| GPIO_Pin_4 |GPIO_Pin_5 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10|
	//	A16
	// PD.11(RS) PD.14(D0)	PD.15(D1) 
	GPIO_Pin_11 | GPIO_Pin_14| GPIO_Pin_15;
	// ����������� ����� ������ ������� 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;	
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;	
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;	
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	GPIO_PinAFConfig(GPIOD, GPIO_PinSource0, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource1, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource4, GPIO_AF_FSMC);	// TFT_RD
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource5, GPIO_AF_FSMC);	// TFT_WR
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource7, GPIO_AF_FSMC);	// TFT_CS
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource8, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource9, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource10, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource11, GPIO_AF_FSMC);	// TFT_RS
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource14, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource15, GPIO_AF_FSMC);
	// ���� ������ PE.07(D4), PE.08(D5), PE.09(D6), PE.10(D7), PE.11(D8), PE.12(D9), PE.13(D10), 
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11| GPIO_Pin_12| GPIO_Pin_13| 
	// PE.14(D11), PE.15(D12)
	GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_Init(GPIOE, &GPIO_InitStructure);	

	GPIO_PinAFConfig(GPIOE, GPIO_PinSource7 , GPIO_AF_FSMC);	
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource8 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource9 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource10 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource11 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource12 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource13 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource14 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource15 , GPIO_AF_FSMC);
	// ������������� ����� ���������� ���������� 
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	/*-- FSMC Configuration ------------------------------------------------------*/
	// ��������� ������������ FSMC 
	RCC_AHB3PeriphClockCmd(RCC_AHB3Periph_FSMC, ENABLE);
	// ��������� ��������� ���������� ����������� 
		p.FSMC_AddressSetupTime = 5;
	p.FSMC_AddressHoldTime = 0;
	p.FSMC_DataSetupTime = 0;
	p.FSMC_BusTurnAroundDuration = 0;
	p.FSMC_CLKDivision = 0;
	p.FSMC_DataLatency = 0;
	p.FSMC_AccessMode = FSMC_AccessMode_A;
////	p.FSMC_AddressSetupTime = 5;
////	p.FSMC_AddressHoldTime = 0;
////	p.FSMC_DataSetupTime = 9;
////	p.FSMC_BusTurnAroundDuration = 0;
////	p.FSMC_CLKDivision = 0;
////	p.FSMC_DataLatency = 0;
////	p.FSMC_AccessMode = FSMC_AccessMode_A;
	// Color LCD configuration - LCD configured as follow:
	// - Data/Address MUX = Disable
	// - Memory Type = SRAM
	// - Data Width = 16bit
	// - Write Operation = Enable
	// - Extended Mode = Enable
	// - Asynchronous Wait = Disable 
	// ��������� ����������� ������ 
	FSMC_NORSRAMInitStructure.FSMC_Bank = FSMC_Bank1_NORSRAM1;
	FSMC_NORSRAMInitStructure.FSMC_DataAddressMux = FSMC_DataAddressMux_Disable;
	FSMC_NORSRAMInitStructure.FSMC_MemoryType = FSMC_MemoryType_SRAM;
	FSMC_NORSRAMInitStructure.FSMC_MemoryDataWidth = FSMC_MemoryDataWidth_16b;
	FSMC_NORSRAMInitStructure.FSMC_BurstAccessMode = FSMC_BurstAccessMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_AsynchronousWait = FSMC_AsynchronousWait_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignalPolarity = FSMC_WaitSignalPolarity_Low;
	FSMC_NORSRAMInitStructure.FSMC_WrapMode = FSMC_WrapMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignalActive = FSMC_WaitSignalActive_BeforeWaitState;
	FSMC_NORSRAMInitStructure.FSMC_WriteOperation = FSMC_WriteOperation_Enable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignal = FSMC_WaitSignal_Disable;
	FSMC_NORSRAMInitStructure.FSMC_ExtendedMode = FSMC_ExtendedMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WriteBurst = FSMC_WriteBurst_Disable;
	FSMC_NORSRAMInitStructure.FSMC_ReadWriteTimingStruct = &p;
	FSMC_NORSRAMInitStructure.FSMC_WriteTimingStruct = &p;
	FSMC_NORSRAMInit(&FSMC_NORSRAMInitStructure); 
	// Enable FSMC NOR/SRAM Bank1 
	FSMC_NORSRAMCmd(FSMC_Bank1_NORSRAM1, ENABLE);
}
void init_RM68120(void){

Lcd_Write_Reg(0XF000,0X55);
Lcd_Write_Reg(0XF001,0XAA);
Lcd_Write_Reg(0XF002,0X52);
Lcd_Write_Reg(0XF003,0X08);
Lcd_Write_Reg(0XF004,0X01);

//GAMMA RED
Lcd_Write_Reg(0XD100,0X00);  
Lcd_Write_Reg(0XD101,0X00);	  
Lcd_Write_Reg(0XD102,0X48);  
Lcd_Write_Reg(0XD103,0X8B);  
Lcd_Write_Reg(0XD104,0XB5);  
Lcd_Write_Reg(0XD105,0X54);  
Lcd_Write_Reg(0XD106,0XD5);  
Lcd_Write_Reg(0XD107,0X04);  
Lcd_Write_Reg(0XD108,0X28);  
Lcd_Write_Reg(0XD109,0X5E);  
Lcd_Write_Reg(0XD10A,0X55);  
Lcd_Write_Reg(0XD10B,0X86);  
Lcd_Write_Reg(0XD10C,0XA7);  
Lcd_Write_Reg(0XD10D,0XC2);  
Lcd_Write_Reg(0XD10E,0XDC);  
Lcd_Write_Reg(0XD10F,0XA9);
Lcd_Write_Reg(0XD110,0XF2);  
Lcd_Write_Reg(0XD111,0X05);  
Lcd_Write_Reg(0XD112,0X17);  
Lcd_Write_Reg(0XD113,0X28);  
Lcd_Write_Reg(0XD114,0XAA);  
Lcd_Write_Reg(0XD115,0X38);  
Lcd_Write_Reg(0XD116,0X46);  
Lcd_Write_Reg(0XD117,0X55);  
Lcd_Write_Reg(0XD118,0X63);  
Lcd_Write_Reg(0XD119,0XAA);  
Lcd_Write_Reg(0XD11A,0X70);  
Lcd_Write_Reg(0XD11B,0X71);  
Lcd_Write_Reg(0XD11C,0X7E); 
Lcd_Write_Reg(0XD11D,0X8B);  
Lcd_Write_Reg(0XD11E,0XAA);  
Lcd_Write_Reg(0XD11F,0X97);  
Lcd_Write_Reg(0XD120,0XA3);  
Lcd_Write_Reg(0XD121,0XB0);
Lcd_Write_Reg(0XD122,0XBC);
Lcd_Write_Reg(0XD123,0XAA);
Lcd_Write_Reg(0XD124,0XC8);
Lcd_Write_Reg(0XD125,0XD4);
Lcd_Write_Reg(0XD126,0XE0);
Lcd_Write_Reg(0XD127,0XEE);
Lcd_Write_Reg(0XD128,0XFE);
Lcd_Write_Reg(0XD129,0XFD);
Lcd_Write_Reg(0XD12A,0X0E);
Lcd_Write_Reg(0XD12B,0X25);
Lcd_Write_Reg(0XD12C,0X4B);
Lcd_Write_Reg(0XD12D,0XFF);
Lcd_Write_Reg(0XD12E,0X69);
Lcd_Write_Reg(0XD12F,0X97);
Lcd_Write_Reg(0XD130,0XB7);
Lcd_Write_Reg(0XD131,0XDD);
Lcd_Write_Reg(0XD132,0X0F);
Lcd_Write_Reg(0XD133,0XFB);  
Lcd_Write_Reg(0XD134,0XFF);  
//GAMMA GREEN
Lcd_Write_Reg(0XD200,0X00);  
Lcd_Write_Reg(0XD201,0X00);	  
Lcd_Write_Reg(0XD202,0X48);  
Lcd_Write_Reg(0XD203,0X8B);  
Lcd_Write_Reg(0XD204,0XB5);  
Lcd_Write_Reg(0XD205,0X54);  
Lcd_Write_Reg(0XD206,0XD5);  
Lcd_Write_Reg(0XD207,0X04);  
Lcd_Write_Reg(0XD208,0X28);  
Lcd_Write_Reg(0XD209,0X5E);  
Lcd_Write_Reg(0XD20A,0X55);  
Lcd_Write_Reg(0XD20B,0X86);  
Lcd_Write_Reg(0XD20C,0XA7);  
Lcd_Write_Reg(0XD20D,0XC2);  
Lcd_Write_Reg(0XD20E,0XDC);  
Lcd_Write_Reg(0XD20F,0XA9);
Lcd_Write_Reg(0XD210,0XF2);  
Lcd_Write_Reg(0XD211,0X05);  
Lcd_Write_Reg(0XD212,0X17);  
Lcd_Write_Reg(0XD213,0X28);  
Lcd_Write_Reg(0XD214,0XAA);  
Lcd_Write_Reg(0XD215,0X38);  
Lcd_Write_Reg(0XD216,0X46);  
Lcd_Write_Reg(0XD217,0X55);  
Lcd_Write_Reg(0XD218,0X63);  
Lcd_Write_Reg(0XD219,0XAA);  
Lcd_Write_Reg(0XD21A,0X70);  
Lcd_Write_Reg(0XD21B,0X71);  
Lcd_Write_Reg(0XD21C,0X7E); 
Lcd_Write_Reg(0XD21D,0X8B);  
Lcd_Write_Reg(0XD21E,0XAA);  
Lcd_Write_Reg(0XD21F,0X97);  
Lcd_Write_Reg(0XD220,0XA3);  
Lcd_Write_Reg(0XD221,0XB0);
Lcd_Write_Reg(0XD222,0XBC);
Lcd_Write_Reg(0XD223,0XAA);
Lcd_Write_Reg(0XD224,0XC8);
Lcd_Write_Reg(0XD225,0XD4);
Lcd_Write_Reg(0XD226,0XE0);
Lcd_Write_Reg(0XD227,0XEE);
Lcd_Write_Reg(0XD228,0XFE);
Lcd_Write_Reg(0XD229,0XFD);
Lcd_Write_Reg(0XD22A,0X0E);
Lcd_Write_Reg(0XD22B,0X25);
Lcd_Write_Reg(0XD22C,0X4B);
Lcd_Write_Reg(0XD22D,0XFF);
Lcd_Write_Reg(0XD22E,0X69);
Lcd_Write_Reg(0XD22F,0X97);
Lcd_Write_Reg(0XD230,0XB7);
Lcd_Write_Reg(0XD231,0XDD);
Lcd_Write_Reg(0XD232,0X0F);
Lcd_Write_Reg(0XD233,0XFB);  
Lcd_Write_Reg(0XD234,0XFF); 
//GAMMA SETT BLUE
Lcd_Write_Reg(0XD300,0X00);  
Lcd_Write_Reg(0XD301,0X00);	  
Lcd_Write_Reg(0XD302,0X48);  
Lcd_Write_Reg(0XD303,0X8B);  
Lcd_Write_Reg(0XD304,0XB5);  
Lcd_Write_Reg(0XD305,0X54);  
Lcd_Write_Reg(0XD306,0XD5);  
Lcd_Write_Reg(0XD307,0X04);  
Lcd_Write_Reg(0XD308,0X28);  
Lcd_Write_Reg(0XD309,0X5E);  
Lcd_Write_Reg(0XD30A,0X55);  
Lcd_Write_Reg(0XD30B,0X86);  
Lcd_Write_Reg(0XD30C,0XA7);  
Lcd_Write_Reg(0XD30D,0XC2);  
Lcd_Write_Reg(0XD30E,0XDC);  
Lcd_Write_Reg(0XD30F,0XA9);
Lcd_Write_Reg(0XD310,0XF2);  
Lcd_Write_Reg(0XD311,0X05);  
Lcd_Write_Reg(0XD312,0X17);  
Lcd_Write_Reg(0XD313,0X28);  
Lcd_Write_Reg(0XD314,0XAA);  
Lcd_Write_Reg(0XD315,0X38);  
Lcd_Write_Reg(0XD316,0X46);  
Lcd_Write_Reg(0XD317,0X55);  
Lcd_Write_Reg(0XD318,0X63);  
Lcd_Write_Reg(0XD319,0XAA);  
Lcd_Write_Reg(0XD31A,0X70);  
Lcd_Write_Reg(0XD31B,0X71);  
Lcd_Write_Reg(0XD31C,0X7E); 
Lcd_Write_Reg(0XD31D,0X8B);  
Lcd_Write_Reg(0XD31E,0XAA);  
Lcd_Write_Reg(0XD31F,0X97);  
Lcd_Write_Reg(0XD320,0XA3);  
Lcd_Write_Reg(0XD321,0XB0);
Lcd_Write_Reg(0XD322,0XBC);
Lcd_Write_Reg(0XD323,0XAA);
Lcd_Write_Reg(0XD324,0XC8);
Lcd_Write_Reg(0XD325,0XD4);
Lcd_Write_Reg(0XD326,0XE0);
Lcd_Write_Reg(0XD327,0XEE);
Lcd_Write_Reg(0XD328,0XFE);
Lcd_Write_Reg(0XD329,0XFD);
Lcd_Write_Reg(0XD32A,0X0E);
Lcd_Write_Reg(0XD32B,0X25);
Lcd_Write_Reg(0XD32C,0X4B);
Lcd_Write_Reg(0XD32D,0XFF);
Lcd_Write_Reg(0XD32E,0X69);
Lcd_Write_Reg(0XD32F,0X97);
Lcd_Write_Reg(0XD330,0XB7);
Lcd_Write_Reg(0XD331,0XDD);
Lcd_Write_Reg(0XD332,0X0F);
Lcd_Write_Reg(0XD333,0XFB);  
Lcd_Write_Reg(0XD334,0XFF); 

//GAMMA SET RED
Lcd_Write_Reg(0XD400,0X00);  
Lcd_Write_Reg(0XD401,0X00);	  
Lcd_Write_Reg(0XD402,0X48);  
Lcd_Write_Reg(0XD403,0X8B);  
Lcd_Write_Reg(0XD404,0XB5);  
Lcd_Write_Reg(0XD405,0X54);  
Lcd_Write_Reg(0XD406,0XD5);  
Lcd_Write_Reg(0XD407,0X04);  
Lcd_Write_Reg(0XD408,0X28);  
Lcd_Write_Reg(0XD409,0X5E);  
Lcd_Write_Reg(0XD40A,0X55);  
Lcd_Write_Reg(0XD40B,0X86);  
Lcd_Write_Reg(0XD40C,0XA7);  
Lcd_Write_Reg(0XD40D,0XC2);  
Lcd_Write_Reg(0XD40E,0XDC);  
Lcd_Write_Reg(0XD40F,0XA9);
Lcd_Write_Reg(0XD410,0XF2);  
Lcd_Write_Reg(0XD411,0X05);  
Lcd_Write_Reg(0XD412,0X17);  
Lcd_Write_Reg(0XD413,0X28);  
Lcd_Write_Reg(0XD414,0XAA);  
Lcd_Write_Reg(0XD415,0X38);  
Lcd_Write_Reg(0XD416,0X46);  
Lcd_Write_Reg(0XD417,0X55);  
Lcd_Write_Reg(0XD418,0X63);  
Lcd_Write_Reg(0XD419,0XAA);  
Lcd_Write_Reg(0XD41A,0X70);  
Lcd_Write_Reg(0XD41B,0X71);  
Lcd_Write_Reg(0XD41C,0X7E); 
Lcd_Write_Reg(0XD41D,0X8B);  
Lcd_Write_Reg(0XD41E,0XAA);  
Lcd_Write_Reg(0XD41F,0X97);  
Lcd_Write_Reg(0XD420,0XA3);  
Lcd_Write_Reg(0XD421,0XB0);
Lcd_Write_Reg(0XD422,0XBC);
Lcd_Write_Reg(0XD423,0XAA);
Lcd_Write_Reg(0XD424,0XC8);
Lcd_Write_Reg(0XD425,0XD4);
Lcd_Write_Reg(0XD426,0XE0);
Lcd_Write_Reg(0XD427,0XEE);
Lcd_Write_Reg(0XD428,0XFE);
Lcd_Write_Reg(0XD429,0XFD);
Lcd_Write_Reg(0XD42A,0X0E);
Lcd_Write_Reg(0XD42B,0X25);
Lcd_Write_Reg(0XD42C,0X4B);
Lcd_Write_Reg(0XD42D,0XFF);
Lcd_Write_Reg(0XD42E,0X69);
Lcd_Write_Reg(0XD42F,0X97);
Lcd_Write_Reg(0XD430,0XB7);
Lcd_Write_Reg(0XD431,0XDD);
Lcd_Write_Reg(0XD432,0X0F);
Lcd_Write_Reg(0XD433,0XFB);  
Lcd_Write_Reg(0XD434,0XFF); 
//GAMMA SET GREEN
Lcd_Write_Reg(0XD500,0X00);  
Lcd_Write_Reg(0XD501,0X00);	  
Lcd_Write_Reg(0XD502,0X48);  
Lcd_Write_Reg(0XD503,0X8B);  
Lcd_Write_Reg(0XD504,0XB5);  
Lcd_Write_Reg(0XD505,0X54);  
Lcd_Write_Reg(0XD506,0XD5);  
Lcd_Write_Reg(0XD507,0X04);  
Lcd_Write_Reg(0XD508,0X28);  
Lcd_Write_Reg(0XD509,0X5E);  
Lcd_Write_Reg(0XD50A,0X55);  
Lcd_Write_Reg(0XD50B,0X86);  
Lcd_Write_Reg(0XD50C,0XA7);  
Lcd_Write_Reg(0XD50D,0XC2);  
Lcd_Write_Reg(0XD50E,0XDC);  
Lcd_Write_Reg(0XD50F,0XA9);
Lcd_Write_Reg(0XD510,0XF2);  
Lcd_Write_Reg(0XD511,0X05);  
Lcd_Write_Reg(0XD512,0X17);  
Lcd_Write_Reg(0XD513,0X28);  
Lcd_Write_Reg(0XD514,0XAA);  
Lcd_Write_Reg(0XD515,0X38);  
Lcd_Write_Reg(0XD516,0X46);  
Lcd_Write_Reg(0XD517,0X55);  
Lcd_Write_Reg(0XD518,0X63);  
Lcd_Write_Reg(0XD519,0XAA);  
Lcd_Write_Reg(0XD51A,0X70);  
Lcd_Write_Reg(0XD51B,0X71);  
Lcd_Write_Reg(0XD51C,0X7E); 
Lcd_Write_Reg(0XD51D,0X8B);  
Lcd_Write_Reg(0XD51E,0XAA);  
Lcd_Write_Reg(0XD51F,0X97);  
Lcd_Write_Reg(0XD520,0XA3);  
Lcd_Write_Reg(0XD521,0XB0);
Lcd_Write_Reg(0XD522,0XBC);
Lcd_Write_Reg(0XD523,0XAA);
Lcd_Write_Reg(0XD524,0XC8);
Lcd_Write_Reg(0XD525,0XD4);
Lcd_Write_Reg(0XD526,0XE0);
Lcd_Write_Reg(0XD527,0XEE);
Lcd_Write_Reg(0XD528,0XFE);
Lcd_Write_Reg(0XD529,0XFD);
Lcd_Write_Reg(0XD52A,0X0E);
Lcd_Write_Reg(0XD52B,0X25);
Lcd_Write_Reg(0XD52C,0X4B);
Lcd_Write_Reg(0XD52D,0XFF);
Lcd_Write_Reg(0XD52E,0X69);
Lcd_Write_Reg(0XD52F,0X97);
Lcd_Write_Reg(0XD530,0XB7);
Lcd_Write_Reg(0XD531,0XDD);
Lcd_Write_Reg(0XD532,0X0F);
Lcd_Write_Reg(0XD533,0XFB);  
Lcd_Write_Reg(0XD534,0XFF); 

//GAMMA SET BLUE
Lcd_Write_Reg(0XD600,0X00);  
Lcd_Write_Reg(0XD601,0X00);	  
Lcd_Write_Reg(0XD602,0X48);  
Lcd_Write_Reg(0XD603,0X8B);  
Lcd_Write_Reg(0XD604,0XB5);  
Lcd_Write_Reg(0XD605,0X54);  
Lcd_Write_Reg(0XD606,0XD5);  
Lcd_Write_Reg(0XD607,0X04);  
Lcd_Write_Reg(0XD608,0X28);  
Lcd_Write_Reg(0XD609,0X5E);  
Lcd_Write_Reg(0XD60A,0X55);  
Lcd_Write_Reg(0XD60B,0X86);  
Lcd_Write_Reg(0XD60C,0XA7);  
Lcd_Write_Reg(0XD60D,0XC2);  
Lcd_Write_Reg(0XD60E,0XDC);  
Lcd_Write_Reg(0XD60F,0XA9);
Lcd_Write_Reg(0XD610,0XF2);  
Lcd_Write_Reg(0XD611,0X05);  
Lcd_Write_Reg(0XD612,0X17);  
Lcd_Write_Reg(0XD613,0X28);  
Lcd_Write_Reg(0XD614,0XAA);  
Lcd_Write_Reg(0XD615,0X38);  
Lcd_Write_Reg(0XD616,0X46);  
Lcd_Write_Reg(0XD617,0X55);  
Lcd_Write_Reg(0XD618,0X63);  
Lcd_Write_Reg(0XD619,0XAA);  
Lcd_Write_Reg(0XD61A,0X70);  
Lcd_Write_Reg(0XD61B,0X71);  
Lcd_Write_Reg(0XD61C,0X7E); 
Lcd_Write_Reg(0XD61D,0X8B);  
Lcd_Write_Reg(0XD61E,0XAA);  
Lcd_Write_Reg(0XD61F,0X97);  
Lcd_Write_Reg(0XD620,0XA3);  
Lcd_Write_Reg(0XD621,0XB0);
Lcd_Write_Reg(0XD622,0XBC);
Lcd_Write_Reg(0XD623,0XAA);
Lcd_Write_Reg(0XD624,0XC8);
Lcd_Write_Reg(0XD625,0XD4);
Lcd_Write_Reg(0XD626,0XE0);
Lcd_Write_Reg(0XD627,0XEE);
Lcd_Write_Reg(0XD628,0XFE);
Lcd_Write_Reg(0XD629,0XFD);
Lcd_Write_Reg(0XD62A,0X0E);
Lcd_Write_Reg(0XD62B,0X25);
Lcd_Write_Reg(0XD62C,0X4B);
Lcd_Write_Reg(0XD62D,0XFF);
Lcd_Write_Reg(0XD62E,0X69);
Lcd_Write_Reg(0XD62F,0X97);
Lcd_Write_Reg(0XD630,0XB7);
Lcd_Write_Reg(0XD631,0XDD);
Lcd_Write_Reg(0XD632,0X0F);
Lcd_Write_Reg(0XD633,0XFB);  
Lcd_Write_Reg(0XD634,0XFF); 
//GAMMA SET END


Lcd_Write_Reg(0XB000,0X00);  
Lcd_Write_Reg(0XB001,0X00);  
Lcd_Write_Reg(0XB002,0X00);  
Lcd_Write_Reg(0XB100,0X05);  
Lcd_Write_Reg(0XB101,0X05);  
Lcd_Write_Reg(0XB102,0X05);  
Lcd_Write_Reg(0XB600,0X44);  
Lcd_Write_Reg(0XB601,0X44);  
Lcd_Write_Reg(0XB602,0X44);  
Lcd_Write_Reg(0XB700,0X34);  
Lcd_Write_Reg(0XB701,0X34);  
Lcd_Write_Reg(0XB702,0X34);  
Lcd_Write_Reg(0XB800,0X24);
Lcd_Write_Reg(0XB801,0X24);
Lcd_Write_Reg(0XB802,0X24);
Lcd_Write_Reg(0XB900,0X34);
Lcd_Write_Reg(0XB901,0X34);
Lcd_Write_Reg(0XB902,0X34);
Lcd_Write_Reg(0XBA00,0X14);  
Lcd_Write_Reg(0XBA01,0X14);
Lcd_Write_Reg(0XBA02,0X14);
Lcd_Write_Reg(0XBF00,0X01); 
Lcd_Write_Reg(0XB300,0X07);  
Lcd_Write_Reg(0XB301,0X07);
Lcd_Write_Reg(0XB302,0X07);
Lcd_Write_Reg(0XB900,0X25);
Lcd_Write_Reg(0XB901,0X25);
Lcd_Write_Reg(0XB902,0X25);
Lcd_Write_Reg(0XBC00,0X00);
Lcd_Write_Reg(0XBC01,0X90);//A0  90  A8
Lcd_Write_Reg(0XBC02,0X61);//3A  61  00
Lcd_Write_Reg(0XBD00,0X00);
Lcd_Write_Reg(0XBD01,0X90);//A0
Lcd_Write_Reg(0XBD02,0X61);//3A
Lcd_Write_Reg(0XBE00,0X00);
Lcd_Write_Reg(0XBE01,0X8F);//8e
Lcd_Write_Reg(0XF000,0X55);
Lcd_Write_Reg(0XF001,0XAA);
Lcd_Write_Reg(0XF002,0X52);
Lcd_Write_Reg(0XF003,0X08);
Lcd_Write_Reg(0XF004,0X00);
Lcd_Write_Reg(0XB400,0X10);
Lcd_Write_Reg(0XB600,0X02);
Lcd_Write_Reg(0XB100,0XCC);
Lcd_Write_Reg(0XB700,0X22);
Lcd_Write_Reg(0XB701,0X22);
Lcd_Write_Reg(0XC80B,0X2A);
Lcd_Write_Reg(0XC80C,0X2A);
Lcd_Write_Reg(0XC80F,0X2A);
Lcd_Write_Reg(0XC810,0X2A);
Lcd_Write_Reg(0XB800,0X01);
Lcd_Write_Reg(0XB801,0X03);
Lcd_Write_Reg(0XB802,0X03);
Lcd_Write_Reg(0XB803,0X03);
Lcd_Write_Reg(0XBC00,0X05);//05
Lcd_Write_Reg(0XBC01,0X05);
Lcd_Write_Reg(0XBC02,0X05);
Lcd_Write_Reg(0XD000,0X01);
Lcd_Write_Reg(0XBA00,0X01);
Lcd_Write_Reg(0XBD02,0X07);
Lcd_Write_Reg(0XBD03,0X31);
Lcd_Write_Reg(0XBE02,0X07);
Lcd_Write_Reg(0XBE03,0X31);
Lcd_Write_Reg(0XBF02,0X07);
Lcd_Write_Reg(0XBF03,0X31);
Lcd_Write_Reg(0XB300,0X00);
Lcd_Write_Reg(0XBD00,0X07);
Lcd_Write_Reg(0XBE00,0X07);
Lcd_Write_Reg(0XBF00,0X07);
Lcd_Write_Reg(0XF000,0X55);
Lcd_Write_Reg(0XF001,0XAA);
Lcd_Write_Reg(0XF002,0X52);
Lcd_Write_Reg(0XF003,0X08);
Lcd_Write_Reg(0XF004,0X02);
Lcd_Write_Reg(0XC301,0XA9);
Lcd_Write_Reg(0XFE01,0X94);
Lcd_Write_Reg(0XF600,0X60);
Lcd_Write_Reg(0X3500,0X00);
Lcd_Write_Reg(0XF000,0X55);
Lcd_Write_Reg(0XF001,0XAA);
Lcd_Write_Reg(0XF002,0X52);
Lcd_Write_Reg(0XF003,0X08);
Lcd_Write_Reg(0XF004,0X01);//01
Lcd_Write_Reg(0X3600,0X00);				// B5 - 1� = Row/column exchange
Lcd_Write_Reg(0X3a00,0X77);	//	  



Lcd_Write_Index(0X1100); //
for(uint32_t i = 0; i < 100000; i++){}//MDELAY(10); 
Lcd_Write_Index(0X2900); //
for(uint32_t i = 0; i < 10000; i++){}//MDELAY(10); 
Lcd_Write_Index(0X2C00); //
for(uint32_t i = 0; i < 10000; i++){}//MDELAY(10); 
//	Lcd_Write_Index(0X2300); //
//for(uint32_t i = 0; i < 10000; i++){}//MDELAY(10); 
}
void init_RM68120_2(void){	//https://github.com/luckasfb/lcm_drivers/blob/master/baoxue_mytqt_yywi/lcm/dh402_rm68120/dh402_rm68120.c

	Lcd_Write_Index(0xF000);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xF001);Lcd_Write_Data(0xAA);
	Lcd_Write_Index(0xF002);Lcd_Write_Data(0x52);
	Lcd_Write_Index(0xF003);Lcd_Write_Data(0x08);
	Lcd_Write_Index(0xF004);Lcd_Write_Data(0x01);

	Lcd_Write_Index(0xD100);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD101);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD102);Lcd_Write_Data(0x11);
	Lcd_Write_Index(0xD103);Lcd_Write_Data(0x2F);
	Lcd_Write_Index(0xD104);Lcd_Write_Data(0x49);
	Lcd_Write_Index(0xD105);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD106);Lcd_Write_Data(0x5E);
	Lcd_Write_Index(0xD107);Lcd_Write_Data(0x80);
	Lcd_Write_Index(0xD108);Lcd_Write_Data(0x9D);
	Lcd_Write_Index(0xD109);Lcd_Write_Data(0xCA);
	Lcd_Write_Index(0xD10a);Lcd_Write_Data(0x54);
	Lcd_Write_Index(0xD10b);Lcd_Write_Data(0xEE);
	Lcd_Write_Index(0xD10c);Lcd_Write_Data(0x0B);
	Lcd_Write_Index(0xD10d);Lcd_Write_Data(0x24);
	Lcd_Write_Index(0xD10e);Lcd_Write_Data(0x3B);
	Lcd_Write_Index(0xD10f);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD110);Lcd_Write_Data(0x4F);
	Lcd_Write_Index(0xD111);Lcd_Write_Data(0x61);
	Lcd_Write_Index(0xD112);Lcd_Write_Data(0x72);
	Lcd_Write_Index(0xD113);Lcd_Write_Data(0x82);
	Lcd_Write_Index(0xD114);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD115);Lcd_Write_Data(0x91);
	Lcd_Write_Index(0xD116);Lcd_Write_Data(0x9F);
	Lcd_Write_Index(0xD117);Lcd_Write_Data(0xAD);
	Lcd_Write_Index(0xD118);Lcd_Write_Data(0xBA);
	Lcd_Write_Index(0xD119);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD11a);Lcd_Write_Data(0xC6);
	Lcd_Write_Index(0xD11b);Lcd_Write_Data(0xC8);
	Lcd_Write_Index(0xD11c);Lcd_Write_Data(0xD4);
	Lcd_Write_Index(0xD11d);Lcd_Write_Data(0xE0);
	Lcd_Write_Index(0xD11e);Lcd_Write_Data(0xA5);
	Lcd_Write_Index(0xD11f);Lcd_Write_Data(0xEB);
	Lcd_Write_Index(0xD120);Lcd_Write_Data(0xF6);
	Lcd_Write_Index(0xD121);Lcd_Write_Data(0x01);
	Lcd_Write_Index(0xD122);Lcd_Write_Data(0x0D);
	Lcd_Write_Index(0xD123);Lcd_Write_Data(0xAA);
	Lcd_Write_Index(0xD124);Lcd_Write_Data(0x19);
	Lcd_Write_Index(0xD125);Lcd_Write_Data(0x25);
	Lcd_Write_Index(0xD126);Lcd_Write_Data(0x31);
	Lcd_Write_Index(0xD127);Lcd_Write_Data(0x40);
	Lcd_Write_Index(0xD128);Lcd_Write_Data(0xAA);
	Lcd_Write_Index(0xD129);Lcd_Write_Data(0x51);
	Lcd_Write_Index(0xD12a);Lcd_Write_Data(0x66);
	Lcd_Write_Index(0xD12b);Lcd_Write_Data(0x85);
	Lcd_Write_Index(0xD12c);Lcd_Write_Data(0xBA);
	Lcd_Write_Index(0xD12d);Lcd_Write_Data(0xFE);
	Lcd_Write_Index(0xD12e);Lcd_Write_Data(0xE5);
	Lcd_Write_Index(0xD12f);Lcd_Write_Data(0x2E);
	Lcd_Write_Index(0xD130);Lcd_Write_Data(0x63);
	Lcd_Write_Index(0xD131);Lcd_Write_Data(0xA8);
	Lcd_Write_Index(0xD132);Lcd_Write_Data(0x0F);
	Lcd_Write_Index(0xD133);Lcd_Write_Data(0xED);
	Lcd_Write_Index(0xD134);Lcd_Write_Data(0xFF);

	Lcd_Write_Index(0xD200);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD201);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD202);Lcd_Write_Data(0x11);
	Lcd_Write_Index(0xD203);Lcd_Write_Data(0x2F);
	Lcd_Write_Index(0xD204);Lcd_Write_Data(0x49);
	Lcd_Write_Index(0xD205);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD206);Lcd_Write_Data(0x5E);
	Lcd_Write_Index(0xD207);Lcd_Write_Data(0x80);
	Lcd_Write_Index(0xD208);Lcd_Write_Data(0x9D);
	Lcd_Write_Index(0xD209);Lcd_Write_Data(0xCA);
	Lcd_Write_Index(0xD20a);Lcd_Write_Data(0x54);
	Lcd_Write_Index(0xD20b);Lcd_Write_Data(0xEE);
	Lcd_Write_Index(0xD20c);Lcd_Write_Data(0x0B);
	Lcd_Write_Index(0xD20d);Lcd_Write_Data(0x24);
	Lcd_Write_Index(0xD20e);Lcd_Write_Data(0x3B);
	Lcd_Write_Index(0xD20f);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD210);Lcd_Write_Data(0x4F);
	Lcd_Write_Index(0xD211);Lcd_Write_Data(0x61);
	Lcd_Write_Index(0xD212);Lcd_Write_Data(0x72);
	Lcd_Write_Index(0xD213);Lcd_Write_Data(0x82);
	Lcd_Write_Index(0xD214);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD215);Lcd_Write_Data(0x91);
	Lcd_Write_Index(0xD216);Lcd_Write_Data(0x9F);
	Lcd_Write_Index(0xD217);Lcd_Write_Data(0xAD);
	Lcd_Write_Index(0xD218);Lcd_Write_Data(0xBA);
	Lcd_Write_Index(0xD219);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD21a);Lcd_Write_Data(0xC6);
	Lcd_Write_Index(0xD21b);Lcd_Write_Data(0xC8);
	Lcd_Write_Index(0xD21c);Lcd_Write_Data(0xD4);
	Lcd_Write_Index(0xD21d);Lcd_Write_Data(0xE0);
	Lcd_Write_Index(0xD21e);Lcd_Write_Data(0xA5);
	Lcd_Write_Index(0xD21f);Lcd_Write_Data(0xEB);
	Lcd_Write_Index(0xD220);Lcd_Write_Data(0xF6);
	Lcd_Write_Index(0xD221);Lcd_Write_Data(0x01);
	Lcd_Write_Index(0xD222);Lcd_Write_Data(0x0D);
	Lcd_Write_Index(0xD223);Lcd_Write_Data(0xAA);
	Lcd_Write_Index(0xD224);Lcd_Write_Data(0x19);
	Lcd_Write_Index(0xD225);Lcd_Write_Data(0x25);
	Lcd_Write_Index(0xD226);Lcd_Write_Data(0x31);
	Lcd_Write_Index(0xD227);Lcd_Write_Data(0x40);
	Lcd_Write_Index(0xD228);Lcd_Write_Data(0xAA);
	Lcd_Write_Index(0xD229);Lcd_Write_Data(0x51);
	Lcd_Write_Index(0xD22a);Lcd_Write_Data(0x66);
	Lcd_Write_Index(0xD22b);Lcd_Write_Data(0x85);
	Lcd_Write_Index(0xD22c);Lcd_Write_Data(0xBA);
	Lcd_Write_Index(0xD22d);Lcd_Write_Data(0xFE);
	Lcd_Write_Index(0xD22e);Lcd_Write_Data(0xE5);
	Lcd_Write_Index(0xD22f);Lcd_Write_Data(0x2E);
	Lcd_Write_Index(0xD230);Lcd_Write_Data(0x63);
	Lcd_Write_Index(0xD231);Lcd_Write_Data(0xA8);
	Lcd_Write_Index(0xD232);Lcd_Write_Data(0x0F);
	Lcd_Write_Index(0xD233);Lcd_Write_Data(0xED);
	Lcd_Write_Index(0xD234);Lcd_Write_Data(0xFF);

	Lcd_Write_Index(0xD300);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD301);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD302);Lcd_Write_Data(0x11);
	Lcd_Write_Index(0xD303);Lcd_Write_Data(0x2F);
	Lcd_Write_Index(0xD304);Lcd_Write_Data(0x49);
	Lcd_Write_Index(0xD305);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD306);Lcd_Write_Data(0x5E);
	Lcd_Write_Index(0xD307);Lcd_Write_Data(0x80);
	Lcd_Write_Index(0xD308);Lcd_Write_Data(0x9D);
	Lcd_Write_Index(0xD309);Lcd_Write_Data(0xCA);
	Lcd_Write_Index(0xD30a);Lcd_Write_Data(0x54);
	Lcd_Write_Index(0xD30b);Lcd_Write_Data(0xEE);
	Lcd_Write_Index(0xD30c);Lcd_Write_Data(0x0B);
	Lcd_Write_Index(0xD30d);Lcd_Write_Data(0x24);
	Lcd_Write_Index(0xD30e);Lcd_Write_Data(0x3B);
	Lcd_Write_Index(0xD30f);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD310);Lcd_Write_Data(0x4F);
	Lcd_Write_Index(0xD311);Lcd_Write_Data(0x61);
	Lcd_Write_Index(0xD312);Lcd_Write_Data(0x72);
	Lcd_Write_Index(0xD313);Lcd_Write_Data(0x82);
	Lcd_Write_Index(0xD314);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD315);Lcd_Write_Data(0x91);
	Lcd_Write_Index(0xD316);Lcd_Write_Data(0x9F);
	Lcd_Write_Index(0xD317);Lcd_Write_Data(0xAD);
	Lcd_Write_Index(0xD318);Lcd_Write_Data(0xBA);
	Lcd_Write_Index(0xD319);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD31a);Lcd_Write_Data(0xC6);
	Lcd_Write_Index(0xD31b);Lcd_Write_Data(0xC8);
	Lcd_Write_Index(0xD31c);Lcd_Write_Data(0xD4);
	Lcd_Write_Index(0xD31d);Lcd_Write_Data(0xE0);
	Lcd_Write_Index(0xD31e);Lcd_Write_Data(0xA5);
	Lcd_Write_Index(0xD31f);Lcd_Write_Data(0xEB);
	Lcd_Write_Index(0xD320);Lcd_Write_Data(0xF6);
	Lcd_Write_Index(0xD321);Lcd_Write_Data(0x01);
	Lcd_Write_Index(0xD322);Lcd_Write_Data(0x0D);
	Lcd_Write_Index(0xD323);Lcd_Write_Data(0xAA);
	Lcd_Write_Index(0xD324);Lcd_Write_Data(0x19);
	Lcd_Write_Index(0xD325);Lcd_Write_Data(0x25);
	Lcd_Write_Index(0xD326);Lcd_Write_Data(0x31);
	Lcd_Write_Index(0xD327);Lcd_Write_Data(0x40);
	Lcd_Write_Index(0xD328);Lcd_Write_Data(0xAA);
	Lcd_Write_Index(0xD329);Lcd_Write_Data(0x51);
	Lcd_Write_Index(0xD32a);Lcd_Write_Data(0x66);
	Lcd_Write_Index(0xD32b);Lcd_Write_Data(0x85);
	Lcd_Write_Index(0xD32c);Lcd_Write_Data(0xBA);
	Lcd_Write_Index(0xD32d);Lcd_Write_Data(0xFE);
	Lcd_Write_Index(0xD32e);Lcd_Write_Data(0xE5);
	Lcd_Write_Index(0xD32f);Lcd_Write_Data(0x2E);
	Lcd_Write_Index(0xD330);Lcd_Write_Data(0x63);
	Lcd_Write_Index(0xD331);Lcd_Write_Data(0xA8);
	Lcd_Write_Index(0xD332);Lcd_Write_Data(0x0F);
	Lcd_Write_Index(0xD333);Lcd_Write_Data(0xED);
	Lcd_Write_Index(0xD334);Lcd_Write_Data(0xFF);

	Lcd_Write_Index(0xD400);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD401);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD402);Lcd_Write_Data(0x11);
	Lcd_Write_Index(0xD403);Lcd_Write_Data(0x2F);
	Lcd_Write_Index(0xD404);Lcd_Write_Data(0x49);
	Lcd_Write_Index(0xD405);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD406);Lcd_Write_Data(0x5E);
	Lcd_Write_Index(0xD407);Lcd_Write_Data(0x80);
	Lcd_Write_Index(0xD408);Lcd_Write_Data(0x9D);
	Lcd_Write_Index(0xD409);Lcd_Write_Data(0xCA);
	Lcd_Write_Index(0xD40a);Lcd_Write_Data(0x54);
	Lcd_Write_Index(0xD40b);Lcd_Write_Data(0xEE);
	Lcd_Write_Index(0xD40c);Lcd_Write_Data(0x0B);
	Lcd_Write_Index(0xD40d);Lcd_Write_Data(0x24);
	Lcd_Write_Index(0xD40e);Lcd_Write_Data(0x3B);
	Lcd_Write_Index(0xD40f);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD410);Lcd_Write_Data(0x4F);
	Lcd_Write_Index(0xD411);Lcd_Write_Data(0x61);
	Lcd_Write_Index(0xD412);Lcd_Write_Data(0x72);
	Lcd_Write_Index(0xD413);Lcd_Write_Data(0x82);
	Lcd_Write_Index(0xD414);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD415);Lcd_Write_Data(0x91);
	Lcd_Write_Index(0xD416);Lcd_Write_Data(0x9F);
	Lcd_Write_Index(0xD417);Lcd_Write_Data(0xAD);
	Lcd_Write_Index(0xD418);Lcd_Write_Data(0xBA);
	Lcd_Write_Index(0xD419);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD41a);Lcd_Write_Data(0xC6);
	Lcd_Write_Index(0xD41b);Lcd_Write_Data(0xC8);
	Lcd_Write_Index(0xD41c);Lcd_Write_Data(0xD4);
	Lcd_Write_Index(0xD41d);Lcd_Write_Data(0xE0);
	Lcd_Write_Index(0xD41e);Lcd_Write_Data(0xA5);
	Lcd_Write_Index(0xD41f);Lcd_Write_Data(0xEB);
	Lcd_Write_Index(0xD420);Lcd_Write_Data(0xF6);
	Lcd_Write_Index(0xD421);Lcd_Write_Data(0x01);
	Lcd_Write_Index(0xD422);Lcd_Write_Data(0x0D);
	Lcd_Write_Index(0xD423);Lcd_Write_Data(0xAA);
	Lcd_Write_Index(0xD424);Lcd_Write_Data(0x19);
	Lcd_Write_Index(0xD425);Lcd_Write_Data(0x25);
	Lcd_Write_Index(0xD426);Lcd_Write_Data(0x31);
	Lcd_Write_Index(0xD427);Lcd_Write_Data(0x40);
	Lcd_Write_Index(0xD428);Lcd_Write_Data(0xAA);
	Lcd_Write_Index(0xD429);Lcd_Write_Data(0x51);
	Lcd_Write_Index(0xD42a);Lcd_Write_Data(0x66);
	Lcd_Write_Index(0xD42b);Lcd_Write_Data(0x85);
	Lcd_Write_Index(0xD42c);Lcd_Write_Data(0xBA);
	Lcd_Write_Index(0xD42d);Lcd_Write_Data(0xFE);
	Lcd_Write_Index(0xD42e);Lcd_Write_Data(0xE5);
	Lcd_Write_Index(0xD42f);Lcd_Write_Data(0x2E);
	Lcd_Write_Index(0xD430);Lcd_Write_Data(0x63);
	Lcd_Write_Index(0xD431);Lcd_Write_Data(0xA8);
	Lcd_Write_Index(0xD432);Lcd_Write_Data(0x0F);
	Lcd_Write_Index(0xD433);Lcd_Write_Data(0xED);
	Lcd_Write_Index(0xD434);Lcd_Write_Data(0xFF);

	Lcd_Write_Index(0xD500);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD501);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD502);Lcd_Write_Data(0x11);
	Lcd_Write_Index(0xD503);Lcd_Write_Data(0x2F);
	Lcd_Write_Index(0xD504);Lcd_Write_Data(0x49);
	Lcd_Write_Index(0xD505);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD506);Lcd_Write_Data(0x5E);
	Lcd_Write_Index(0xD507);Lcd_Write_Data(0x80);
	Lcd_Write_Index(0xD508);Lcd_Write_Data(0x9D);
	Lcd_Write_Index(0xD509);Lcd_Write_Data(0xCA);
	Lcd_Write_Index(0xD50a);Lcd_Write_Data(0x54);
	Lcd_Write_Index(0xD50b);Lcd_Write_Data(0xEE);
	Lcd_Write_Index(0xD50c);Lcd_Write_Data(0x0B);
	Lcd_Write_Index(0xD50d);Lcd_Write_Data(0x24);
	Lcd_Write_Index(0xD50e);Lcd_Write_Data(0x3B);
	Lcd_Write_Index(0xD50f);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD510);Lcd_Write_Data(0x4F);
	Lcd_Write_Index(0xD511);Lcd_Write_Data(0x61);
	Lcd_Write_Index(0xD512);Lcd_Write_Data(0x72);
	Lcd_Write_Index(0xD513);Lcd_Write_Data(0x82);
	Lcd_Write_Index(0xD514);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD515);Lcd_Write_Data(0x91);
	Lcd_Write_Index(0xD516);Lcd_Write_Data(0x9F);
	Lcd_Write_Index(0xD517);Lcd_Write_Data(0xAD);
	Lcd_Write_Index(0xD518);Lcd_Write_Data(0xBA);
	Lcd_Write_Index(0xD519);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD51a);Lcd_Write_Data(0xC6);
	Lcd_Write_Index(0xD51b);Lcd_Write_Data(0xC8);
	Lcd_Write_Index(0xD51c);Lcd_Write_Data(0xD4);
	Lcd_Write_Index(0xD51d);Lcd_Write_Data(0xE0);
	Lcd_Write_Index(0xD51e);Lcd_Write_Data(0xA5);
	Lcd_Write_Index(0xD51f);Lcd_Write_Data(0xEB);
	Lcd_Write_Index(0xD520);Lcd_Write_Data(0xF6);
	Lcd_Write_Index(0xD521);Lcd_Write_Data(0x01);
	Lcd_Write_Index(0xD522);Lcd_Write_Data(0x0D);
	Lcd_Write_Index(0xD523);Lcd_Write_Data(0xAA);
	Lcd_Write_Index(0xD524);Lcd_Write_Data(0x19);
	Lcd_Write_Index(0xD525);Lcd_Write_Data(0x25);
	Lcd_Write_Index(0xD526);Lcd_Write_Data(0x31);
	Lcd_Write_Index(0xD527);Lcd_Write_Data(0x40);
	Lcd_Write_Index(0xD528);Lcd_Write_Data(0xAA);
	Lcd_Write_Index(0xD529);Lcd_Write_Data(0x51);
	Lcd_Write_Index(0xD52a);Lcd_Write_Data(0x66);
	Lcd_Write_Index(0xD52b);Lcd_Write_Data(0x85);
	Lcd_Write_Index(0xD52c);Lcd_Write_Data(0xBA);
	Lcd_Write_Index(0xD52d);Lcd_Write_Data(0xFE);
	Lcd_Write_Index(0xD52e);Lcd_Write_Data(0xE5);
	Lcd_Write_Index(0xD52f);Lcd_Write_Data(0x2E);
	Lcd_Write_Index(0xD530);Lcd_Write_Data(0x63);
	Lcd_Write_Index(0xD531);Lcd_Write_Data(0xA8);
	Lcd_Write_Index(0xD532);Lcd_Write_Data(0x0F);
	Lcd_Write_Index(0xD533);Lcd_Write_Data(0xED);
	Lcd_Write_Index(0xD534);Lcd_Write_Data(0xFF);

	Lcd_Write_Index(0xD600);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD601);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD602);Lcd_Write_Data(0x11);
	Lcd_Write_Index(0xD603);Lcd_Write_Data(0x2F);
	Lcd_Write_Index(0xD604);Lcd_Write_Data(0x49);
	Lcd_Write_Index(0xD605);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xD606);Lcd_Write_Data(0x5E);
	Lcd_Write_Index(0xD607);Lcd_Write_Data(0x80);
	Lcd_Write_Index(0xD608);Lcd_Write_Data(0x9D);
	Lcd_Write_Index(0xD609);Lcd_Write_Data(0xCA);
	Lcd_Write_Index(0xD60A);Lcd_Write_Data(0x54);
	Lcd_Write_Index(0xD60B);Lcd_Write_Data(0xEE);
	Lcd_Write_Index(0xD60C);Lcd_Write_Data(0x0B);
	Lcd_Write_Index(0xD60D);Lcd_Write_Data(0x24);
	Lcd_Write_Index(0xD60E);Lcd_Write_Data(0x3B);
	Lcd_Write_Index(0xD60F);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD610);Lcd_Write_Data(0x4F);
	Lcd_Write_Index(0xD611);Lcd_Write_Data(0x61);
	Lcd_Write_Index(0xD612);Lcd_Write_Data(0x72);
	Lcd_Write_Index(0xD613);Lcd_Write_Data(0x82);
	Lcd_Write_Index(0xD614);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD615);Lcd_Write_Data(0x91);
	Lcd_Write_Index(0xD616);Lcd_Write_Data(0x9F);
	Lcd_Write_Index(0xD617);Lcd_Write_Data(0xAD);
	Lcd_Write_Index(0xD618);Lcd_Write_Data(0xBA);
	Lcd_Write_Index(0xD619);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xD61a);Lcd_Write_Data(0xC6);
	Lcd_Write_Index(0xD61b);Lcd_Write_Data(0xC8);
	Lcd_Write_Index(0xD61c);Lcd_Write_Data(0xD4);
	Lcd_Write_Index(0xD61d);Lcd_Write_Data(0xE0);
	Lcd_Write_Index(0xD61e);Lcd_Write_Data(0xA5);
	Lcd_Write_Index(0xD61f);Lcd_Write_Data(0xEB);
	Lcd_Write_Index(0xD620);Lcd_Write_Data(0xF6);
	Lcd_Write_Index(0xD621);Lcd_Write_Data(0x01);
	Lcd_Write_Index(0xD622);Lcd_Write_Data(0x0D);
	Lcd_Write_Index(0xD623);Lcd_Write_Data(0xAA);
	Lcd_Write_Index(0xD624);Lcd_Write_Data(0x19);
	Lcd_Write_Index(0xD625);Lcd_Write_Data(0x25);
	Lcd_Write_Index(0xD626);Lcd_Write_Data(0x31);
	Lcd_Write_Index(0xD627);Lcd_Write_Data(0x40);
	Lcd_Write_Index(0xD628);Lcd_Write_Data(0xAA);
	Lcd_Write_Index(0xD629);Lcd_Write_Data(0x51);
	Lcd_Write_Index(0xD62a);Lcd_Write_Data(0x66);
	Lcd_Write_Index(0xD62b);Lcd_Write_Data(0x85);
	Lcd_Write_Index(0xD62c);Lcd_Write_Data(0xBA);
	Lcd_Write_Index(0xD62d);Lcd_Write_Data(0xFE);
	Lcd_Write_Index(0xD62e);Lcd_Write_Data(0xE5);
	Lcd_Write_Index(0xD62f);Lcd_Write_Data(0x2E);
	Lcd_Write_Index(0xD630);Lcd_Write_Data(0x63);
	Lcd_Write_Index(0xD631);Lcd_Write_Data(0xA8);
	Lcd_Write_Index(0xD632);Lcd_Write_Data(0x0F);
	Lcd_Write_Index(0xD633);Lcd_Write_Data(0xED);
	Lcd_Write_Index(0xD634);Lcd_Write_Data(0xFF);

	Lcd_Write_Index(0xB000);Lcd_Write_Data(0x0C);
	Lcd_Write_Index(0xB001);Lcd_Write_Data(0x0C);
	Lcd_Write_Index(0xB002);Lcd_Write_Data(0x0C);
	
	Lcd_Write_Index(0xB100);Lcd_Write_Data(0x0C);
	Lcd_Write_Index(0xB101);Lcd_Write_Data(0x0C);
	Lcd_Write_Index(0xB102);Lcd_Write_Data(0x0C);

	Lcd_Write_Index(0xB600);Lcd_Write_Data(0x34);
	Lcd_Write_Index(0xB601);Lcd_Write_Data(0x34);
	Lcd_Write_Index(0xB602);Lcd_Write_Data(0x34);

	Lcd_Write_Index(0xB700);Lcd_Write_Data(0x24);
	Lcd_Write_Index(0xB701);Lcd_Write_Data(0x24);
	Lcd_Write_Index(0xB702);Lcd_Write_Data(0x24);

	Lcd_Write_Index(0xB800);Lcd_Write_Data(0x24);
	Lcd_Write_Index(0xB801);Lcd_Write_Data(0x24);
	Lcd_Write_Index(0xB802);Lcd_Write_Data(0x24);

	Lcd_Write_Index(0xB900);Lcd_Write_Data(0x34);
	Lcd_Write_Index(0xB901);Lcd_Write_Data(0x34);
	Lcd_Write_Index(0xB902);Lcd_Write_Data(0x34);

	Lcd_Write_Index(0xBA00);Lcd_Write_Data(0x24);
	Lcd_Write_Index(0xBA01);Lcd_Write_Data(0x24);
	Lcd_Write_Index(0xBA02);Lcd_Write_Data(0x24);

	Lcd_Write_Index(0xBC00);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xBC01);Lcd_Write_Data(0x80);
	Lcd_Write_Index(0xBC02);Lcd_Write_Data(0x20);

	Lcd_Write_Index(0xBD00);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xBD01);Lcd_Write_Data(0x80);
	Lcd_Write_Index(0xBD02);Lcd_Write_Data(0x20);

	Lcd_Write_Index(0xBE00);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xBE01);Lcd_Write_Data(0x60);

	Lcd_Write_Index(0xF000);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xF001);Lcd_Write_Data(0xAA);
	Lcd_Write_Index(0xF002);Lcd_Write_Data(0x52);
	Lcd_Write_Index(0xF003);Lcd_Write_Data(0x08);
	Lcd_Write_Index(0xF004);Lcd_Write_Data(0x00);

	Lcd_Write_Index(0xB400);Lcd_Write_Data(0x10);

	Lcd_Write_Index(0xB600);Lcd_Write_Data(0x02);

	Lcd_Write_Index(0xB100);Lcd_Write_Data(0xF8);


	Lcd_Write_Index(0xB500);Lcd_Write_Data(0x6B);

	Lcd_Write_Index(0xB700);Lcd_Write_Data(0x22);
	Lcd_Write_Index(0xB701);Lcd_Write_Data(0x22);

	Lcd_Write_Index(0xC800);Lcd_Write_Data(0x01);
	Lcd_Write_Index(0xC801);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0xC802);Lcd_Write_Data(0x2C);
	Lcd_Write_Index(0xC803);Lcd_Write_Data(0x13);
	Lcd_Write_Index(0xC804);Lcd_Write_Data(0x2C);
	Lcd_Write_Index(0xC805);Lcd_Write_Data(0x13);
	Lcd_Write_Index(0xC806);Lcd_Write_Data(0x2C);
	Lcd_Write_Index(0xC807);Lcd_Write_Data(0x13);
	Lcd_Write_Index(0xC808);Lcd_Write_Data(0x2C);
	Lcd_Write_Index(0xC809);Lcd_Write_Data(0x13);
	Lcd_Write_Index(0xC80a);Lcd_Write_Data(0x3F);
	Lcd_Write_Index(0xC80b);Lcd_Write_Data(0x3F);
	Lcd_Write_Index(0xC80c);Lcd_Write_Data(0x3F);
	Lcd_Write_Index(0xC80d);Lcd_Write_Data(0x3F);
	Lcd_Write_Index(0xC80e);Lcd_Write_Data(0x3F);
	Lcd_Write_Index(0xC80f);Lcd_Write_Data(0x3F);
	Lcd_Write_Index(0xC810);Lcd_Write_Data(0x3F);
	Lcd_Write_Index(0xC811);Lcd_Write_Data(0x3F);

	Lcd_Write_Index(0xB800);Lcd_Write_Data(0x01);
	Lcd_Write_Index(0xB801);Lcd_Write_Data(0x03);
	Lcd_Write_Index(0xB802);Lcd_Write_Data(0x03);
	Lcd_Write_Index(0xB803);Lcd_Write_Data(0x03);

	Lcd_Write_Index(0xBC00);Lcd_Write_Data(0x05);
	Lcd_Write_Index(0xBC01);Lcd_Write_Data(0x05);
	Lcd_Write_Index(0xBC02);Lcd_Write_Data(0x05);

	Lcd_Write_Index(0xD000);Lcd_Write_Data(0x01);

	Lcd_Write_Index(0xBA00);Lcd_Write_Data(0x01);

	Lcd_Write_Index(0xBD00);Lcd_Write_Data(0x01);
	Lcd_Write_Index(0xBD01);Lcd_Write_Data(0x10);
	Lcd_Write_Index(0xBD02);Lcd_Write_Data(0x07);
	Lcd_Write_Index(0xBD03);Lcd_Write_Data(0x07);

	Lcd_Write_Index(0xBE00);Lcd_Write_Data(0x01);
	Lcd_Write_Index(0xBE01);Lcd_Write_Data(0x10);
	Lcd_Write_Index(0xBE02);Lcd_Write_Data(0x07);
	Lcd_Write_Index(0xBE03);Lcd_Write_Data(0x07);

	Lcd_Write_Index(0xBF00);Lcd_Write_Data(0x01);
	Lcd_Write_Index(0xBF01);Lcd_Write_Data(0x10);
	Lcd_Write_Index(0xBF02);Lcd_Write_Data(0x07);
	Lcd_Write_Index(0xBF03);Lcd_Write_Data(0x07);

	Lcd_Write_Index(0xF000);Lcd_Write_Data(0x55);
	Lcd_Write_Index(0xF001);Lcd_Write_Data(0xAA);
	Lcd_Write_Index(0xF002);Lcd_Write_Data(0x52);
	Lcd_Write_Index(0xF003);Lcd_Write_Data(0x08);
	Lcd_Write_Index(0xF004);Lcd_Write_Data(0x02);

	Lcd_Write_Index(0xF600);Lcd_Write_Data(0x60);

	Lcd_Write_Index(0xEB00);Lcd_Write_Data(0x01);
	Lcd_Write_Index(0xEB01);Lcd_Write_Data(0x26);
	Lcd_Write_Index(0xEB02);Lcd_Write_Data(0x01);
	Lcd_Write_Index(0xEB03);Lcd_Write_Data(0xC8);
	Lcd_Write_Index(0xEB04);Lcd_Write_Data(0x89);

	
	Lcd_Write_Index(0x3500);Lcd_Write_Data(0x00);
	

	//Lcd_Write_Index(0x3600);Lcd_Write_Data(0x0);

	/*(2A00h): Column Address Set  
	1st parameter 1 1 ^ x 0   0   0   0   0   0   0   SC8 00
	2nd parameter 1 1 ^ x SC7 SC6 SC5 SC4 SC3 SC2 SC1 SC0 00
	3rd parameter 1 1 ^ x 0   0   0   0   0   0   0   EC8 01
	4th parameter 1 1 ^ x EC7 EC6 EC5 EC4 EC3 EC2 EC1 EC0 DF
	*/
	Lcd_Write_Index(0x2A00);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0x2A01);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0x2A02);Lcd_Write_Data(0x000F);			//� - ������ ������ �������� ������ �� �
	Lcd_Write_Index(0x2A03);Lcd_Write_Data(0x00F0);
	/*(2A00h): Column Address Set  
	1st parameter 1 1 ^ x 0   0   0   0   0   0   0   SC8 00
	2nd parameter 1 1 ^ x SC7 SC6 SC5 SC4 SC3 SC2 SC1 SC0 00
	3rd parameter 1 1 ^ x 0   0   0   0   0   0   0   EC8 01
	4th parameter 1 1 ^ x EC7 EC6 EC5 EC4 EC3 EC2 EC1 EC0 DF
	*/
	Lcd_Write_Index(0x2B00);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0x2B01);Lcd_Write_Data(0x00);
	Lcd_Write_Index(0x2B02);Lcd_Write_Data(0x0002);			//Y - ������ ������ �������� ������ �� Y
	Lcd_Write_Index(0x2B03);Lcd_Write_Data(0x0000);

	Lcd_Write_Index(0x3A00);Lcd_Write_Data(0x55);
	/*
	
	
// ��� �������� ��
#define VERTICAL_FLIP							  (1 << 0)		//VERTICAL_FLIP
#define HORIZONTAL_FLIP							(1 << 1)		//HORIZONTAL_FLIP
#define REFRESH_RIGTH_TO_LEFT				(1 << 2)		//Vertical Refresh Order
#define RGB_ORDER										(1 << 3)		//Vertical Refresh Order
#define REFRESH_BOTTOM_TO_TOP				(1 << 4)		//Vertical Refresh Order
#define ROW_COLUMN_EXCH							(1 << 5)		//Row/Column Order
#define RIGTH_TO_LEFT								(1 << 6)		//Column Address Order
#define BOTTOM_TO_TOP								(1 << 7)		//Row Address Order

	*/
  Lcd_Write_Index(0x3600);Lcd_Write_Data(
																				 //(VERTICAL_FLIP)					|					
																				 //(HORIZONTAL_FLIP)|								
                                         //(REFRESH_RIGTH_TO_LEFT)|			
                                         //(RGB_ORDER)								|	
                                        // (REFRESH_BOTTOM_TO_TOP)	|	
                                         (ROW_COLUMN_EXCH)		|					// 
                                         (RIGTH_TO_LEFT)				
                                        //(BOTTOM_TO_TOP)					
																				 );
	Lcd_Write_Index(0X1100); //
	for(uint32_t i = 0; i < 100000; i++){}//MDELAY(10); 
	Lcd_Write_Index(0X2900); //
	for(uint32_t i = 0; i < 10000; i++){}//MDELAY(10); 
	
	

	Lcd_Write_Index(0X2C00); //
	for(uint32_t i = 0; i < 10000; i++){}//MDELAY(10); 
		
//	Lcd_Write_Index(0X2300);	 Lcd_Write_Data(0x31);
//	Lcd_Write_Index(0X2200);	 Lcd_Write_Data(0x22);			
		
	SET_BL;
	LCD_SetFont(&Font16x24);
}
void init_RCC(void){
	
	RCC_ClocksTypeDef RCC_init;
	RCC_ClockSecuritySystemCmd(ENABLE);
	RCC_HSEConfig(RCC_HSE_ON); // �������� ��������� � ������� �������...
	while(!RCC_WaitForHSEStartUp()); // ... � ��� ����� �� ���������������
	RCC_PLLCmd(DISABLE); // ��������� ���� ����� �����������������
	while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY));
	RCC_PLLConfig(RCC_PLLSource_HSE, 50, 432, 2, 4); // ����������� ���.���� �� 168 ���
														// |   |  |     |   |
													  // |   |  |     |   +->   (Q) �����. ��������� ���� ��� ������������ OTG FS, SDIO � RNG (�.�. ��������� �� 4 �� 15)
														// |   |  |     +----->   (�) �����. ������� ����� ���� ��� ������� ����. ������� (SYSCLK) (2, 4, 6 ��� 8)
														// |   |  +--------->     (N) �����. �����. ���� (�.�. 192 432)
														// |   +------------->    (�) �����. ����������� ������� �� ���� (�.�. ��������� �� 0 �� 63)
														// +---------------------------> �������� ��� ��������� ���� - ��������� ���������
	RCC_PLLCmd(ENABLE); 					               			// �������� �������� ����
	while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) != 0); 	// ������� ��� ���������� 
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK); // �������� ��������� ������� - ����
	RCC_HCLKConfig(RCC_SYSCLK_Div2); // ������� HCLK (���� AHB) ����� ����. (SYSCLK) �.�. /1
	//SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK); // �������� ��� SysTick`� ����� ��������� �������
	RCC_PCLK1Config(RCC_HCLK_Div8); // ����� ��������� ������� �� 4 � �������� ������������ ������� ��� ���1 (42 ���)
	RCC_PCLK2Config(RCC_HCLK_Div4); // ����� ��������� ������� �� 2 � �������� ������������ ������� ��� ���2 (84 ���)
//	RCC_PLLI2SCmd(DISABLE); // ��������� ���� I2C ����� �����������������
//	RCC_PLLI2SConfig(240,2); // ����������� ���� I2C �� ����. ������� (192 ���)
	// | |
	// | +--> // �����. ������������ ���� ��� I2C
	// +-----> // ����������� ��������� ���� ��� I2C
	RCC_PLLI2SCmd(ENABLE); // �������� ���� ��� I2C ��� ������� ������������� ������� =)
	RCC_GetClocksFreq(&RCC_init);

//	RCC_DeInit();
//	RCC_HSEConfig( RCC_HSE_ON);	
//	if ( RCC_WaitForHSEStartUp() == ERROR ) {
//		for (;;) __NOP();
//	} else {
//		RCC_PLLConfig ( RCC_PLLSource_HSE, 0, 400, 2, 4 );
//		RCC_PLLCmd ( ENABLE );

//		RCC_SYSCLKConfig ( RCC_SYSCLKSource_PLLCLK ); // RCC_SYSCLKSource_PLLCLK
//	}
}
void init_USART(void){
	
  USART_InitTypeDef USART_InitTypeDef_BLE;
	GPIO_InitTypeDef  GPIO_InitTypeDef_USART;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	/******************** ������������ SPI ******************************************/
	GPIO_StructInit(&GPIO_InitTypeDef_USART);
	
		GPIO_InitTypeDef_USART.GPIO_Mode 	= GPIO_Mode_AF;	
		GPIO_InitTypeDef_USART.GPIO_Pin 	= GPIO_Pin_9 | GPIO_Pin_10;
		GPIO_InitTypeDef_USART.GPIO_Speed = GPIO_Speed_50MHz;
	
  GPIO_Init(GPIOA, &GPIO_InitTypeDef_USART);
	
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9,   GPIO_AF_USART1);			
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10,  GPIO_AF_USART1);			
	
	USART_InitTypeDef_BLE.USART_BaudRate = 9600;//115200;
	USART_InitTypeDef_BLE.USART_WordLength = USART_WordLength_8b;
	USART_InitTypeDef_BLE.USART_StopBits = USART_StopBits_1;
	USART_InitTypeDef_BLE.USART_Parity = USART_Parity_No;
	USART_InitTypeDef_BLE.USART_Mode = USART_Mode_Tx;
	USART_InitTypeDef_BLE.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	
	USART_Init(USART1, &USART_InitTypeDef_BLE);
	
//	USART_ITConfig(USART1, USART_IT_TXE | USART_IT_RXNE, ENABLE);
	USART_Cmd(USART1, ENABLE);
}
void I2C1_init(void){
	
  I2C_InitTypeDef   I2C_InitStructure;
	GPIO_InitTypeDef 	GPIO_InitStructure_I2C1;
  /* Enable the I2C periph */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1,  ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	
  /* I2C configuration -------------------------------------------------------*/
  I2C_InitStructure.I2C_Mode 								= I2C_Mode_I2C;
	I2C_InitStructure.I2C_DutyCycle 					= I2C_DutyCycle_2;
	I2C_InitStructure.I2C_ClockSpeed 					= 400000;
  I2C_InitStructure.I2C_OwnAddress1 				= 0x0;
  I2C_InitStructure.I2C_Ack 								= I2C_Ack_Enable;//I2C_Ack_Disable;				///!!!!!!!!!!!!!!!!!!
  I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	
  I2C_Init(I2C_TS, &I2C_InitStructure);
  
	/* I2C SCK, SDA pin configuration ----------------------------------------*/
  GPIO_InitStructure_I2C1.GPIO_Pin 		= GPIO_Pin_6 | GPIO_Pin_7;
  GPIO_InitStructure_I2C1.GPIO_Mode 	= GPIO_Mode_AF;
  GPIO_InitStructure_I2C1.GPIO_OType  = GPIO_OType_OD;
  GPIO_InitStructure_I2C1.GPIO_PuPd  	= GPIO_PuPd_UP;
  GPIO_InitStructure_I2C1.GPIO_Speed 	= GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure_I2C1);
	 
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6,  GPIO_AF_I2C1);					
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7,  GPIO_AF_I2C1);			
	
  I2C_Cmd(I2C_TS, ENABLE);
}
void init_DMA(){
	/*
		�������������� �� �� ��������. 
		��� �������� � ��������� ��� ����� ������ ��������, ���� � �������. 
		�������� -> �������� -> ��������� � ��� � ����� 
	*/
//	NVIC_InitTypeDef NVIC_InitStructure;
 // DMA_InitTypeDef  DMA_InitStructure;
  __IO uint32_t    Timeout = TIMEOUT_MAX;
//  DMA_InitTypeDef  DMA_InitStructure;		//��������� ���������� ����� ��� ��� ���������� � ���-�� ������������� � � ���-�� �������� � ������ �������				
 
  RCC_AHB1PeriphClockCmd(DMA_STREAM_CLOCK, ENABLE);
  DMA_DeInit(DMA_STREAM);															/* ���������� �������� DMA */
  while (DMA_GetCmdStatus(DMA_STREAM) != DISABLE){}		/* ��������� �������� �� ������ ����� DMA. ��� ������� ����� �� ����������� ��� ��������� ��� */
 
  /* ��������� ������ DMA */
		DMA_InitStructure.DMA_Channel 						= DMA_CHANNEL;  

		DMA_InitStructure.DMA_DIR 								= DMA_DIR_MemoryToMemory;

		DMA_InitStructure.DMA_PeripheralInc				= DMA_PeripheralInc_Disable;//DMA_PeripheralInc_Enable;
		DMA_InitStructure.DMA_MemoryInc 					= DMA_MemoryInc_Disable;//DMA_MemoryInc_Enable;
		DMA_InitStructure.DMA_PeripheralDataSize 	= DMA_PeripheralDataSize_HalfWord;
		DMA_InitStructure.DMA_MemoryDataSize			= DMA_MemoryDataSize_HalfWord;
		DMA_InitStructure.DMA_Mode 								= DMA_Mode_Normal;
		DMA_InitStructure.DMA_Priority 						= DMA_Priority_High;
		DMA_InitStructure.DMA_FIFOMode 						= DMA_FIFOMode_Disable;         
		DMA_InitStructure.DMA_FIFOThreshold 			= DMA_FIFOThreshold_Full;
		DMA_InitStructure.DMA_MemoryBurst 				= DMA_MemoryBurst_Single;
		DMA_InitStructure.DMA_PeripheralBurst 		= DMA_PeripheralBurst_Single;
//  DMA_Init(DMA_STREAM, &DMA_InitStructure);
// 
//  DMA_ITConfig(DMA_STREAM, DMA_IT_TC, ENABLE);	/* ��������� ���������� �� ��������� �������� ������ */
//  DMA_Cmd(DMA_STREAM, ENABLE); 									/* �������� DMA */
//  Timeout = TIMEOUT_MAX;  											/* ��������� ���������� �� DMA */
//  
//	while ((DMA_GetCmdStatus(DMA_STREAM) != ENABLE) && (Timeout-- > 0)) {}
//		
//		if (Timeout == 0) 										/* ���� ������� ������ ������� �������, ������ �������� ������ */
//		{
//			while (1){} 												/* ��� ������ ��� � ����������� ���� */	
//		}
  /* ����������� ���������� �� ��������� �������� */
//  NVIC_InitStructure.NVIC_IRQChannel = DMA_STREAM_IRQ;
//  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_Init(&NVIC_InitStructure);
	
	/* Enable DMA clock */

//	RCC_AHB1PeriphClockCmd(DMA_STREAM_CLOCK, ENABLE); 
//	/* Reset DMA Stream registers (for debug purpose) */
//	DMA_DeInit(DMA_STREAM);

//	/* Check if the DMA Stream is disabled before enabling it.
//		 Note that this step is useful when the same Stream is used multiple times:
//		 enabled, then disabled then re-enabled... In this case, the DMA Stream disable
//		 will be effective only at the end of the ongoing data transfer and it will
//		 not be possible to re-configure it before making sure that the Enable bit
//			has been cleared by hardware. If the Stream is used only once, this step might
//			be bypassed. */
//	
//	while (DMA_GetCmdStatus(DMA_STREAM) != DISABLE) {}

//	/* Configure DMA Stream */
//	DMA_InitStructure.DMA_Channel = DMA_CHANNEL; 
//	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)SRC_Const_Buffer; // ��� ��� ����� ������� ������
//	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)DST_Buffer;  // ��� ��� ����� ������ ���
//	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToMemory;
//	DMA_InitStructure.DMA_BufferSize = (uint32_t)BUFFER_SIZE; // ���������� �����������
//	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Enable;
//	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
//	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word; //������ �����������
//	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
//	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
//	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
//	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;       
//	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
//	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
//	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
//	DMA_Init(DMA_STREAM, &DMA_InitStructure);
//	 
//	/* Enable DMA Stream Transfer Complete interrupt */
//	DMA_ITConfig(DMA_STREAM, DMA_IT_TC, ENABLE);

//	/* DMA Stream enable */
//	DMA_Cmd(DMA_STREAM, ENABLE);
}
void init_DMA2(){ 	//http://www.mytutorialcafe.com/experiment%20microcontroller%20ARM%20STM32F4%20DMA%20memory%20to%20memory.htm

		 //init DMA --> lihat tabel di ref manual
//		 DMA_InitTypeDef DMA_InitStructure;
//		 DMA_StructInit(&DMA_InitStructure);
//		 RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
////		 //
//		 DMA_DeInit(DMA2_Stream0);
//		 DMA_InitStructure.DMA_Channel 					= DMA_Channel_0;
////		 DMA_InitStructure.DMA_DIR 							= DMA_DIR_MemoryToMemory;
//		 DMA_InitStructure.DMA_Mode							= DMA_Mode_Normal;
//		 DMA_InitStructure.DMA_Priority 				= DMA_Priority_High;
//		 DMA_InitStructure.DMA_MemoryBurst 			= DMA_MemoryBurst_Single;
//		 DMA_InitStructure.DMA_PeripheralBurst 	= DMA_PeripheralBurst_Single;
//		 /* config of memory */
//		 DMA_InitStructure.DMA_FIFOMode 					= DMA_FIFOMode_Disable;
//		 DMA_InitStructure.DMA_FIFOThreshold 			= DMA_FIFOThreshold_Full;
//		 DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
//		 DMA_InitStructure.DMA_MemoryInc 					= DMA_MemoryInc_Enable;
//		 /* config of peripheral */
//		 DMA_InitStructure.DMA_MemoryDataSize 		= DMA_MemoryDataSize_Word;
//		 DMA_InitStructure.DMA_MemoryDataSize 		= DMA_MemoryDataSize_Byte;
//		 DMA_InitStructure.DMA_MemoryInc 					= DMA_MemoryInc_Enable;
//		 DMA_InitStructure.DMA_PeripheralInc 			= DMA_PeripheralInc_Enable;
//		 DMA_InitStructure.DMA_BufferSize 				= ARRAYSIZE;
//		 DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&destination[0];
//		 DMA_InitStructure.DMA_Memory0BaseAddr 		= (uint32_t)&source[0];
//		 DMA_Init(DMA2_Stream0,&DMA_InitStructure);
//		 DMA_ITConfig(DMA2_Stream0,DMA_IT_TC,ENABLE);
//		 //
//		 NVIC_InitTypeDef NVIC_InitStructure;
//		 NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream0_IRQn;
//		 NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority =0;
//		 NVIC_InitStructure.NVIC_IRQChannelSubPriority =0;
//		 NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//		 NVIC_Init(&NVIC_InitStructure); 
//		
//	DMA_FlowControllerConfig(DMA2_Stream0, DMA_FlowCtrl_Peripheral);		 
}
void init_DMA3(){

//	DMA_InitTypeDef DMA_InitStructure;
//	DMA_InitStructure.DMA_Channel 						= DMA_Channel_0; //RX
//	DMA_InitStructure.DMA_PeripheralBaseAddr	= (uint32_t)&destination[0];
//	DMA_InitStructure.DMA_Memory0BaseAddr 		= (uint32_t)&source[0];
//	DMA_InitStructure.DMA_DIR 								= DMA_DIR_MemoryToMemory;									// ������ �� ������ � ������
//	DMA_InitStructure.DMA_BufferSize 					= (uint32_t) ARRAYSIZE/2/2; 				// ���������� ���� � ������
//	DMA_InitStructure.DMA_PeripheralInc 			= DMA_PeripheralInc_Enable;					// �� �������������� ����� � ��������� ��� ��� �� ������
//	DMA_InitStructure.DMA_MemoryInc 					= DMA_MemoryInc_Enable;							// �������������� ����� � ������
//	DMA_InitStructure.DMA_PeripheralDataSize 	= DMA_PeripheralDataSize_HalfWord;	// �������� �� ��������� HalfWord-16 ���
//	DMA_InitStructure.DMA_MemoryDataSize 			= DMA_MemoryDataSize_HalfWord;			//DMA_MemoryDataSize_Byte;// � ����� � ��� ���� � ������ DMA_PeripheralDataSize_HalfWord;
//	DMA_InitStructure.DMA_Mode 								= DMA_Mode_Normal; 									// ����� ������ FIFO �����
//	DMA_InitStructure.DMA_Priority 						= DMA_Priority_High;
//	DMA_InitStructure.DMA_FIFOMode 						= DMA_FIFOMode_Disable;							// DMA_FIFOMode_Enable;// ����� FIFO ��������
//	DMA_InitStructure.DMA_FIFOThreshold 			= DMA_FIFOThreshold_Full; 					//���������� ��� ���������� 1/2 ������
//	DMA_InitStructure.DMA_MemoryBurst 				= DMA_MemoryBurst_Single;					//DMA_MemoryBurst_Single;
//	DMA_InitStructure.DMA_PeripheralBurst 		= DMA_PeripheralBurst_Single;// DMA_PeripheralBurst_Single;
//	DMA_Init(DMA2_Stream0, &DMA_InitStructure);
//	DMA_Cmd(DMA2_Stream0, ENABLE);

//	DMA_ClearITPendingBit(DMA2_Stream0, DMA_IT_TCIF0);
//	DMA_ClearITPendingBit(DMA2_Stream0, DMA_IT_HTIF0);

//while ((DMA_GetCmdStatus(DMA2_Stream0) != ENABLE) ){}
}
void TIM_Config(void){

  GPIO_InitTypeDef GPIO_InitStructure;

  /* TIM3 clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

  /* GPIOC clock enable */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
  
  /* GPIOC Configuration: TIM3 CH1 (PC6), TIM3 CH2 (PC7), TIM3 CH3 (PC8) and TIM3 CH4 (PC9) */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_Init(GPIOC, &GPIO_InitStructure); 

  /* Connect TIM3 pins to AF2 */  
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_TIM3);
//  GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_TIM3); 
//  GPIO_PinAFConfig(GPIOC, GPIO_PinSource8, GPIO_AF_TIM3);
//  GPIO_PinAFConfig(GPIOC, GPIO_PinSource9, GPIO_AF_TIM3); 



	
	  /* Compute the prescaler value */
  PrescalerValue = (uint16_t) ((168000000 /2) / 21000000) - 1;

  /* Time base configuration */
  TIM_TimeBaseStructure.TIM_Period = 665;
  TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

  TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

  /* PWM1 Mode configuration: Channel1 */
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = CCR1_Val;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

  TIM_OC1Init(TIM3, &TIM_OCInitStructure);

  TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);
  TIM_ARRPreloadConfig(TIM3, ENABLE);

	TIM_Cmd(TIM3, ENABLE);
}
//void TIM_Config(void)
//{
//  GPIO_InitTypeDef GPIO_InitStructure;

//  /* TIM3 clock enable */
//  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

//  /* GPIOC clock enable */
//  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
//  
//  /* GPIOC Configuration: TIM3 CH1 (PC6), TIM3 CH2 (PC7), TIM3 CH3 (PC8) and TIM3 CH4 (PC9) */
//  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
//  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
//  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
//  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
//  GPIO_Init(GPIOC, &GPIO_InitStructure); 

//  /* Connect TIM3 pins to AF2 */  
////  GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_TIM3);
//  GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_TIM3); 
//////GPIO_PinAFConfig(GPIOC, GPIO_PinSource9, GPIO_AF_TIM3); 
//	
//		
//  /* Compute the prescaler value */
//  PrescalerValue = (uint16_t) ((SystemCoreClock /2) / 21000000) - 1;

//  /* Time base configuration */
//  TIM_TimeBaseStructure.TIM_Period = 2;
//  TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue;
//  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
//  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

//  TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

//  /* PWM1 Mode configuration: Channel1 */
//  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
//  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
//  TIM_OCInitStructure.TIM_Pulse = CCR1_Val;
//  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

//  TIM_OC1Init(TIM3, &TIM_OCInitStructure);

//  TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);

//  /* PWM1 Mode configuration: Channel2 */
//  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
//  TIM_OCInitStructure.TIM_Pulse = CCR2_Val;

//  TIM_OC2Init(TIM3, &TIM_OCInitStructure);

//  TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);

//  /* PWM1 Mode configuration: Channel3 */
//  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
//  TIM_OCInitStructure.TIM_Pulse = CCR3_Val;

//  TIM_OC3Init(TIM3, &TIM_OCInitStructure);

//  TIM_OC3PreloadConfig(TIM3, TIM_OCPreload_Enable);

//  /* PWM1 Mode configuration: Channel4 */
//  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
//  TIM_OCInitStructure.TIM_Pulse = CCR4_Val;

//  TIM_OC4Init(TIM3, &TIM_OCInitStructure);

//  TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Enable);

//  TIM_ARRPreloadConfig(TIM3, ENABLE);

//  /* TIM3 enable counter */
//  TIM_Cmd(TIM3, ENABLE);
//	
//}
