#ifndef __INIT_H__
#define __INIT_H__

#include "stm32f4xx.h" 
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_usart.h"
#include "stm32f4xx_i2c.h"
#include "fonts.h"
#include "stm32f4xx_fsmc.h"
#include "stm32f4xx_i2c.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_syscfg.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_rtc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_dma.h"
#include "stm32f4xx_tim.h"

#include "LCD.h"



#define I2C_TS						I2C1

#define F_CPU 			36000000UL	// �������� � ��� 72���/2
#define TimerTick  	F_CPU/10000-1	// ��� ����� ��������	///10���

#define SET_BL   	GPIO_SetBits  (GPIOC, GPIO_Pin_7)
#define RESET_BL 	GPIO_ResetBits(GPIOC, GPIO_Pin_7)

/* ��� �������� ��*/
#define VERTICAL_FLIP							  (1 << 0)		//VERTICAL_FLIP
#define HORIZONTAL_FLIP							(1 << 1)		//HORIZONTAL_FLIP
#define REFRESH_RIGTH_TO_LEFT				(1 << 2)		//Vertical Refresh Order
#define RGB_ORDER										(1 << 3)		//Vertical Refresh Order
#define REFRESH_BOTTOM_TO_TOP				(1 << 4)		//Vertical Refresh Order
#define ROW_COLUMN_EXCH							(1 << 5)		//Row/Column Order
#define RIGTH_TO_LEFT								(1 << 6)		//Column Address Order
#define BOTTOM_TO_TOP								(1 << 7)		//Row Address Order

#define DMA_CHANNEL              DMA_Channel_0
#define DMA_STREAM               DMA2_Stream0
#define TIMEOUT_MAX              10000 /* Maximum timeout value */
#define DMA_STREAM_CLOCK         RCC_AHB1Periph_DMA2




/*------ Prototype function --------------------------------------------*/
void PORT_init						(void);
void SysTick_init					(void);
void init_LED							(void);
void init_PE1							(void);
void init_MCO 						(void);
void init_FSMC						(void);
void init_RM68120					(void);
void init_RM68120_2				(void);
void init_RCC							(void);
void init_USART						(void);
void Delay_ms_RCC					(uint32_t ms);
void init_DMA							(void);
void init_DMA2						(void);
void init_DMA3						(void);
void LCD_SetFont					(sFONT *fonts);
void I2C1_init						(void);			
void EXTI_PF4_init				(void);
void TIM_Config						(void);

#endif // __RTC_H__
