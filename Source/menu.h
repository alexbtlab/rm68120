#ifndef __MENU_H__
#define __MENU_H__

#include "stm32f4xx.h" 
#include "stdlib.h"
#include "stdio.h"
#include "grlib.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_syscfg.h"
#include "tm_stm32f4_rtc.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_gpio.h"

#include "LCD.h"

#define countof(a) (sizeof(a) / sizeof(*(a)))

#define  DELAY(x)			for(uint32_t i = 0; i < x; i++){}
	

#define	GREEN_RGB565 	 0x7E0
#define	RED_RGB565   0xF800
#define NUM_BANK_GRAM 0x5

#define GRAPHICS_COLOR_BLUE						 0x000000FF
#define GRAPHICS_COLOR_WHITE					 0x00FFFFFF
//#define GRAPHICS_COLOR_GREEN					 0xFF00

#define MAINCONTROL             0
#define TIMER_ITEM              1
#define SVENT_ITEM              2
#define SETTING_ITEM            3
#define DEBUG_ITEM              4
#define SMODE_ITEM              5
#define DATAGRW_ITEM            6
#define TIMEDATE_ITEM           7
#define TDATA_ITEM              8

#define DATA_SUBITEM            9
#define MOUNTH_SUBITEM				  10
#define YEAR_SUBITEM 						11
#define DAY_SUBITEM 						12
#define TIME_SUBITEM 						13
#define MIN_SUBITEM 						14
#define ALARM_SUBITEM 					15

#define LIGTH_SUBITEM 					16

#define  MAX_MENU_LEVELS 5

	extern	char  str[11];
	extern	char *pstr;
/*---------------------------------------------------*/
	typedef void (* tMenuFunc)(void);
	typedef struct sMenuItem *tMenuItem;
	typedef struct sMenu *tMenu;

	static tMenuItem psMenuItem, psCurrentMenuItem;
	static tMenu psPrevMenu[MAX_MENU_LEVELS];
	static tMenu psCurrentMenu;
	
  struct sMenuItem {			// ��������� ������ ����
  
  char *psTitle;					// �������� ������ ���� 
  tMenuFunc pfMenuFunc;		// ����������� ������� ������
  tMenu psSubMenu;				// ������� �������. ���� ��� NULL, ���� ���� ��������� �� ����
};

struct sMenu {						// ��������� ����
  
  char *psTitle;					// ��������� ����
  tMenuItem psItems;			// ��������� �� ������ ����
  u32 nItems;							// ���������� ������� ����
};
/*---------------------------------------------------*/	
typedef void (* tTimeFunc)(void);
	typedef struct sTimeItem *tTimeItem;
	typedef struct sTime *tTime;

	static tTimeItem psTimeItem, psCurrentTimeItem;
	static tTime psPrevTime[10];
	static tTime psCurrentTime;

	struct sTimeItem {

	char *psTitle;
	tTimeFunc pfTimeFunc;
	tTime psSubTime;
};

struct sTime {
  char *psTitle;
  tTimeItem psItems;
  u32 nItems;
};

#define STATE_MAINMENU 0
#define STATE_SUBMENU 1
#define STATE_SUBMENU_SETTING  2

#define MESS_LAMP_TOP 0
#define MESS_LAMP_BOT 1
#define MESS_LAMP_SYSBOX 2
#define MESS_VENTFLOW 3
#define MESS_VENTVEGA 4

	extern Graphics_Context g_sContext;
	extern TM_RTC_Time_t datatime;

/*--------------------------------------------------------------------------------------------------------------------------------------------*/
void Menu_Init(void);
void SetMenu(tMenu pMenu, tMenuItem pMenuItem);
void DisplayMenu(uint32_t Top_TAB, uint32_t Left_TAB);
void DisplayMainControl(uint32_t Top_TAB, uint32_t Left_TAB);
void DisplaySubMenuTime(uint32_t Top_TAB, uint32_t Left_TAB);
void DisplaySubMenuData(uint32_t Top_TAB, uint32_t Left_TAB);
void DisplaySubMenuAlarm(uint32_t Top_TAB, uint32_t Left_TAB);
void drawMainMenu(void);
void draw_button(uint32_t X, uint32_t Y, uint32_t length, uint32_t height, FunctionalState State, uint8_t *str);
void draw_rect(uint32_t X, uint32_t Y, uint32_t length, uint32_t height, uint16_t color);
void SelFunc(void);
void SelFunc2(void);
void ReturnFunc(void);
void DisplayMenuItemString	(u32 x, u32 y, char *ptr);
void DisplayMainMenu(void);
void DisplayTdataMenu(void);
void set_state_level(uint8_t level);
void set_ligth_level(uint8_t level);
void set_mainctrl(uint8_t message);
void Time_Data_Setting(void);
void func_Time_Data_Setting(void);
void LCD_SetCursor(uint16_t Xpos, uint16_t Ypos);
void Start_GRAM(void);
//void Lcd_Write_Data					(uint16_t data);
void func_MAINCONTROL(void);
void func_SPEED_VNT(void);
void func_SETTING(void);
void func_DEBUG(void);
void func_SILENT_MODE(void);
void vfunc_DATA_GRW(void);
void func_TIMEDATE(void);
void func_RETURN(void);
void func_NULL(void);
void func_Data(void);
void func_Alarm(void);
void func_Mounth(void);
void func_Year(void);
void func_Day(void);
void func_Time(void);
void func_Min(void);
void func_TData_Set	(void);
void func_Ligth_Level(void);

#endif // __MENU_H__
