#include "menu.h"

volatile uint8_t state_menu =  0;

u32 y0=0;
  	 	
volatile u32 MenuItemIndex = 0, nMenuLevel = 0;
volatile u32 TimeItemIndex = 0;

static u32 ItemNumb[MAX_MENU_LEVELS];

volatile uint8_t Select_item_menu = 0;
uint8_t current_level = 0;
uint8_t current_ligthlevel = 10;

Graphics_Rectangle Clear_Rect_level    	= {715,  70, 750, 100};
Graphics_Rectangle Clear_submenu_area   = {326,  65, 776, 436};
Graphics_Rectangle Clear_name_menu  	 	= {20,  20, 290,  50};
Graphics_Rectangle Clear_name_submenu  	= {320,  20, 530,  59};
Graphics_Rectangle Clear_XY         		= {704,  80, 765,  150};

Graphics_Rectangle Rect_topbox         = {475,  75, 605,  250};
Graphics_Rectangle Rect_botbox         = {320,  60, 780,  65};
Graphics_Rectangle Rect_sysbox         = {320,  60, 780,  65};

FlagStatus State_LAMPTOP = RESET;
FlagStatus State_LAMPBOT = RESET;
FlagStatus State_LAMPSYSBOX = RESET;
FlagStatus State_VENTFLOW = RESET;
FlagStatus State_VENTVEGA = RESET;

extern TIM_OCInitTypeDef  TIM_OCInitStructure;
extern uint16_t CCR1_Val;
/*-----------------------------------------------------------------------------------------------*/
struct sMenuItem TDATA_Menu_Items[] = 		{ 	{"Time",   func_Time, 	NULL},
												{"Data",   func_Data,   NULL},
												{"Day",    func_Day,    NULL},
												{"Data",   func_Data,   NULL},	
												{"Mounth", func_Mounth, NULL}, 
												{"Year",   func_Year,   NULL},
																			    };	
struct sMenu TDATA_Menu 	= {"TIME MENU", TDATA_Menu_Items, countof(TDATA_Menu_Items)};	
/*------------------------------------------------------------------------------------------------*/
struct sMenuItem SETTING_Menu_Items[] = 	{  			{"Time",  	func_Time,        NULL},
														{"Data",    func_Data,        NULL},
														{"Alarm", 	func_Alarm,       NULL},
														{"Ligth", 	func_Ligth_Level, NULL},
														{"-RETURN", ReturnFunc,       NULL},		
																					};	
struct sMenu SETTING_Menu = {"SETTING MENU", SETTING_Menu_Items, countof(SETTING_Menu_Items)};	
/*------------------------------------------------------------------------------------------------*/
struct sMenuItem Main_Menu_Items[] = 			{   {"MAIN CTRL",   func_MAINCONTROL,  NULL},
													{"SETTING",     func_SETTING,  &SETTING_Menu},
													{"TIME ",     	func_Min,          NULL},
													{"DEBUG",       func_DEBUG,        NULL},
													{"SPEED VNT", 	func_SPEED_VNT,    NULL},
													{"SMODE",       func_SILENT_MODE,  NULL},																	
													{"DATA GRW",    func_DATA_GRW,     NULL},																	
													//{"/RETURN", 	  ReturnFunc, 		  NULL},																								
																			  };	
struct sMenu Main_Menu	 	= {"MAIN MENU", Main_Menu_Items, countof(Main_Menu_Items)};
/*------------------------------------------------------------------------------------------------*/
struct sTimeItem Time_Menu_Items[] = 			{  	  {"1", 	NULL, NULL},
													  {"2", 	func_SPEED_VNT, 	NULL},
													  {"3", 	func_SPEED_VNT, 	NULL},														
																			  };	
struct sTime Time_Menu 		= {"Time Data setting", Time_Menu_Items, countof(Time_Menu_Items)};
/*------------------------------------------------------------------------------------------------*/

void func_MAINCONTROL(void)	{

			Select_item_menu = MAINCONTROL;
			DisplayMainControl(220,340);
}
void func_SPEED_VNT(void){

			Select_item_menu = SVENT_ITEM;
			Graphics_drawString(&g_sContext, "Fvent Speed",    AUTO_STRING_LENGTH, 350, 70,  TRANSPARENT_TEXT);
			set_state_level(current_level);
}	
void func_SETTING(void){
	Select_item_menu = SETTING_ITEM;
}
void func_DEBUG(void){

	Select_item_menu = DEBUG_ITEM;
}
void func_RETURN(void){

	Select_item_menu = DEBUG_ITEM;
	ReturnFunc();
}
void func_SILENT_MODE(void)	{
	
Select_item_menu = SMODE_ITEM;
}
void func_DATA_GRW(void){

		Select_item_menu = DATAGRW_ITEM;
		
				Graphics_setFont   (&g_sContext, &g_sFontCmss36);
				Graphics_drawString(&g_sContext, "MCU  :   STM32F407VG",    AUTO_STRING_LENGTH, 350, 120,  TRANSPARENT_TEXT);
				Graphics_drawString(&g_sContext, "TFT    : RM68120",        AUTO_STRING_LENGTH, 350, 150,  TRANSPARENT_TEXT);
				Graphics_drawString(&g_sContext, "Touch :  FT5216GM7",  		AUTO_STRING_LENGTH, 350, 180,  TRANSPARENT_TEXT);
				Graphics_drawString(&g_sContext, "Autor :  Seregin Alex",   AUTO_STRING_LENGTH, 350, 210,  TRANSPARENT_TEXT);
				Graphics_drawString(&g_sContext, "Data  :  08.05.19",       AUTO_STRING_LENGTH, 350, 240,  TRANSPARENT_TEXT);
				Graphics_setFont   (&g_sContext, &g_sFontCmss36b);
}
void func_TIMEDATE(void){

		Select_item_menu = TIMEDATE_ITEM;

}
void func_Data(void){

		Select_item_menu = DATA_SUBITEM;
	
	DisplaySubMenuData(220,340);
}
void func_Alarm(void){

	Select_item_menu = ALARM_SUBITEM;
	DisplaySubMenuAlarm(220,340);
}
void func_NULL(void){

		Select_item_menu = DEBUG_ITEM;
}
void func_Mounth(void){

		Select_item_menu = MOUNTH_SUBITEM;
}
void func_Year(void){

		Select_item_menu = YEAR_SUBITEM;
}
void fun_Day(void){

		Select_item_menu = DAY_SUBITEM;
}
void func_Time(void){

		//u32 x = 0, index;
		Select_item_menu = TIME_SUBITEM;	
   
	
	
		DisplaySubMenuTime(220,340);

	
//		for (index = 0 ;     index < 12;          index++, x += 35) {
//        
//				draw_rect(x + 340, 80, 30, 40,0x7F);
//				
//		}

	
}
void func_Min(void){

		Select_item_menu = MIN_SUBITEM;
}
void func_TData_Set(void){

		Select_item_menu = TDATA_ITEM;
	
				Graphics_setFont   (&g_sContext, &g_sFontCmss36);
				Graphics_drawString(&g_sContext, "MCU  :   STM32F407VG",    AUTO_STRING_LENGTH, 350, 120,  TRANSPARENT_TEXT);
				Graphics_drawString(&g_sContext, "TFT    : RM68120",        AUTO_STRING_LENGTH, 350, 150,  TRANSPARENT_TEXT);
				Graphics_drawString(&g_sContext, "Touch :  FT5216GM7",  		AUTO_STRING_LENGTH, 350, 180,  TRANSPARENT_TEXT);
				Graphics_drawString(&g_sContext, "Autor :  Seregin Alex",   AUTO_STRING_LENGTH, 350, 210,  TRANSPARENT_TEXT);
				Graphics_drawString(&g_sContext, "Data  :  08.05.19",       AUTO_STRING_LENGTH, 350, 240,  TRANSPARENT_TEXT);
				Graphics_setFont   (&g_sContext, &g_sFontCmss36b);
}																							
void func_Time_Data_Setting(void){

	Select_item_menu = TDATA_ITEM;
	SetMenu(&TDATA_Menu, TDATA_Menu_Items);
	DisplayTdataMenu();
}
void func_Ligth_Level(void)	{

			Select_item_menu = LIGTH_SUBITEM;
	
			Graphics_drawString(&g_sContext, "Ligth level",    AUTO_STRING_LENGTH, 350, 70,  TRANSPARENT_TEXT);

			set_ligth_level(current_ligthlevel);
}	
void IdleFunc(void){
}
void ReturnFunc (void){

	 if(nMenuLevel != 0) {      

			psCurrentMenu = psPrevMenu[nMenuLevel-1];
			psMenuItem = &psCurrentMenu->psItems[0];
			ItemNumb[nMenuLevel] = 0;
			MenuItemIndex = 0;
			nMenuLevel--;

			DisplayMenu(60,20);
	 }
}
void SelFunc(void){
		
	if(MenuItemIndex <= (psCurrentMenu->nItems)-1  ){
		
		psMenuItem = &psCurrentMenu->psItems[MenuItemIndex];
	  psCurrentMenuItem = psMenuItem;
	
    if(psMenuItem->psSubMenu != NULL) {         									//��������� ���� �� �������
        
			  state_menu =  STATE_SUBMENU;
			  MenuItemIndex = 8;
			  while(MenuItemIndex--)	draw_rect(20, 60+(MenuItemIndex)*45, 270,40,0xFF);		// �������� ������ ����������� ����
			  MenuItemIndex = 0;                      									//�������� ������ ������ ����
        
			  psCurrentMenu = psMenuItem->psSubMenu;  									//����������� �������� ���� �������
        psMenuItem = &(psCurrentMenu->psItems)[0];
			 
				nMenuLevel++;                           //��������� ������ ����
        psPrevMenu[nMenuLevel] = psCurrentMenu; //����������� ������������� ���� ������� ����
			 
			 MenuItemIndex = 0;
			
			 Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
       DisplayMenu(60,20);
    }
		else{
			
				DisplayMenu(60,20);
				psCurrentMenuItem->pfMenuFunc();            //��������� ������� �������� ������ ���� (���� ���� �������, �� �-� ������)	
			
		}
	}
}
void SelFunc2(void){
	
//		DisplayMenu(100,340);
//    psCurrentMenuItem = psMenuItem;             //����������� �������� ������ ���� ����� ��������� (�������� UpFunc())
//    psCurrentMenuItem->pfMenuFunc();            //��������� ������� �������� ������ ���� (���� ���� �������, �� �-� ������)
}
void DisplayMenuItemString(u32 x, u32 y, char *ptr){
//    u32 x;
		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
//    LCD_PUTS(0, y, "                     ");
  //  x = 35;//(MAX_X - (CurrentFont->Width * strlen((const char *)ptr))) / 2;				
	Graphics_drawString(&g_sContext, (int8_t*)ptr,  AUTO_STRING_LENGTH, x, y,  TRANSPARENT_TEXT);//LCD_PUTS(x, y, ptr);
}
u32  DisplayMenuTitle(char *ptr){
//    u32 x =0;// y;

    //x = (MAX_X - (CurrentFont->Width * strlen(ptr))) / 2;
   // x=0;        //���������� ��������� ���� �� �������� ������, � ������
    //LCD_PUTS(x, 0, ptr);
	
   Graphics_drawString(&g_sContext, (int8_t*)ptr,   AUTO_STRING_LENGTH, 25,  20,  TRANSPARENT_TEXT);
//    y += (CurrentFont->Height + 1);    //�.�. �-� LCD_PUTS() ���������� ���������� Y ������� (���������) ������,
//    CurrentMethod = MET_OR;                             //�� ������ ������������ ������� ��������� �� ����� � ����������� Y
//    LCD_Line(0, y, MAX_X, y);                           //���������� ��� ������ ����� �������������� ���������

    return 1;
}
void Menu_Init(void){
    //����� ���� � ����������� �� ��������������� �����
//    int num_cable;
//    u32 x,i;
 //   num_cable=InitCable();
    //num_cable=35;										
//    switch(num_cable)
//        {
//        case 21:                                                        //���� �3-��, ������ "��1"
                psCurrentMenu = &Main_Menu;                      //����� ��������� ������������ ����������� ����� � ������
                psPrevMenu[nMenuLevel] = psCurrentMenu;
                psMenuItem = Main_Menu_Items;
	
								psCurrentTime = &Time_Menu;                      //����� ��������� ������������ ����������� ����� � ������
//            //    psPrevButt[nMenuLevel] = psCurrentButt;
                psTimeItem = Time_Menu_Items;
////                break;
//        case 34:                                                        //���� �4-��, ������ "��"
//                psCurrentMenu = &In_TPK_Menu;                           //����� ��������� ������������ ����������� ����� � ������
//                psPrevMenu[nMenuLevel] = psCurrentMenu;
//                psMenuItem = In_TPK_Menu_Items;
//                break;
//        case 35:                                                        //�������� ���-����, ������ ���268-50 (��������)
//                psCurrentMenu = &IPE_KSTP_Menu;
//                psPrevMenu[nMenuLevel] = psCurrentMenu;
//                psMenuItem = IPE_KSTP_Menu_Items;
//                break;
//        case 0:                                                         //�� ����������� ����
//                CurrentFont = &Font_6x8;
//                CurrentMethod = MET_AND;
//				
//                psCurrentMenu = &CALL_EXCHANGE_MODE_Menu;
//                psPrevMenu[nMenuLevel] = psCurrentMenu;
//                psMenuItem = CALL_EXCHANGE_MODE_Menu_Items;

//                for(i=0; i<2; i++)
//									{
//									LCD_PUTS(6, 8*2 , "���� �� �����������!");
//									delay_ms(300);
//									LCD_PUTS(6, 8*2 , "                    ");
//									delay_ms(300);	
//									}	
//                LCD_PUTS(6, 8*2 , "���� �� �����������!");
//								delay_ms(300);
//				
//								DisplayMenu_CALL_EXCHANGE_MODE();				//������ �������. ����� �������� ���� ���� ���� ����� ������� "���� �� �����������!"
//								ReadKey();
//                //break;
//        default:                                                        //�� ��������� ����������� ����
//                CurrentFont = &Font_6x8;
//                LCD_CLS();
//                CurrentMethod = MET_AND;
//                while(1)
//                   {
//                   x = (MAX_X - (CurrentFont->Width * strlen("�� �����"))) / 2;
//                   LCD_PUTS(x, 8*2, "�� �����");
//                   LCD_PUTS(7, 8*3 , "����������� ����!!!");
//                   delay_ms(500);
//                   LCD_PUTS(x, 8*2, "        ");
//                   delay_ms(500);
//                   }
//                //break;
//        }

    
//    CurrentFont = &Font_6x8;
//    CurrentMethod = MET_AND;
}
void SetMenu(tMenu pMenu, tMenuItem pMenuItem){
   
                psCurrentMenu = pMenu;                      //����� ��������� ������������ ����������� ����� � ������
                psPrevMenu[nMenuLevel] = psCurrentMenu;
                psMenuItem = pMenuItem;
}
void DisplayMenu(uint32_t Top_TAB, uint32_t Left_TAB){
    
		u32 y, index = 0;

    tMenuItem psMenuItem2;

    /* ���������� ��������� ���� */     //� ��������� ���������� Y ����� �������������� ���������
    y0=Top_TAB;
	
	
	  Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLUE);	
    Graphics_fillRectangle		 (&g_sContext, &Clear_name_menu);
	  Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
	
    DisplayMenuTitle(psCurrentMenu->psTitle);
    /* ���������� ������ ���� */
    for (index = 0, y = y0;     index < psCurrentMenu->nItems;          index++, y += 45) {
        
				draw_rect(Left_TAB, y, 270,40,0x7F);
				psMenuItem2 = &(psCurrentMenu->psItems[index]);
        DisplayMenuItemString(35, y, psMenuItem2->psTitle);
    }

    /* ���������� ������� ����� */
    psMenuItem = &(psCurrentMenu->psItems[MenuItemIndex]);
		draw_rect(Left_TAB, Top_TAB+(MenuItemIndex*45), 270,40,0xFFFF);
		
		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLUE);
		Graphics_drawString(&g_sContext, (int8_t*)(psMenuItem->psTitle),  AUTO_STRING_LENGTH, 42, Top_TAB+2+(MenuItemIndex*45),  TRANSPARENT_TEXT);
		
		Graphics_fillRectangle(&g_sContext, &Clear_name_submenu);	
		
		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
    Graphics_drawString(&g_sContext, (int8_t*)psMenuItem->psTitle,   AUTO_STRING_LENGTH, 320,  Left_TAB+2,  TRANSPARENT_TEXT);
		
		Graphics_setForegroundColor(&g_sContext, 0x0);		
		Graphics_fillRectangle(&g_sContext, &Clear_submenu_area);		
		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
	
}
void DisplayMainControl(uint32_t Top_TAB, uint32_t Left_TAB){
 
	uint32_t i =0;
//char str_lcd[20] = {0};
//char *pstr_lcd = str_lcd;

//sprintf(pstr_lcd, "%#d:%#d:%#d" , datatime.hours, datatime.minutes, datatime.seconds);
//		
// Graphics_drawString(&g_sContext, pstr_lcd,   AUTO_STRING_LENGTH, 340,  80,  TRANSPARENT_TEXT);
		

	Graphics_setFont(&g_sContext, &g_sFontCmss30b);

//    for (i = 0; i < 3; i++) 			draw_rect((140*i)+Left_TAB, 150, 120,40,0x7F);
//	Graphics_drawString(&g_sContext, "-Hour-      UP      DOWN",   AUTO_STRING_LENGTH, 350,  152,  TRANSPARENT_TEXT);

//    for (i = 0; i < 3; i++) 			draw_rect((140*i)+Left_TAB, 200, 120,40,0x7F);
//	Graphics_drawString(&g_sContext, "-Min-       UP      DOWN",   AUTO_STRING_LENGTH, 350,  202,  TRANSPARENT_TEXT);


//    for (i = 0; i < 3; i++) 			draw_rect((140*i)+Left_TAB, 250, 120,40,0x7F);
//	Graphics_drawString(&g_sContext, "-Sec-       UP      DOWN",   AUTO_STRING_LENGTH, 350,  252,  TRANSPARENT_TEXT);



for (i = 0; i < 1; i++) 			draw_rect((140*i)+Left_TAB-4, 76,  120,165,0xFFFF);
for (i = 0; i < 1; i++) 			draw_rect((140*i)+Left_TAB-4, 251, 120,165,0xFFFF);
															draw_rect((130)+Left_TAB-4,   251, 120,165,0xFFFF);

for (i = 0; i < 1; i++) 			draw_rect((140*i)+Left_TAB, 80,  120,165,0xFF00);
for (i = 0; i < 1; i++) 			draw_rect((140*i)+Left_TAB, 255, 120,165,0xFF00);
															draw_rect((130)+Left_TAB,   255, 120,165,0x00FF);
															
	
	Graphics_setFont(&g_sContext, &g_sFontCmss30b);		

	Graphics_drawString(&g_sContext, "20C",      AUTO_STRING_LENGTH, 370,  160,  TRANSPARENT_TEXT);														
	Graphics_drawString(&g_sContext, "20C",      AUTO_STRING_LENGTH, 370,  320,  TRANSPARENT_TEXT);	
	Graphics_drawString(&g_sContext, "20C",      AUTO_STRING_LENGTH, 500,  320,  TRANSPARENT_TEXT);	

	Graphics_drawString(&g_sContext, "50%",      AUTO_STRING_LENGTH, 370,  185,  TRANSPARENT_TEXT);														
	Graphics_drawString(&g_sContext, "50%",      AUTO_STRING_LENGTH, 370,  345,  TRANSPARENT_TEXT);	
	Graphics_drawString(&g_sContext, "50%",      AUTO_STRING_LENGTH, 500,  345,  TRANSPARENT_TEXT);	

	Graphics_setFont(&g_sContext, &g_sFontCmss24b);

															
	if(State_LAMPTOP == SET)					{draw_rect((260)+Left_TAB, 80 + (0 * 70), 160,60,0x1fe);		draw_rect((10)+340, 90 + (0 * 70), 100, 20,0xFFE0);}									
	else															{draw_rect((260)+Left_TAB, 80 + (0 * 70), 160,60,0xb5b6);		draw_rect((10)+340, 90 + (0 * 70), 100, 20,0xFF00);}	
	if(State_LAMPBOT == SET)					{draw_rect((260)+Left_TAB, 80 + (1 * 70), 160,60,0x1fe);		draw_rect((10)+340, 90 + (175),    100, 20,0xFFE0);}									
	else															{draw_rect((260)+Left_TAB, 80 + (1 * 70), 160,60,0xb5b6);		draw_rect((10)+340, 90 + (175),    100, 20,0xFF00);}
	if(State_LAMPSYSBOX == SET)				{draw_rect((260)+Left_TAB, 80 + (2 * 70), 160,60,0x1fe);		draw_rect((140)+340, 90 + (175),   100, 20,0xFFE0);}									
	else															{draw_rect((260)+Left_TAB, 80 + (2 * 70), 160,60,0xb5b6);		draw_rect((140)+340, 90 + (175),   100, 20,0x00FF);}	
	if(State_VENTFLOW == SET)					{draw_rect((260)+Left_TAB, 80 + (3 * 70), 160,60,0x1fe);}									
	else															{draw_rect((260)+Left_TAB, 80 + (3 * 70), 160,60,0xb5b6);}	
	if(State_VENTVEGA == SET)					{draw_rect((260)+Left_TAB, 80 + (4 * 70), 160,60,0x1fe);}									
	else															{draw_rect((260)+Left_TAB, 80 + (4 * 70), 160,60,0xb5b6);}	
																		



Graphics_drawString(&g_sContext, "Lamp TOP",      AUTO_STRING_LENGTH, 610,  95,  TRANSPARENT_TEXT);
Graphics_drawString(&g_sContext, "Lamp BOT",      AUTO_STRING_LENGTH, 610,  165,  TRANSPARENT_TEXT);
Graphics_drawString(&g_sContext, "Lamp SysBOX",   AUTO_STRING_LENGTH, 605,  235,  TRANSPARENT_TEXT);

Graphics_drawString(&g_sContext, " Fan FLOW",     AUTO_STRING_LENGTH, 605,  305,  TRANSPARENT_TEXT);
Graphics_drawString(&g_sContext, " Fan VEGA",     AUTO_STRING_LENGTH, 605,  375,  TRANSPARENT_TEXT);

Graphics_setFont(&g_sContext, &g_sFontCmss36b);

}
void DisplaySubMenuTime(uint32_t Top_TAB, uint32_t Left_TAB){
 
	uint32_t i =0;
//char str_lcd[20] = {0};
//char *pstr_lcd = str_lcd;

//sprintf(pstr_lcd, "%#d:%#d:%#d" , datatime.hours, datatime.minutes, datatime.seconds);
//		
// Graphics_drawString(&g_sContext, pstr_lcd,   AUTO_STRING_LENGTH, 340,  80,  TRANSPARENT_TEXT);
		

	Graphics_setFont(&g_sContext, &g_sFontCmss30b);

    for (i = 0; i < 3; i++) 			draw_rect((140*i)+Left_TAB, 150, 120,40,0x7F);
	Graphics_drawString(&g_sContext, "-Hour-      UP      DOWN",   AUTO_STRING_LENGTH, 350,  152,  TRANSPARENT_TEXT);

    for (i = 0; i < 3; i++) 			draw_rect((140*i)+Left_TAB, 200, 120,40,0x7F);
	Graphics_drawString(&g_sContext, "-Min-       UP      DOWN",   AUTO_STRING_LENGTH, 350,  202,  TRANSPARENT_TEXT);


    for (i = 0; i < 3; i++) 			draw_rect((140*i)+Left_TAB, 250, 120,40,0x7F);
	Graphics_drawString(&g_sContext, "-Sec-       UP      DOWN",   AUTO_STRING_LENGTH, 350,  252,  TRANSPARENT_TEXT);

Graphics_setFont(&g_sContext, &g_sFontCmss36b);

}
void DisplaySubMenuAlarm(uint32_t Top_TAB, uint32_t Left_TAB){
 
	uint32_t i =0;
//char str_lcd[20] = {0};
//char *pstr_lcd = str_lcd;

//sprintf(pstr_lcd, "%#d:%#d:%#d" , datatime.hours, datatime.minutes, datatime.seconds);
//		
// Graphics_drawString(&g_sContext, pstr_lcd,   AUTO_STRING_LENGTH, 340,  80,  TRANSPARENT_TEXT);
		

	Graphics_setFont(&g_sContext, &g_sFontCmss30b);

    for (i = 0; i < 3; i++) 			draw_rect((140*i)+Left_TAB, 150, 120,40,0x7F);
	Graphics_drawString(&g_sContext, "-Hour-      UP      DOWN",   AUTO_STRING_LENGTH, 350,  152,  TRANSPARENT_TEXT);

    for (i = 0; i < 3; i++) 			draw_rect((140*i)+Left_TAB, 200, 120,40,0x7F);
	Graphics_drawString(&g_sContext, "-Min-       UP      DOWN",   AUTO_STRING_LENGTH, 350,  202,  TRANSPARENT_TEXT);


    for (i = 0; i < 3; i++) 			draw_rect((140*i)+Left_TAB, 250, 120,40,0x7F);
	Graphics_drawString(&g_sContext, "-Sec-       UP      DOWN",   AUTO_STRING_LENGTH, 350,  252,  TRANSPARENT_TEXT);



    for (i = 0; i < 2; i++) 			draw_rect((210*i)+Left_TAB, 320, 190,40,0x7F);
	Graphics_drawString(&g_sContext, "AlarmA ON    AlarmA OFF",   AUTO_STRING_LENGTH, 350,  322,  TRANSPARENT_TEXT);
	
	 for (i = 0; i < 2; i++) 				draw_rect((210*i)+Left_TAB, 370, 190,40,0x7F);
	Graphics_drawString(&g_sContext, "AlarmB ON    AlarmB OFF",   AUTO_STRING_LENGTH, 350,  372,  TRANSPARENT_TEXT);

Graphics_setFont(&g_sContext, &g_sFontCmss36b);

}
void DisplaySubMenuData(uint32_t Top_TAB, uint32_t Left_TAB){
 
	uint32_t i =0;
//char str_lcd[20] = {0};
//char *pstr_lcd = str_lcd;

Graphics_setFont(&g_sContext, &g_sFontCmss30b);

    /* ���������� ������ ���� */
    for (i = 0; i < 3; i++) 				draw_rect((140*i)+Left_TAB, 150, 120,40,0x7F);	
	Graphics_drawString(&g_sContext, "-Data-      UP      DOWN",   AUTO_STRING_LENGTH, 350,  152,  TRANSPARENT_TEXT);
		    /* ���������� ������ ���� */
    for (i = 0; i < 3; i++) 				draw_rect((140*i)+Left_TAB, 200, 120,40,0x7F);
	Graphics_drawString(&g_sContext, "-Mon-       UP      DOWN",   AUTO_STRING_LENGTH, 350,  202,  TRANSPARENT_TEXT);
    /* ���������� ������ ���� */
    for (i = 0; i < 3; i++) 				draw_rect((140*i)+Left_TAB, 250, 120,40,0x7F);
	Graphics_drawString(&g_sContext, "-Year-      UP      DOWN",   AUTO_STRING_LENGTH, 350,  252,  TRANSPARENT_TEXT);
    /* ���������� ������ ���� */
    for (i = 0; i < 3; i++) 		    draw_rect((140*i)+Left_TAB, 300, 120,40,0x7F);
	Graphics_drawString(&g_sContext, "-Day-       UP      DOWN",   AUTO_STRING_LENGTH, 350,  302,  TRANSPARENT_TEXT);	

Graphics_setFont(&g_sContext, &g_sFontCmss36b);

}
void DisplayButtomMenu(void){
 
//u32 y, index;

//    tButtItem psButtItem2;


//    y0=390;
////    DisplayMenuTitle(psCurrentMenu->psTitle);
////    /* ���������� ������ ���� */
//    for (index = 0, y = y0;     index < psCurrentButt->nItems;          index++, y += 45) {
//        
//				draw_rect((95*index)+20, y0, 80, 50,0x7F);
//				psButtItem2 = &(psCurrentButt->psItems[index]);
//        DisplayMenuItemString((95*index)+20, 395, psButtItem2->psTitle);
//    }

////    /* ���������� ������� ����� */
////    psMenuItem = &(psCurrentMenu->psItems[MenuItemIndex]);
////		draw_rect(Left_TAB, Top_TAB+(MenuItemIndex*45), 270,40,0xFFFF);
////		
////		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLUE);
////		Graphics_drawString(&g_sContext, (psMenuItem->psTitle),  AUTO_STRING_LENGTH, 42, Top_TAB+2+(MenuItemIndex*45),  TRANSPARENT_TEXT);
////		
//		Graphics_fillRectangle(&g_sContext, &Clear_name_submenu);	
//		
//		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
//    Graphics_drawString(&g_sContext, psMenuItem->psTitle,   AUTO_STRING_LENGTH, 320,  Left_TAB+2,  TRANSPARENT_TEXT);
//		
//		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLUE);		
//		Graphics_fillRectangle(&g_sContext, &Clear_submenu_area);		
//		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
	
}
void DisplayMainMenu(void){
    
//	  u32 y, index;

//    tMenuItem psMenuItem2;

//    /* ���������� ��������� ���� */     //� ��������� ���������� Y ����� �������������� ���������
//    y0=60;
//    DisplayMenuTitle(psCurrentMenu->psTitle);
//    /* ���������� ������ ���� */
//    for (index = 0, y = y0;     index < psCurrentMenu->nItems;          index++, y += 45) {
//        
//				draw_rect(20, y, 270,40,0x7F);
//				psMenuItem2 = &(psCurrentMenu->psItems[index]);
//        DisplayMenuItemString(35, y, psMenuItem2->psTitle);
//    }

//    /* ���������� ������� ����� */
//    psMenuItem = &(psCurrentMenu->psItems[MenuItemIndex]);
//		draw_rect(20, 60+(MenuItemIndex*45), 270,40,0xFFFF);
//		
//		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLUE);
//		Graphics_drawString(&g_sContext, (psMenuItem->psTitle),  AUTO_STRING_LENGTH, 42, 62+(MenuItemIndex*45),  TRANSPARENT_TEXT);
//		
//		Graphics_fillRectangle(&g_sContext, &Clear_name_submenu);	
//		
//		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
//    Graphics_drawString(&g_sContext, psMenuItem->psTitle,   AUTO_STRING_LENGTH, 320,  22,  TRANSPARENT_TEXT);
//		
//		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLUE);		
//		Graphics_fillRectangle(&g_sContext, &Clear_submenu_area);		
//		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
}
void DisplayTdataMenu(void){
    
	  u32 y, index;
    tMenuItem psMenuItem2;

		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLUE);	
		Graphics_fillRectangle(&g_sContext, &Clear_name_submenu);	
		
		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
	  Graphics_drawString(&g_sContext, "TimeData Set",   AUTO_STRING_LENGTH, 320,  22,  TRANSPARENT_TEXT);
	
		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLUE);		
		Graphics_fillRectangle(&g_sContext, &Clear_submenu_area);		
		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);					
	
	
    /* ���������� ��������� ���� */     //� ��������� ���������� Y ����� �������������� ���������
    y0=100;
  //  DisplayMenuTitle(psCurrentMenu->psTitle);
    /* ���������� ������ ���� */
    for (index = 0, y = y0;     index < psCurrentMenu->nItems;          index++, y += 45) {
       
				draw_rect(340, y, 170,40, 0x7F);
				psMenuItem2 = &(psCurrentMenu->psItems[index]);
        DisplayMenuItemString(350, y, psMenuItem2->psTitle);
    }
		
		
		
		
		
		
    /* ���������� ������� ����� */
    psMenuItem = &(psCurrentMenu->psItems[MenuItemIndex]);
		draw_rect(340, 100+(MenuItemIndex*45), 170,40,0xFFFF);
				
	Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLUE);
	Graphics_drawString(&g_sContext, (int8_t*)(psMenuItem->psTitle),  AUTO_STRING_LENGTH, 342, 102+(MenuItemIndex*45),  TRANSPARENT_TEXT);
//		
////		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLUE);		
////		Graphics_fillRectangle(&g_sContext, &Clear_submenu_area);		
//		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
}
void set_state_level(uint8_t level){

	uint64_t i, j = 0;
	
	if(level >= 0){
	if(level > current_level){
		
	current_level = level;
	
		while(level--){
		
			for( j = 0; j < 60; j++)	{		LCD_SetCursor(360 + (level)*40, j + 120);		Start_GRAM();			for( i = 0; i < 30; i++)			Lcd_Write_Data(GREEN_RGB565);	}
						
		}
	}
	else{
		
		current_level = level;
		
		while(level<10){
		
			for( j = 0; j < 60; j++)	{		LCD_SetCursor(360 + (level)*40, j + 120);		Start_GRAM();			for( i = 0; i < 30; i++)			Lcd_Write_Data(0xFFFF);	}
			level++;			
		}
	}
	}else current_level = 0;
	
	Graphics_setForegroundColor(&g_sContext, 0x0);		Graphics_fillRectangle(&g_sContext, &Clear_Rect_level);		//Graphics_drawStringCentered(&g_sContext, "______________",  AUTO_STRING_LENGTH, 420, 130,  TRANSPARENT_TEXT);		DELAY(200000);
	Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);		
	
	sprintf(pstr, "Level-%#d", current_level);	
	Graphics_drawString(&g_sContext, (int8_t*)pstr,  AUTO_STRING_LENGTH, 620, 70,  TRANSPARENT_TEXT);	
}
void set_mainctrl(uint8_t message){
	
	
	
						
		//draw_rect((260)+340, 80 + (0 * 70), 160,60,0xb5b6);
	


Graphics_setFont(&g_sContext, &g_sFontCmss24b);
Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);


	
	switch(message){
	/*******************************************************/
		case MESS_LAMP_TOP: 
			if(State_LAMPTOP == SET){
				State_LAMPTOP = RESET;
				DELAY(1000000);
				GPIO_ResetBits(GPIOB, GPIO_Pin_14);
				draw_rect((260)+340, 80 + (0 * 70), 160,60,0xb5b6);
				draw_rect((10)+340, 90 + (0 * 70), 100, 20,0xFF00);
			}
			else{
				State_LAMPTOP = SET;
				DELAY(1000000);
				GPIO_SetBits  (GPIOB, GPIO_Pin_14);
				draw_rect((260)+340, 80 + (0 * 70), 160,60,0x1fe);
				draw_rect((10)+340, 90 + (0 * 70), 100, 20,0xFFE0);
			}			
		Graphics_drawString(&g_sContext, "Lamp TOP",      AUTO_STRING_LENGTH, 610,  95,  TRANSPARENT_TEXT);	
		break;
		/*******************************************************/
		case MESS_LAMP_BOT:
			if(State_LAMPBOT == SET){
				State_LAMPBOT = RESET;
				DELAY(1000000);
				GPIO_ResetBits(GPIOB, GPIO_Pin_13);
				draw_rect((260)+340, 80 + (1 * 70), 160,60,0xb5b6);
				draw_rect((10)+340, 90 + (175), 100, 20,0xFF00);
			}
			else{
				State_LAMPBOT = SET;
				DELAY(1000000);
				GPIO_SetBits  (GPIOB, GPIO_Pin_13);
				draw_rect((260)+340, 80 + (1 * 70), 160,60,0x1fe);
				draw_rect((10)+340, 90 + (175), 100, 20,0xFFE0);
			}			
		Graphics_drawString(&g_sContext, "Lamp BOT",      AUTO_STRING_LENGTH, 610,  165,  TRANSPARENT_TEXT);
		break;
/*******************************************************/
		case MESS_LAMP_SYSBOX: 
			if(State_LAMPSYSBOX == SET){
				State_LAMPSYSBOX = RESET;
				DELAY(1000000);
				GPIO_ResetBits(GPIOB, GPIO_Pin_12);
				draw_rect((260)+340, 80 + (2 * 70), 160,60,0xb5b6);
				draw_rect((140)+340, 90 + (175), 100, 20,0x00FF);
			}
			else{
				State_LAMPSYSBOX = SET;
				DELAY(1000000);
				GPIO_SetBits  (GPIOB, GPIO_Pin_12);
				draw_rect((260)+340, 80 + (2 * 70), 160,60,0x1fe);
				draw_rect((140)+340, 90 + (175), 100, 20,0xFFE0);
			}	
		Graphics_drawString(&g_sContext, "Lamp SysBOX",     AUTO_STRING_LENGTH, 605,  235,  TRANSPARENT_TEXT);	
		break;
/*******************************************************/
		case MESS_VENTFLOW:
			if(State_VENTFLOW == SET){
				State_VENTFLOW = RESET;
				DELAY(1000000);
//				GPIO_ResetBits(GPIOB, GPIO_Pin_11);
					draw_rect((260)+340, 80 + (3 * 70), 160,60,0xb5b6);
			}
			else{
				State_VENTFLOW = SET;
				DELAY(1000000);
//				GPIO_SetBits  (GPIOB, GPIO_Pin_11);
					draw_rect((260)+340, 80 + (3 * 70), 160,60,0x1fe);
			}			
		Graphics_drawString(&g_sContext, " Fan FLOW",     AUTO_STRING_LENGTH, 605,  305,  TRANSPARENT_TEXT);
		break;
/*******************************************************/
		case MESS_VENTVEGA:
			if(State_VENTVEGA == SET){
				State_VENTVEGA = RESET;
				DELAY(1000000);
				GPIO_ResetBits(GPIOB, GPIO_Pin_11);
				draw_rect((260)+340, 80 + (4 * 70), 160,60,0xb5b6);
			}
			else{
				State_VENTVEGA = SET;
				DELAY(1000000);
				GPIO_SetBits  (GPIOB, GPIO_Pin_11);
				draw_rect((260)+340, 80 + (4 * 70), 160,60,0x1fe);
			}		
		Graphics_drawString(&g_sContext, " Fan VEGA",     AUTO_STRING_LENGTH, 605,  375,  TRANSPARENT_TEXT);	
		break;
	}
/*******************************************************/
Graphics_setFont(&g_sContext, &g_sFontCmss36b);
}
void set_ligth_level(uint8_t ligthlevel){

	uint64_t i, j = 0;
	
	if(ligthlevel >= 0){
	if(ligthlevel > current_ligthlevel){
		
	current_ligthlevel = ligthlevel;
	
		while(ligthlevel--){
		
			for( j = 0; j < 60; j++)	{		LCD_SetCursor(360 + (ligthlevel)*40, j + 120);		Start_GRAM();			for( i = 0; i < 30; i++)			Lcd_Write_Data(GREEN_RGB565);	}
						
		}
	}
	else{
		
		current_ligthlevel = ligthlevel;
		
		while(ligthlevel<10){
		
			for( j = 0; j < 60; j++)	{		LCD_SetCursor(360 + (ligthlevel)*40, j + 120);		Start_GRAM();			for( i = 0; i < 30; i++)			Lcd_Write_Data(0xFFFF);	}
			ligthlevel++;			
		}
	}
	}else current_ligthlevel = 0;
	
	Graphics_setForegroundColor(&g_sContext, 0x0);		Graphics_fillRectangle(&g_sContext, &Clear_Rect_level);		//Graphics_drawStringCentered(&g_sContext, "______________",  AUTO_STRING_LENGTH, 420, 130,  TRANSPARENT_TEXT);		DELAY(200000);
	Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);		
	
	sprintf(pstr, "Level-%#d", current_ligthlevel);
	CCR1_Val = current_ligthlevel * 50;
	
	TIM_OCInitStructure.TIM_Pulse = CCR1_Val;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

  TIM_OC1Init(TIM3, &TIM_OCInitStructure);

  TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);
  TIM_ARRPreloadConfig(TIM3, ENABLE);
	
	Graphics_drawString(&g_sContext, (int8_t*)pstr,  AUTO_STRING_LENGTH, 620, 70,  TRANSPARENT_TEXT);	
}
