#include "main.h"

__IO uint16_t uhADCxConvertedValue = 0;
__IO uint32_t uwADCxConvertedVoltage = 0;

DMA_InitTypeDef  DMA_InitStructure;

//static uint32_t cnt_push = 0;
FlagStatus TIMESET_BUSY = RESET;

TM_RTC_AlarmTime_t  AlarmTime;

void Lcd_Write_Data_DMA(uint16_t data);
void TIM_Config(void);

int main(void)
{ 

//	uint64_t i, j; 
//	uint16_t tmpbuf[7] = {0};
//	uint16_t *ptmpbuf = tmpbuf;	
	
	I2C1_init();
	PORT_init();	
	init_LED();					
		
//	GPIO_InitTypeDef 	GPIO_InitStructLampCtrl;

//	RCC_AHB1PeriphClockCmd (RCC_AHB1Periph_GPIOB, ENABLE);

//	GPIO_StructInit(&GPIO_InitStructLampCtrl);
//	GPIO_InitStructLampCtrl.GPIO_Pin 			= GPIO_Pin_14;														
//	GPIO_InitStructLampCtrl.GPIO_Speed 		= GPIO_Speed_50MHz;										
//	GPIO_InitStructLampCtrl.GPIO_Mode 		= GPIO_Mode_OUT;
//	GPIO_InitStructLampCtrl.GPIO_OType 		=	GPIO_OType_PP;
//	GPIO_InitStructLampCtrl.GPIO_PuPd 		= GPIO_PuPd_UP;
//	
//	GPIO_Init(GPIOB, &GPIO_InitStructLampCtrl);
		
/********************************************************************************************************/
//	  /* TIM Configuration */
  
//  
//  /* -----------------------------------------------------------------------
//    TIM3 Configuration: generate 4 PWM signals with 4 different duty cycles.
//    
//    In this example TIM3 input clock (TIM3CLK) is set to 2 * APB1 clock (PCLK1), 
//    since APB1 prescaler is different from 1.   
//      TIM3CLK = 2 * PCLK1  
//      PCLK1 = HCLK / 4 
//      => TIM3CLK = HCLK / 2 = SystemCoreClock /2
//          
//    To get TIM3 counter clock at 21 MHz, the prescaler is computed as follows:
//       Prescaler = (TIM3CLK / TIM3 counter clock) - 1
//       Prescaler = ((SystemCoreClock /2) /21 MHz) - 1
//                                              
//    To get TIM3 output clock at 30 KHz, the period (ARR)) is computed as follows:
//       ARR = (TIM3 counter clock / TIM3 output clock) - 1
//           = 665
//                  
//    TIM3 Channel1 duty cycle = (TIM3_CCR1/ TIM3_ARR)* 100 = 50%
//    TIM3 Channel2 duty cycle = (TIM3_CCR2/ TIM3_ARR)* 100 = 37.5%
//    TIM3 Channel3 duty cycle = (TIM3_CCR3/ TIM3_ARR)* 100 = 25%
//    TIM3 Channel4 duty cycle = (TIM3_CCR4/ TIM3_ARR)* 100 = 12.5%

//    Note: 
//     SystemCoreClock variable holds HCLK frequency and is defined in system_stm32f4xx.c file.
//     Each time the core clock (HCLK) changes, user had to call SystemCoreClockUpdate()
//     function to update SystemCoreClock variable value. Otherwise, any configuration
//     based on this variable will be incorrect.    
//  ----------------------------------------------------------------------- */   

	printf("init_PE1       OK\n");	
	init_FSMC();			DELAY(100000); 				printf("init_FSMC      OK\n");	
	init_RM68120_2();												printf("init_RM68120_2 OK\n");
	init_RCC();			
	init_DMA();
  
/********************************************************************************************************/

	Kitronix320x240x16_SSD2119Init();
    
	Graphics_initContext(&g_sContext, &g_sKitronix320x240x16_SSD2119);
  Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_AQUA);
  Graphics_setFont(&g_sContext, &g_sFontCmss40b);

	DMA_M2M_Send((uint32_t)&Blue_BackGround, (uint32_t)LCD_DATA , 32768);				//Clear LCD in DMA Mode
		
//	for( j = 0; j < 340; j++)	{LCD_SetCursor(50, j + 50);			Start_GRAM();		for( i = 0; i < 120; i++)		Lcd_Write_Data_DMA(0xFF00);}
//	for( j = 0; j < 340; j++)	{LCD_SetCursor(250, j + 50);		Start_GRAM();		for( i = 0; i < 120; i++)		Lcd_Write_Data		(0xFF00);}
//	Graphics_drawImage(&g_sContext, 		&zxcv8BPP_UNCOMP, 0, 0);
//	/* TIM3 enable counter */
	
	drawMainMenu();
		
	EXTI_PF4_init();		
	NVIC_EnableIRQ(EXTI0_IRQn);
			
	Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLACK);
	
	RTC_init();
	
	RTC_CalibOutputConfig(RTC_CalibOutput_512Hz);
	RTC_OutputConfig(RTC_Output_WakeUp, RTC_OutputPolarity_High);
	//RTC_CalibOutputCmd(ENABLE);

	RTC_TamperPinSelection(RTC_TamperPin_PC13);
	//RTC_TimeStampPinSelection(RTC_TimeStampPin_PC13);
	RTC_OutputTypeConfig(RTC_OutputType_PushPull);
	
	init_ADC();
	
	
	TIM_Config();
//	NVIC_SetPriority();

//	TIM_Config();

//	AlarmTime.alarmtype = TM_RTC_AlarmType_DayInWeek;
//	AlarmTime.day     	= 1;
//	AlarmTime.hours   	= 0;
//	AlarmTime.minutes		= 0;
//	AlarmTime.seconds 	= 10;
//	TM_RTC_SetAlarm(TM_RTC_Alarm_A, &AlarmTime, TM_RTC_Format_BIN) ;

	while(1){
	
	
	}		
}
void init_ADC (void){

	ADC_InitTypeDef       ADC_InitStructure;
  ADC_CommonInitTypeDef ADC_CommonInitStructure;
  DMA_InitTypeDef       DMA_InitStructure;
  GPIO_InitTypeDef      GPIO_InitStructure;

  /* Enable ADCx, DMA and GPIO clocks ****************************************/ 
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);  
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC3, ENABLE);
  

  /* DMA2 Stream0 channel2 configuration **************************************/
  DMA_InitStructure.DMA_Channel = DMA_Channel_2;  
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)ADCx_DR_ADDRESS;
  DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&uwADCxConvertedVoltage;
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
  DMA_InitStructure.DMA_BufferSize = 1;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Disable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;         
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  DMA_Init(DMA2_Stream0, &DMA_InitStructure);
  DMA_Cmd(DMA2_Stream0, ENABLE);

  /* Configure ADC3 Channel7 pin as analog input ******************************/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  /* ADC Common Init **********************************************************/
  ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
  ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
  ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
  ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
  ADC_CommonInit(&ADC_CommonInitStructure);

  /* ADC3 Init ****************************************************************/
  ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
  ADC_InitStructure.ADC_ScanConvMode = DISABLE;
  ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
  ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
  ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_NbrOfConversion = 1;
  ADC_Init(ADC3, &ADC_InitStructure);

  /* ADC3 regular channel7 configuration **************************************/
  ADC_RegularChannelConfig(ADC3, ADC_Channel_10, 1, ADC_SampleTime_3Cycles);

 /* Enable DMA request after last transfer (Single-ADC mode) */
  ADC_DMARequestAfterLastTransferCmd(ADC3, ENABLE);

  /* Enable ADC3 DMA */
  ADC_DMACmd(ADC3, ENABLE);

  /* Enable ADC3 */
  ADC_Cmd(ADC3, ENABLE);
	
	ADC_SoftwareStartConv(ADC3);
}
void DMA_M2M_Send (uint32_t DMA_PeriphAddr, uint32_t DMA_MemAddr, uint32_t DMA_BufSize)	{
 
			//��������� ���������� ����� ��� ��� ���������� � ���-�� ������������� � � ���-�� �������� � ������ �������				
	/*
		DMA_BufSize 		- ������ ���������� ������������ 32� ����
		DMA_PeriphAddr	- ������ 
		DMA_MemAddr			- ����
	*/
	DMA_InitStructure.DMA_BufferSize 					=  DMA_BufSize;          // ����� ��������
		
	for(uint64_t i = 0; i < 13; i++)
  {
		//DMA_MemAddr++;		
		DMA_InitStructure.DMA_PeripheralBaseAddr 	= DMA_PeriphAddr;	//(uint32_t)&SRC_Const_Buffer[0];	
		DMA_InitStructure.DMA_Memory0BaseAddr 		= DMA_MemAddr;		//(uint32_t)LCD_DATA;//LCD_DATA;
		DMA_Init(DMA_STREAM, &DMA_InitStructure);

		//DMA_ITConfig(DMA_STREAM, DMA_IT_TC, ENABLE);      	//�������� ����������
		DMA_Cmd(DMA_STREAM, ENABLE);                  				//�������!

	 //  xSemaphoreTake(DMA_MemToMem, portMAX_DELAY);      	//���� ���� ���������
		while (DMA_GetCmdStatus(DMA_STREAM) != DISABLE);
		DMA_DeInit(DMA_STREAM);
  }	
}
void Lcd_Write_Data_DMA(uint16_t data){

		DMA_InitStructure.DMA_BufferSize 					=  1;          						// 	����� ��������
		DMA_InitStructure.DMA_PeripheralBaseAddr 	= (uint32_t)&data;				//	(uint32_t)&SRC_Const_Buffer[0];	
		DMA_InitStructure.DMA_Memory0BaseAddr 		= (uint32_t)LCD_DATA;			//	LCD_DATA;
		DMA_Init(DMA_STREAM, &DMA_InitStructure);

		//DMA_ITConfig(DMA_STREAM, DMA_IT_TC, ENABLE);      	//�������� ����������
		DMA_Cmd(DMA_STREAM, ENABLE);                  				//�������!

		//  xSemaphoreTake(DMA_MemToMem, portMAX_DELAY); 			//���� ���� ���������
		while (DMA_GetCmdStatus(DMA_STREAM) != DISABLE);
		DMA_DeInit(DMA_STREAM);
}
void DMA_check_MemtoMem(void){
	
//	TransferStatus = Buffercmp(SRC_Const_Buffer, DST_Buffer, BUFFER_SIZE); 		/* �������� ������ �� ���������� */
  /* TransferStatus = PASSED, ���� ������ ���������� */
  /* TransferStatus = FAILED, ���� ������ ������ */
  if (TransferStatus != FAILED)
  {
       /* �������� �������� ���� ���� �������� ������ ������� */
       //GPIO_SetBits(GPIOE, GPIO_Pin_1);//STM_EVAL_LEDOn(LED6);
			 LCD_DisplayStringLine(40,"DMA Check Ok");
			 LCD_DisplayStringLine(70,"src=dst");
  }
	else {
		LCD_DisplayStringLine(40,"DMA Check Err");
		LCD_DisplayStringLine(70,"src!=dst");
	}
}
TestStatus	Buffercmp(const uint32_t* pBuffer, uint32_t* pBuffer1, uint16_t BufferLength){
  
while(BufferLength--)
  {
    if(*pBuffer != *pBuffer1)
    {
      return FAILED;
    }
    pBuffer++;
    pBuffer1++;
  }
  return PASSED;  
}
void DMA2_Stream0_IRQHandler(void){
	
 /* ��������� ���� �� ��� ���������� ;) */
  if(DMA_GetITStatus(DMA_STREAM, DMA_IT_TCIF))
  {
    /* ������� ��� ��������� ���������� */
    DMA_ClearITPendingBit(DMA_STREAM, DMA_IT_TCIF);  
		GPIO_SetBits(GPIOE, GPIO_Pin_1);/* �������� ���� - ����� �������� */// STM_EVAL_LEDOn(LED5);
  }
}
void blink_LED(void){
	
		GPIO_SetBits(GPIOD, GPIO_Pin_12);	for(uint32_t i = 0; i < 100000; i++){}
		GPIO_ResetBits(GPIOD, GPIO_Pin_12);	for(uint32_t i = 0; i < 100000; i++){}
			
			GPIO_SetBits(GPIOD, GPIO_Pin_13);	for(uint32_t i = 0; i < 100000; i++){}
		GPIO_ResetBits(GPIOD, GPIO_Pin_13);	for(uint32_t i = 0; i < 100000; i++){}
			
			GPIO_SetBits(GPIOD, GPIO_Pin_14);	for(uint32_t i = 0; i < 100000; i++){}
		GPIO_ResetBits(GPIOD, GPIO_Pin_14);	for(uint32_t i = 0; i < 100000; i++){}
			
			GPIO_SetBits(GPIOD, GPIO_Pin_15);	for(uint32_t i = 0; i < 100000; i++){}
		GPIO_ResetBits(GPIOD, GPIO_Pin_15);	for(uint32_t i = 0; i < 100000; i++){}
		
}	
char *utoa_builtin_div(uint32_t value, char *buffer){     //http://we.easyelectronics.ru/Soft/preobrazuem-v-stroku-chast-1-celye-chisla.html
   buffer += 11; 
// 11 ���� ���������� ��� ����������� ������������� 32-� �������� �����
// �  ������������ ����
   *--buffer = 0;
   do
   {
      *--buffer = value % 10 + '0';
      value /= 10;
   }
   while (value != 0);
   return buffer;
}
void printf_ui32_buf(uint16_t * buf, uint8_t size_buf){
   
	char tmpbuf[20] = {0};
	char *ptmpbuf = tmpbuf;
	
		for(uint32_t i = 0; i < size_buf; i++)	{
			printf(utoa_builtin_div(*(buf + i), ptmpbuf + i));	printf(", "); 
		}	
		printf("\n");
}
uint32_t FT5216GM7_TIMEOUT_UserCallback(void){
	
	return 0;
}
uint16_t  FT5216GM7_Read(uint8_t DeviceAddr, uint8_t RegAddr, uint8_t* pBuffer, uint16_t NumByteToRead){    
  
	
	
	uint8_t transmissionDirection;
//																																			while(I2C_GetFlagStatus(I2C_TS, I2C_FLAG_BUSY)){}
//   I2C_GenerateSTART(I2C_TS, ENABLE);																	while(!I2C_CheckEvent(I2C_TS, I2C_EVENT_MASTER_MODE_SELECT)){}
//    I2C_Send7bitAddress(I2C_TS, 0x70, I2C_Direction_Transmitter);			while(!I2C_CheckEvent(I2C_TS, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)){}
//    I2C_SendData(I2C_TS, 0x70);																				while(!I2C_CheckEvent(I2C_TS, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){}    
//		I2C_SendData(I2C_TS, 0x5f);																				while(!I2C_CheckEvent(I2C_TS, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){}   
//		I2C_GenerateSTOP(I2C_TS, ENABLE);
	

//	/* Test on BUSY Flag */
//  FT5216GM7_Timeout = FT5216GM7_LONG_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_TS, I2C_FLAG_BUSY) != RESET)
//  {
//    if((FT5216GM7_Timeout--) == 0) return FT5216GM7_TIMEOUT_UserCallback();
//  }
	
  

  /* Wait until TXIS flag is set */
//  FT5216GM7_Timeout = FT5216GM7_LONG_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_TS, I2C_FLAG_TXE) == RESET)
//  {
//    if((FT5216GM7_Timeout--) == 0) return FT5216GM7_TIMEOUT_UserCallback();
//  }
  
	
//  if(NumByteToRead>1)
//      RegAddr |= 0x80;
	

	transmissionDirection = I2C_Direction_Transmitter;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	while(I2C_GetFlagStatus(I2C_TS, I2C_FLAG_BUSY)){}
   I2C_GenerateSTART(I2C_TS, ENABLE);																			while(!I2C_CheckEvent(I2C_TS, I2C_EVENT_MASTER_MODE_SELECT)){}
	
	
	I2C_Send7bitAddress(I2C_TS, DeviceAddr, transmissionDirection);
 
    if(transmissionDirection == I2C_Direction_Transmitter)    							while(!I2C_CheckEvent(I2C_TS, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
    if(transmissionDirection == I2C_Direction_Receiver)											while(!I2C_CheckEvent(I2C_TS, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));
																																			
	/* Send Register address */
  I2C_SendData(I2C_TS, (uint8_t)RegAddr);																		while(!I2C_CheckEvent(I2C_TS, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){}   
  //I2C_SendData(I2C_TS, 0x0);																								while(!I2C_CheckEvent(I2C_TS, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){}   
  I2C_GenerateSTOP(I2C_TS, ENABLE);

		
	transmissionDirection = I2C_Direction_Receiver;
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	



	while(I2C_GetFlagStatus(I2C_TS, I2C_FLAG_BUSY)){}	
	I2C_GenerateSTART  (I2C_TS, ENABLE);																			while(!I2C_CheckEvent(I2C_TS, I2C_EVENT_MASTER_MODE_SELECT)){}	
	
	I2C_Send7bitAddress(I2C_TS, DeviceAddr, transmissionDirection);
	
    if(transmissionDirection == I2C_Direction_Transmitter)    							while(!I2C_CheckEvent(I2C_TS, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
    if(transmissionDirection == I2C_Direction_Receiver)											while(!I2C_CheckEvent(I2C_TS, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));

	//I2C_SendData(I2C_TS,  (uint8_t)RegAddr);																	while(!I2C_CheckEvent(I2C_TS, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){}  
 	
 while (NumByteToRead)
  {   
    /* Wait until RXNE flag is set */
//    FT5216GM7_Timeout = FT5216GM7_LONG_TIMEOUT;
    while(I2C_GetFlagStatus(I2C_TS, I2C_FLAG_RXNE) == RESET)    
    {
   //   if((FT5216GM7_Timeout--) == 0) return FT5216GM7_TIMEOUT_UserCallback();
    }
    // while( !I2C_CheckEvent(I2C_TS, I2C_EVENT_MASTER_BYTE_RECEIVED) );
     /* Read data from RXDR */
    *pBuffer = I2C_ReceiveData(I2C_TS);
    /* Point to the next location where the byte read will be saved */
    pBuffer++;
    
    /* Decrement the read bytes counter */
    NumByteToRead--;
  } 
	
	I2C_GenerateSTOP(I2C_TS, ENABLE);	
	
	
	
//		/* Wait until TC flag is set */
//  FT5216GM7_Timeout = FT5216GM7_LONG_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_TS, I2C_FLAG_TXE) == RESET)
//  {
//    if((FT5216GM7_Timeout--) == 0) return FT5216GM7_TIMEOUT_UserCallback();
//  }  
  
  /* Configure slave address, nbytes, reload, end mode and start or stop generation */
  //I2C_TransferHandling(I2C_TS, DeviceAddr, NumByteToRead, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
  
  /* Wait until all data are received */
//  while (NumByteToRead)
//  {   
//    /* Wait until RXNE flag is set */
////    FT5216GM7_Timeout = FT5216GM7_LONG_TIMEOUT;
////    while(I2C_GetFlagStatus(I2C_TS, I2C_FLAG_RXNE) == RESET)    
////    {
////   //   if((FT5216GM7_Timeout--) == 0) return FT5216GM7_TIMEOUT_UserCallback();
////    }
//     while( !I2C_CheckEvent(I2C_TS, I2C_EVENT_MASTER_BYTE_RECEIVED) );
//    /* Read data from RXDR */
//    *pBuffer = I2C_ReceiveData(I2C_TS);
//    /* Point to the next location where the byte read will be saved */
//    pBuffer++;
//    
//    /* Decrement the read bytes counter */
//    NumByteToRead--;
//  } 
//  
//  /* Wait until STOPF flag is set */
//  FT5216GM7_Timeout = FT5216GM7_LONG_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_TS, I2C_FLAG_STOPF) == RESET)   
//  {
//    if((FT5216GM7_Timeout--) == 0) return FT5216GM7_TIMEOUT_UserCallback();
//  }

  /* Clear STOPF flag */
 // I2C_ClearFlag(I2C_TS, I2C_FLAG_STOPF);
  
  /* If all operations OK */
  return FT5216GM7_OK;  
}
uint32_t FT5216GM7_Read_XY(void){
	
	uint16_t X      = 0;
	uint16_t Y      = 0;

	FT5216GM7_Read(ADR_FT5216GM7, FT5216GM7_TOUCH1_XH, pRX_buf_REG_TS, 8);
	
			if(*pRX_buf_REG_TS == 128)	{Y =         *(pRX_buf_REG_TS + 1);}	
			if(*pRX_buf_REG_TS == 129)	{Y = 0xFF +  *(pRX_buf_REG_TS + 1);}

	FT5216GM7_Read(ADR_FT5216GM7, FT5216GM7_TOUCH1_YH, pRX_buf_REG_TS, 4);	
			
			if(RX_buf_REG_TS[0] == 0)		{X =               *(pRX_buf_REG_TS + 1);}					//????????
			if(RX_buf_REG_TS[0] == 1)		{X =  0xFF      + (*(pRX_buf_REG_TS + 1));}
			if(RX_buf_REG_TS[0] == 2)		{X = (0xFF * 2) + (*(pRX_buf_REG_TS + 1));}
			if(RX_buf_REG_TS[0] == 3)		{X = (0xFF * 3) + (*(pRX_buf_REG_TS + 1));}
			
			
	return 	(X 		| (((Y-478)*(-1)) << 16));	
}
void EXTI0_IRQHandler(void){

	static uint32_t cnt_irq = 0;			
	
	cnt_irq++;
	
	if(cnt_irq != 1){	
		
					XY_data[0] =	 FT5216GM7_Read_XY() & 0xFFFF; 
					XY_data[1] =	 FT5216GM7_Read_XY() >> 16;		
	  }
		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);	

		if(XY_ITEMS_1)			{		MenuItemIndex = 0; 	SelFunc();		} 
		if(XY_ITEMS_2)			{		MenuItemIndex = 1; 	SelFunc();		} 
		if(XY_ITEMS_3)		  {		MenuItemIndex = 2; 	SelFunc();		}
		if(XY_ITEMS_4)			{		MenuItemIndex = 3; 	SelFunc();		}
		if(XY_ITEMS_5)			{		MenuItemIndex = 4; 	SelFunc();		}
		if(XY_ITEMS_6)		  {		MenuItemIndex = 5; 	SelFunc();		}
		if(XY_ITEMS_7)			{		MenuItemIndex = 6; 	SelFunc();		}
	
		switch(Select_item_menu){
	/*------*/case MAINCONTROL:/*-----------------------------------------------------------------------------------------------------------*/
								if(XY_LAMPTOP)			set_mainctrl(MESS_LAMP_TOP);   
								if(XY_LAMPBOT)			set_mainctrl(MESS_LAMP_BOT);
								if(XY_LAMPSYSBOX)		set_mainctrl(MESS_LAMP_SYSBOX);
								if(XY_VENTFLOW)			set_mainctrl(MESS_VENTFLOW);
								if(XY_VENTVEGA)			set_mainctrl(MESS_VENTVEGA);
							break;
/*--------*/case	DEBUG_ITEM:/*-----------------------------------------------------------------------------------------------------------*/
							Graphics_setForegroundColor(&g_sContext, 0x0);		Graphics_fillRectangle(&g_sContext, &Clear_XY);
							Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
							
							Graphics_setFont(&g_sContext, &g_sFontCmss36);
							sprintf(pstr, "Y[pxl]=%#d", XY_data[1]);	Graphics_drawString(&g_sContext, (int8_t *)pstr,  AUTO_STRING_LENGTH, 600, 80,   TRANSPARENT_TEXT);		
							sprintf(pstr, "X[pxl]=%#d", XY_data[0]);	Graphics_drawString(&g_sContext, (int8_t *)pstr,  AUTO_STRING_LENGTH, 600, 122,  TRANSPARENT_TEXT);	
							Graphics_setFont(&g_sContext, &g_sFontCmss36b);
						break;
/*--------*/case	SVENT_ITEM:/*-----------------------------------------------------------------------------------------------------------*/
							if(XY_LEVEL_1)			set_state_level(1);
							if(XY_LEVEL_2)			set_state_level(2);
							if(XY_LEVEL_3)			set_state_level(3);
							if(XY_LEVEL_4)			set_state_level(4);
							if(XY_LEVEL_5)			set_state_level(5);
							if(XY_LEVEL_6)			set_state_level(6);
							if(XY_LEVEL_7)			set_state_level(7);
							if(XY_LEVEL_8)			set_state_level(8);
							if(XY_LEVEL_9)			set_state_level(9);
							if(XY_LEVEL_10)			set_state_level(10);
						break;
/*--------*/case	TIME_SUBITEM:/*-----------------------------------------------------------------------------------------------------------*/
							if(TIMESET_BUSY == RESET){	
								if(XY_UP_HOUR)					edit_RTC(&datatime.hours,			INC_PARAM_TIME);
								if(XY_UP_MIN)						edit_RTC(&datatime.minutes,	  INC_PARAM_TIME);
								if(XY_UP_SEC)						edit_RTC(&datatime.seconds,	  INC_PARAM_TIME);
								if(XY_DOWN_HOUR)				edit_RTC(&datatime.hours,			DEC_PARAM_TIME);
								if(XY_DOWN_MIN)					edit_RTC(&datatime.minutes,		DEC_PARAM_TIME);
								if(XY_DOWN_SEC)	  		  edit_RTC(&datatime.seconds, 	DEC_PARAM_TIME);
							}
						break;
/*--------*/case	DATA_SUBITEM:/*-----------------------------------------------------------------------------------------------------------*/
							if(TIMESET_BUSY == RESET){	
								if(XY_UP_DATA)					edit_RTC(&datatime.date,			INC_PARAM_TIME);
								if(XY_UP_MON)						edit_RTC(&datatime.month,	  	INC_PARAM_TIME);
								if(XY_UP_YEAR)					edit_RTC(&datatime.year,	  	INC_PARAM_TIME);
								if(XY_UP_DAY)						edit_RTC(&datatime.day,	  		INC_PARAM_TIME);
								if(XY_DOWN_DATA)				edit_RTC(&datatime.date,			DEC_PARAM_TIME);
								if(XY_DOWN_MON)					edit_RTC(&datatime.month,			DEC_PARAM_TIME);
								if(XY_DOWN_YEAR)	  		edit_RTC(&datatime.year, 			DEC_PARAM_TIME);
								if(XY_DOWN_DAY)	  		  edit_RTC(&datatime.day, 			DEC_PARAM_TIME);
							}
						break;
/*--------*/case	ALARM_SUBITEM:/*-----------------------------------------------------------------------------------------------------------*/
							if(TIMESET_BUSY == RESET){	
								if(XY_UP_HOUR)					edit_RTC(&AlarmTime.hours,			INC_PARAM_TIME);
								if(XY_UP_MIN)						edit_RTC(&AlarmTime.minutes,	  INC_PARAM_TIME);
								if(XY_UP_SEC)						edit_RTC(&AlarmTime.seconds,	  INC_PARAM_TIME);
								if(XY_DOWN_HOUR)				edit_RTC(&AlarmTime.hours,			DEC_PARAM_TIME);
								if(XY_DOWN_MIN)					edit_RTC(&AlarmTime.minutes,		DEC_PARAM_TIME);
								if(XY_DOWN_SEC)	  		  edit_RTC(&AlarmTime.seconds, 		DEC_PARAM_TIME);
								
								if(XY_SET_ALARMA)	  		{TM_RTC_SetAlarm(TM_RTC_Alarm_A, &AlarmTime, TM_RTC_Format_BIN);			Graphics_drawString(&g_sContext, "1",   AUTO_STRING_LENGTH, 20,  450,  TRANSPARENT_TEXT);}
								if(XY_SET_ALARMB)	  		{TM_RTC_SetAlarm(TM_RTC_Alarm_B, &AlarmTime, TM_RTC_Format_BIN);			Graphics_drawString(&g_sContext, "2",   AUTO_STRING_LENGTH, 20,  450,  TRANSPARENT_TEXT);}
								if(XY_RESET_ALARMA)	  	{TM_RTC_DisableAlarm(TM_RTC_Alarm_A);																	Graphics_drawString(&g_sContext, "3",   AUTO_STRING_LENGTH, 20,  450,  TRANSPARENT_TEXT);}
								if(XY_RESET_ALARMB)	  	{TM_RTC_DisableAlarm(TM_RTC_Alarm_B);																	Graphics_drawString(&g_sContext, "4",   AUTO_STRING_LENGTH, 20,  450,  TRANSPARENT_TEXT);}
							}
						break;
/*--------*/case	LIGTH_SUBITEM:/*-----------------------------------------------------------------------------------------------------------*/
							if(XY_LEVEL_1)			set_ligth_level(1);
							if(XY_LEVEL_2)			set_ligth_level(2);
							if(XY_LEVEL_3)			set_ligth_level(3);
							if(XY_LEVEL_4)			set_ligth_level(4);
							if(XY_LEVEL_5)			set_ligth_level(5);
							if(XY_LEVEL_6)			set_ligth_level(6);
							if(XY_LEVEL_7)			set_ligth_level(7);
							if(XY_LEVEL_8)			set_ligth_level(8);
							if(XY_LEVEL_9)			set_ligth_level(9);
							if(XY_LEVEL_10)			set_ligth_level(10);
						break;
							
						default : break;
		}	
		EXTI_ClearITPendingBit(EXTI_Line0);
}
void USART1_IRQHandler(void){


}
void TM_RTC_AlarmAHandler(void){

	Graphics_drawString(&g_sContext, "TM_RTC_AlarmAHandler",    AUTO_STRING_LENGTH, 350, 350,  TRANSPARENT_TEXT);
}
#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif





/************************ (C) COPYRIGHT �� STMicroelectronics *****END OF FILE****/
