#ifndef __RTC_H__
#define __RTC_H__

#include "tm_stm32f4_rtc.h"
#include "grlib.h"
#include "stdio.h"
#include "menu.h"


#define INC_PARAM_TIME   0	
#define DEC_PARAM_TIME   1

extern Graphics_Context g_sContext;
extern  uint8_t Select_item_menu;
typedef struct {
	uint16_t year;
	uint8_t	month;
	uint8_t day;
} date_s;

typedef struct {
	uint8_t hours;
	uint8_t minutes;
	uint8_t seconds;
	uint8_t centiseconds;
} time_s;

extern FlagStatus TIMESET_BUSY;



/*------ Prototype function ---------------*/
void RTC_init									(void);
void TM_RTC_RequestHandler		(void);
void Delay_ms_RCC							(uint32_t ms);
void edit_RTC(uint8_t *param, uint8_t incdec);
#endif // __RTC_H__
