#include "LCD.h"

	Graphics_Rectangle Rect_top         = {320,  60, 780,  65};
	Graphics_Rectangle Rect_bottom      = {320, 436, 780, 441};
	Graphics_Rectangle Rect_left        = {320,  60, 325, 440};
	Graphics_Rectangle Rect_rigth       = {776,  60, 781, 440};
	Graphics_Rectangle main_Rect5       = {320,  60, 325, 300};

	void Lcd_Write_Data_DMA(uint16_t data);
	
void LCD_DrawChar(uint16_t Ypos, uint16_t Xpos, const uint16_t *c){
   
	uint32_t index = 0, i = 0;
  uint16_t  Xaddress = 0;
	uint16_t  Yaddress = 0;
	
  Xaddress = Xpos;
  Yaddress = Ypos;
  
	LCD_SetCursor(Xaddress, Yaddress);

//	Start_GRAM();
  for(index = 0; index < LCD_Currentfonts->Height; index++)
  {
   	Start_GRAM();			/* Prepare to write GRAM */
    for(i = 0; i < LCD_Currentfonts->Width; i++)
    {
      if((((c[index] & ((0x80 << ((LCD_Currentfonts->Width / 12 ) * 8 ) ) >> i)) == 0x00) &&(LCD_Currentfonts->Width <= 12))||
        (((c[index] & (0x1 << i)) == 0x00)&&(LCD_Currentfonts->Width > 12 )))
						{
							Lcd_Write_Data(BackColor);
						}
						else
						{
							Lcd_Write_Data(TextColor);
						} 
    }
    Yaddress++;
		LCD_SetCursor(Xaddress, Yaddress);
  }
}
void LCD_DisplayChar(uint16_t Line, uint16_t Column, uint8_t Ascii){
 
	Ascii -= 32;
  LCD_DrawChar(Line, Column, &LCD_Currentfonts->table[Ascii * LCD_Currentfonts->Height]);
}
void LCD_DisplayStringLine(uint16_t Line, uint8_t *ptr)																			{
  
	uint16_t refcolumn = 70;//uint16_t refcolumn = LCD_PIXEL_WIDTH - 1;

  /* Send the string character by character on lCD */
  while ((*ptr != 0) & (((refcolumn + 1) & 0xFFFF) >= LCD_Currentfonts->Width))
  {
    /* Display one character on LCD */
    LCD_DisplayChar(Line, refcolumn, *ptr);				//LCD_DisplayChar(refcolumn, Line, *ptr); - ZXY
    /* Decrement the column position by 16 */
    refcolumn += LCD_Currentfonts->Width;
    /* Point on the next character */
    ptr++;
  }
}
void LCD_DrawLine(uint16_t Xpos, uint16_t Ypos, uint16_t Length, uint8_t Direction){
  uint32_t i = 0;
  
  LCD_SetCursor(Xpos, Ypos);
  if(Direction == LCD_DIR_HORIZONTAL)
  {
    Start_GRAM(); /* Prepare to write GRAM */
    for(i = 0; i < Length; i++)
    {
      Lcd_Write_Data(TextColor);
    }
  }
  else		
  {
    for(i = 0; i < Length; i++)
    {
      Start_GRAM(); /* Prepare to write GRAM */
      Lcd_Write_Data(TextColor);
      Xpos++;
      LCD_SetCursor(Xpos, Ypos);
    }
  }
}
void LCD_SetFont(sFONT *fonts){
  
	LCD_Currentfonts = fonts;
}
void drawMainMenu(void){

  Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
  Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_BLACK);
	
	Graphics_setFont(&g_sContext, &g_sFontCmss20b);
	Graphics_drawString(&g_sContext, "GrowTech v2.0 GosNIIPlab 2019",    AUTO_STRING_LENGTH, 480, 450,  TRANSPARENT_TEXT);
	Graphics_setFont(&g_sContext, &g_sFontCmss36b);
	
	Menu_Init();
	DisplayMenu(60,20);	
//	DisplayButtomMenu();
	
	Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);		Graphics_fillRectangle(&g_sContext, &Rect_top);	
	Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);		Graphics_fillRectangle(&g_sContext, &Rect_left);
	Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);		Graphics_fillRectangle(&g_sContext, &Rect_bottom);	
	Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);		Graphics_fillRectangle(&g_sContext, &Rect_rigth);	
}
void draw_button(uint32_t X, uint32_t Y, uint32_t length, uint32_t height, FunctionalState State, uint8_t * str){

	uint64_t i, j = 0;
	
	if(State == ENABLE){
	
		for( j = 0; j < height; j++)	{		LCD_SetCursor(X, j + Y);		Start_GRAM();				for( i = 0; i < length; i++)			Lcd_Write_Data(0xFFFF);	}
			
			Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLUE);	
			Graphics_drawString(&g_sContext, (int8_t *)str,  AUTO_STRING_LENGTH, X+10, Y,  TRANSPARENT_TEXT);	
	}
	else{
	
		for( j = 0; j < height; j++)	{		LCD_SetCursor(X, j + Y);		Start_GRAM();				for( i = 0; i < length; i++)			Lcd_Write_Data(0xFF);	}
			
			Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);	
			Graphics_drawString(&g_sContext, (int8_t *)str,  AUTO_STRING_LENGTH, X+10, Y,  TRANSPARENT_TEXT);	
	
	
	}
	
	

}
void draw_rect(uint32_t X, uint32_t Y, uint32_t length, uint32_t height, uint16_t color){

	uint64_t i, j = 0;
//		uint64_t color2 = 0;
	
		for( j = 0; j < height; j++)	{LCD_SetCursor(X, j + Y);		Start_GRAM();		for( i = 0; i < length; i++)		Lcd_Write_Data(color);}
		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);	
	
//	
//		uint64_t i, j = 0;
//	
//		for( j = 0; j < height; j++)	{
//		
////				if((j<10)&(j>0))   		color2=0xF800;//Lcd_Write_Data(0xF800);
////				if((j<20)&(j>10))   	color2=0x7E0;//Lcd_Write_Data(0x7E0);
////				if((j<30)&(j>20)) 		color2=0X7F;//Lcd_Write_Data(0X7F);
////				if((j<40)&(j>30)) 	  
//					color2=(0x7E0|0xF800);//Lcd_Write_Data(0xF800|0x7f);	
//			
//			
//			
//			
//			LCD_SetCursor(X, j + Y);		
//			Start_GRAM();		
//			for( i = 0; i < length; i++)		{
//				
//				Lcd_Write_Data(color2);
//			}
//		}
//		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);	
	
	
}
void Start_GRAM(void){
	
	Lcd_Write_Index(0X2C00); 			//Lcd_Write_Data(0x2c);//
	//for(uint32_t i = 0; i < 1; i++){}//MDELAY(10); 
}
void Stop_GRAM(void){
	
	Lcd_Write_Index(0X3C00); //
	for(uint32_t i = 0; i < 1; i++){}//MDELAY(10); 
}
void LCD_SetCursor(uint16_t Xpos, uint16_t Ypos){
	
	Lcd_Write_Index(0x2A00);	if (Xpos <= 255)    		              Lcd_Write_Data(0);
														if((Xpos > 	255)	&	(Xpos <= 510))		Lcd_Write_Data(1);
														if((Xpos > 	510)	&	(Xpos <= 765)) 	  Lcd_Write_Data(2);
														if((Xpos > 	765)	&	(Xpos <= 863)) 	  Lcd_Write_Data(3);
														
	Lcd_Write_Index(0x2A01);	Lcd_Write_Data(Xpos);	  //X1

	Lcd_Write_Index(0x2B00);	if(Ypos <= 255)										Lcd_Write_Data(0);
														if(Ypos >  255)										Lcd_Write_Data(1);
	Lcd_Write_Index(0x2B01);	Lcd_Write_Data(Ypos); 	//Y1	
}
void LCD_SetCursorEnd(uint16_t Xpos, uint16_t Ypos){

	Lcd_Write_Index(0x2A02);	Lcd_Write_Data(0);
	Lcd_Write_Index(0x2A03);	Lcd_Write_Data(Xpos);	//X2	

	Lcd_Write_Index(0x2B02);	Lcd_Write_Data(0);
	Lcd_Write_Index(0x2B03);	Lcd_Write_Data(Ypos); 	//Y2
	
//  LCD_WriteReg(0x004E, Xpos);
//  LCD_WriteReg(0x004F, Ypos);
//  Address_set(Xpos,Ypos,Xpos,Ypos);
}
void LCD_Clear(uint16_t Color){
  
	uint32_t index = 0;
	
  LCD_SetCursor(0, 0);
	Start_GRAM();	
  //   *(uint16_t *) (LCD_REG) = 0x45; // LCD_WriteRAM_Prepare(); /* Prepare to write GRAM */
  for(index = 0; index < 854 * 480 ; index++)//409914    854 * 240
  {
    Lcd_Write_Data(Color & 0xFFFF);
  }
}
void Fill_Rectangle(uint16_t X1, uint16_t X2, uint16_t Y1, uint16_t Y2, uint16_t Color){
  
//	uint32_t index = 0;
	
  LCD_SetCursor   (X1, Y1);
	LCD_SetCursorEnd(X2, Y2);
	
	Start_GRAM();	
	
//  for(index = 0; index < 4000000 ; index+2)				//409914    854 * 240
//  {
//  Lcd_Write_Data(    ((gImage_zxcv[index+1]) & 0xFF) );//| ((gImage_zxcv[index + 1]) << 8))     ; 
//		//Lcd_Write_Data(Color & 0xFFFF);
//  }
}
void Lcd_Write_Index(uint16_t index){
   
		*(uint16_t *) (LCD_REG) = index;
}
void Lcd_Write_Data(uint16_t data){
   
		*(uint16_t *) (LCD_DATA)= data;
}
uint16_t Lcd_Read_Data(void){
   uint16_t data = * (uint16_t *)(LCD_DATA);
   return data;
}
uint16_t Lcd_Read_Reg(uint16_t reg_addr){
   
	 volatile uint16_t data = 0;
   Lcd_Write_Index(reg_addr);
   data = Lcd_Read_Data();
   return data;
}
void Lcd_Write_Reg(uint16_t reg,uint16_t value)	{
	
   *(uint16_t *) (LCD_REG) = reg;
   *(uint16_t *) (LCD_DATA) = value;
}
