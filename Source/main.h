/**
  ******************************************************************************
  * @file    LTDC/LTDC_ColorKeying/main.h 
  * @author  MCD Application Team
  * @version V1.4.0
  * @date    04-August-2014
  * @brief   Header for main.c module
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#include "stm32f4xx.h"                  // Device header
#include "grlib.h"
#include "misc.h"
#include "stdlib.h"
#include "stdio.h"
#include "tm_stm32f4_rtc.h"

#include "kitronix320x240x16_ssd2119_spi.h"
#include "images.h"
#include "menu.h"
#include "init.h"
#include "LCD.h"
#include "RTC.h"

#include "stm32f4xx_tim.h"
#include "stm32f4xx_adc.h"

#include "stm32f4xx_usart.h"

#define ADR_FT5216GM7 	  0x70

#define FT5216GM7_FLAG_TIMEOUT    ((uint32_t)0x1000)
#define FT5216GM7_LONG_TIMEOUT    ((uint32_t)(10 * FT5216GM7_FLAG_TIMEOUT)) 

static	uint8_t  									RX_buf_REG_TS[8]	=	{0};
static	uint8_t *pRX_buf_REG_TS = RX_buf_REG_TS;
	
	__IO uint32_t  FT5216GM7_Timeout = FT5216GM7_LONG_TIMEOUT; 
	
#define FT5216GM7_OK               ((uint32_t) 0)
#define FT5216GM7_FAIL             ((uint32_t) 0)

/***********************************************/
/********* START REGISTER MAPPING  *************/
/***********************************************/
#define FT5216GM7_GEST_ID			   						0x01  
#define FT5216GM7_TD_STATUS      						0x02

#define FT5216GM7_TOUCH1_XH      						0x03
#define FT5216GM7_TOUCH1_XL      						0x04
#define FT5216GM7_TOUCH1_YH									0x05
#define FT5216GM7_TOUCH1_YL      						0x06
#define FT5216GM7_TOUCH1_WEIGTH 						0x07
#define FT5216GM7_TOUCH1_MISC    						0x08
#define FT5216GM7_TOUCH2_XH      						0x09
#define FT5216GM7_TOUCH2_XL      						0x0A
#define FT5216GM7_TOUCH2_YH      						0x0B
#define FT5216GM7_TOUCH2_YL      						0x0C
#define FT5216GM7_TOUCH2_WEIGTH  						0x0D
#define FT5216GM7_TOUCH2_MISC    						0x0E
#define FT5216GM7_TOUCH3_XH      						0x0F
#define FT5216GM7_TOUCH3_XL      						0x10
#define FT5216GM7_TOUCH3_YH      						0x11
#define FT5216GM7_TOUCH3_YL      						0x12
#define FT5216GM7_TOUCH3_WEIGTH  						0x13
#define FT5216GM7_TOUCH3_MISC    						0x14
#define FT5216GM7_TOUCH4_XH      						0x15
#define FT5216GM7_TOUCH4_XL      						0x16
#define FT5216GM7_TOUCH4_YH      						0x17
#define FT5216GM7_TOUCH4_YL      						0x18
#define FT5216GM7_TOUCH4_WEIGTH  						0x19
#define FT5216GM7_TOUCH4_MISC    						0x1A
#define FT5216GM7_TOUCH5_XH      						0x1B
#define FT5216GM7_TOUCH5_XL      						0x1C
#define FT5216GM7_TOUCH5_YH      						0x1D
#define FT5216GM7_TOUCH5_YL      						0x1E
#define FT5216GM7_TOUCH5_WEIGTH  						0x1F
#define FT5216GM7_TOUCH5_MISC    						0x20

#define FT5216GM7_ID_G_THGROUP							0x80
#define FT5216GM7_ID_G_THPEAK								0x81
#define FT5216GM7_ID_G_THCAL								0x82
#define FT5216GM7_ID_G_COMPENSATE_STATUS		0x83
#define FT5216GM7_ID_G_COMPENSATE_FLAG			0x84
#define FT5216GM7_ID_G_THDIFF								0x85
#define FT5216GM7_ID_G_CTRL									0x86
#define FT5216GM7_ID_G_TIME_ENTER_MONITOR		0x87
#define FT5216GM7_ID_G_PERIOD_ACTIVE				0x88
#define FT5216GM7_ID_G_PERIODMONITOR				0x89
#define FT5216GM7_ID_G_SCANRATE							0x8A
#define FT5216GM7_ID_G_CHARGER_STATE				0x8B
#define FT5216GM7_ID_G_SCAN_REGB						0x8C
#define FT5216GM7_ID_G_SCAN_CAP							0x8D
#define FT5216GM7_ID_G_SCAN_FILTERMODE			0x8E
#define FT5216GM7_ID_G_SCAN_REFLASH					0x8F
#define FT5216GM7_ID_G_MOVE_STH_I						0x90
#define FT5216GM7_ID_G_MOVESTH_N						0x91
#define FT5216GM7_ID_G_LEFT_RIGTH_OFFSET		0x92
#define FT5216GM7_ID_G_UP_DOWN_OFFSET				0x93
#define FT5216GM7_ID_G_DISTANCE_LEFT_RIGTH	0x94
#define FT5216GM7_ID_G_DISTANCE_UP_DOWN			0x95

#define FT5216GM7_ID_G_ZOOM_DIS_SQR					0x97
#define FT5216GM7_ID_G_MAX_X_HIGH						0x98
#define FT5216GM7_ID_G_MAX_X_LOW						0x99
#define FT5216GM7_ID_G_MAX_Y_HIGH						0x9A
#define FT5216GM7_ID_G_MAX_Y_LOW						0x9B

#define FT5216GM7_ID_G_K_X_HIGH							0x9C
#define FT5216GM7_ID_G_K_X_LOW							0x9D
#define FT5216GM7_ID_G_K_Y_HIGH							0x9E
#define FT5216GM7_ID_G_K_Y_LOW  						0x9F
	
#define FT5216GM7_ID_G_AUTO_CLB_MODE				0xA0
#define FT5216GM7_ID_G_LIB_VERSION_H				0xA1
#define FT5216GM7_ID_G_LIB_VERSION_L				0xA2
#define FT5216GM7_ID_G_CIPHER								0xA3
#define FT5216GM7_ID_G_MODE									0xA4
#define FT5216GM7_ID_G_PMODE								0xA5
#define FT5216GM7_ID_G_FIRMID								0xA6	
#define FT5216GM7_ID_G_STATE								0xA7
#define FT5216GM7_ID_G_VENODRID							0xA8
#define FT5216GM7_ID_G_ERR									0xA9
#define FT5216GM7_ID_G_CLB									0xAA
#define FT5216GM7_ID_G_STATIC_TH						0xAB
#define FT5216GM7_ID_G_MID_SPEED_TH					0xAC
#define FT5216GM7_ID_G_HIGH_SPEED_TH				0xAD
#define FT5216GM7_ID_G_DRAW_LINE_TH					0xAE
#define FT5216GM7_ID_G_RELEASE_CODE_ID			0xAF
#define FT5216GM7_ID_G_FACE_DEC_MODE				0xB0

#define FT5216GM7_ID_G_PRESIZE_EN						0xB2
#define FT5216GM7_ID_G_BIGAREA_PEAK_TH			0xB3
#define FT5216GM7_ID_G_BIGAREA_PEAK_NUM			0xB4

#define FT5216GM7_LOG_MSG_CNT								0xFE
#define FT5216GM7_LOG_CUR_CHA								0xFF


	
	uint32_t   XY_data[2] = {0};
	uint32_t *pXY_data = XY_data;

	
/*******************************************************************************************************
��� ������� �� SWD
*******************************************************************************************************/
//#ifdef DEBUG_SWD
#define ITM_Port8(n)    (*((volatile unsigned char *)(0xE0000000+4*n)))
#define ITM_Port16(n)   (*((volatile unsigned short*)(0xE0000000+4*n)))
#define ITM_Port32(n)   (*((volatile unsigned long *)(0xE0000000+4*n)))
#define DEMCR           (*((volatile unsigned long *)(0xE000EDFC)))
#define TRCENA          0x01000000

struct __FILE { int handle; /* Add whatever you need here */ };
FILE __stdout;
FILE __stdin;

int fputc(int ch, FILE *f) 
   {
       if (DEMCR & TRCENA) 
        {
            while (ITM_Port32(0) == 0);
            ITM_Port8(0) = ch;
        }
    return(ch);
    }

		
#define  DELAY(x)			for(uint32_t i = 0; i < x; i++){}
	

	
#define I2C_TS             I2C1//interface number

#define SET_A0   	  GPIO_SetBits  (GPIOA, GPIO_Pin_0)
#define RESET_A0   	GPIO_ResetBits  (GPIOA, GPIO_Pin_0)

#define SET_D12   	  GPIO_SetBits  (GPIOD, GPIO_Pin_12)
#define RESET_D12   	GPIO_ResetBits  (GPIOD, GPIO_Pin_12)

#define SET_D13   	  GPIO_SetBits  (GPIOD, GPIO_Pin_13)
#define RESET_D13   	GPIO_ResetBits  (GPIOD, GPIO_Pin_13)
	


/*******************************************************************************************************
	DMA definision
*******************************************************************************************************/
#define BUFFER_SIZE              6248
#define DMA_IT_TCIF              DMA_IT_TCIF0
#define ARRAYSIZE 6248

uint16_t src_Const_Buffer[ARRAYSIZE] = {0};
uint16_t *psource      = src_Const_Buffer;

uint16_t dst_Const_Buffer[ARRAYSIZE] = {0};
uint16_t *pdestination = dst_Const_Buffer;

uint16_t      source[ARRAYSIZE];
uint16_t destination[ARRAYSIZE];

volatile uint16_t     buff[ARRAYSIZE] = {0};
volatile uint16_t old_buff[ARRAYSIZE] = {0xff};



typedef enum {FAILED = 0, PASSED = !FAILED} TestStatus;
__IO TestStatus  TransferStatus = FAILED;

uint32_t DST_Buffer[BUFFER_SIZE];

uint16_t   Blue_BackGround = BLUE_RGB565;//{123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123};
uint16_t *pBlue_BackGround = &Blue_BackGround;
uint16_t   Gray_BackGround = GRAY_RGB565;//{123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123,123};
uint16_t *pGray_BackGround = &Gray_BackGround;
	
char  str[11] = {0};
char *pstr  = str;

Graphics_Context g_sContext;
/*------------------------ TouchScreen Coordinate Difinision---------------------------------------------*/

#define XY_LEVEL_1	 (XY_data[1] > 120)&&(XY_data[1] < 180)  &&   (XY_data[0] > 360)&&(XY_data[0] < 390)
#define XY_LEVEL_2	 (XY_data[1] > 120)&&(XY_data[1] < 180)  &&   (XY_data[0] > 400)&&(XY_data[0] < 430)
#define XY_LEVEL_3	 (XY_data[1] > 120)&&(XY_data[1] < 180)  &&   (XY_data[0] > 440)&&(XY_data[0] < 470)
#define XY_LEVEL_4   (XY_data[1] > 120)&&(XY_data[1] < 180)  &&   (XY_data[0] > 480)&&(XY_data[0] < 510)
#define XY_LEVEL_5   (XY_data[1] > 120)&&(XY_data[1] < 180)  &&   (XY_data[0] > 520)&&(XY_data[0] < 550)
#define XY_LEVEL_6   (XY_data[1] > 120)&&(XY_data[1] < 180)  &&   (XY_data[0] > 560)&&(XY_data[0] < 590)
#define XY_LEVEL_7   (XY_data[1] > 120)&&(XY_data[1] < 180)  &&   (XY_data[0] > 600)&&(XY_data[0] < 630)
#define XY_LEVEL_8   (XY_data[1] > 120)&&(XY_data[1] < 180)  &&   (XY_data[0] > 640)&&(XY_data[0] < 670)
#define XY_LEVEL_9   (XY_data[1] > 120)&&(XY_data[1] < 180)  &&   (XY_data[0] > 680)&&(XY_data[0] < 710)
#define XY_LEVEL_10  (XY_data[1] > 120)&&(XY_data[1] < 180)  &&   (XY_data[0] > 720)&&(XY_data[0] < 750)

#define XY_UP_HOUR   		(XY_data[1] > 150)&&(XY_data[1] < 190)  &&   (XY_data[0] > 480)&&(XY_data[0] < 600)
#define XY_UP_MIN    		(XY_data[1] > 200)&&(XY_data[1] < 240)  &&   (XY_data[0] > 480)&&(XY_data[0] < 600)
#define XY_UP_SEC    		(XY_data[1] > 250)&&(XY_data[1] < 290)  &&   (XY_data[0] > 480)&&(XY_data[0] < 600)
#define XY_DOWN_HOUR 		(XY_data[1] > 150)&&(XY_data[1] < 190)  &&   (XY_data[0] > 620)&&(XY_data[0] < 740)
#define XY_DOWN_MIN  		(XY_data[1] > 200)&&(XY_data[1] < 240)  &&   (XY_data[0] > 620)&&(XY_data[0] < 740)
#define XY_DOWN_SEC  		(XY_data[1] > 250)&&(XY_data[1] < 290)  &&   (XY_data[0] > 620)&&(XY_data[0] < 740)

#define XY_SET_ALARMA		(XY_data[1] > 320)&&(XY_data[1] < 360)  &&   (XY_data[0] > 340)&&(XY_data[0] < 530)
#define XY_SET_ALARMB		(XY_data[1] > 370)&&(XY_data[1] < 410)  &&   (XY_data[0] > 340)&&(XY_data[0] < 530)
#define XY_RESET_ALARMA	(XY_data[1] > 320)&&(XY_data[1] < 360)  &&   (XY_data[0] > 550)&&(XY_data[0] < 740)
#define XY_RESET_ALARMB	(XY_data[1] > 370)&&(XY_data[1] < 410)  &&   (XY_data[0] > 550)&&(XY_data[0] < 740)


#define XY_UP_DATA   	(XY_data[1] > 150)&&(XY_data[1] < 190)  &&   (XY_data[0] > 480)&&(XY_data[0] < 600)
#define XY_UP_MON    	(XY_data[1] > 200)&&(XY_data[1] < 240)  &&   (XY_data[0] > 480)&&(XY_data[0] < 600)
#define XY_UP_YEAR    (XY_data[1] > 250)&&(XY_data[1] < 290)  &&   (XY_data[0] > 480)&&(XY_data[0] < 600)
#define XY_UP_DAY			(XY_data[1] > 300)&&(XY_data[1] < 340)  &&   (XY_data[0] > 480)&&(XY_data[0] < 600)
#define XY_DOWN_DATA  (XY_data[1] > 150)&&(XY_data[1] < 190)  &&   (XY_data[0] > 620)&&(XY_data[0] < 740)
#define XY_DOWN_MON  	(XY_data[1] > 200)&&(XY_data[1] < 240)  &&   (XY_data[0] > 620)&&(XY_data[0] < 740)
#define XY_DOWN_YEAR	(XY_data[1] > 250)&&(XY_data[1] < 290)  &&   (XY_data[0] > 620)&&(XY_data[0] < 740)
#define XY_DOWN_DAY 	(XY_data[1] > 300)&&(XY_data[1] < 340)  &&   (XY_data[0] > 620)&&(XY_data[0] < 740)






#define XY_ITEMS_1    (XY_data[1] >  60)&&(XY_data[1] < 100)  &&   (XY_data[0] > 20)&&(XY_data[0] < 290)
#define XY_ITEMS_2    (XY_data[1] > 105)&&(XY_data[1] < 145)  &&   (XY_data[0] > 20)&&(XY_data[0] < 290)
#define XY_ITEMS_3    (XY_data[1] > 150)&&(XY_data[1] < 190)  &&   (XY_data[0] > 20)&&(XY_data[0] < 290) 
#define XY_ITEMS_4    (XY_data[1] > 195)&&(XY_data[1] < 235)  &&   (XY_data[0] > 20)&&(XY_data[0] < 290)        
#define XY_ITEMS_5    (XY_data[1] > 240)&&(XY_data[1] < 280)  &&   (XY_data[0] > 20)&&(XY_data[0] < 290)        
#define XY_ITEMS_6    (XY_data[1] > 285)&&(XY_data[1] < 325)  &&   (XY_data[0] > 20)&&(XY_data[0] < 290)        
#define XY_ITEMS_7    (XY_data[1] > 330)&&(XY_data[1] < 370)  &&   (XY_data[0] > 20)&&(XY_data[0] < 290) 
#define XY_RETURN     (XY_data[1] > 375)&&(XY_data[1] < 415)  &&   (XY_data[0] > 20)&&(XY_data[0] < 290) 

#define XY_LAMPTOP    (XY_data[1] >  80)&&(XY_data[1] < 140)  &&   (XY_data[0] > 600)&&(XY_data[0] < 765)
#define XY_LAMPBOT    (XY_data[1] > 150)&&(XY_data[1] < 210)  &&   (XY_data[0] > 600)&&(XY_data[0] < 765)
#define XY_LAMPSYSBOX (XY_data[1] > 220)&&(XY_data[1] < 280)  &&   (XY_data[0] > 600)&&(XY_data[0] < 765) 
#define XY_VENTFLOW   (XY_data[1] > 290)&&(XY_data[1] < 350)  &&   (XY_data[0] > 600)&&(XY_data[0] < 765)        
#define XY_VENTVEGA   (XY_data[1] > 360)&&(XY_data[1] < 420)  &&   (XY_data[0] > 600)&&(XY_data[0] < 765)        

	
	
#define ADCx_DR_ADDRESS          ((uint32_t)0x4001224C)

		
	
extern u32 MenuItemIndex, nMenuLevel;
extern uint8_t Select_item_menu;	
extern	Graphics_Rectangle Clear_XY;	
extern uint8_t state_menu;
/*******************************************************************************************************
Utils function
********************************************************************************************************/
void 				init_ADC									(void);
void 				DMA_M2M_Send							(uint32_t DMA_PeriphAddr, uint32_t DMA_MemAddr, uint32_t DMA_BufSize);
void 				DMA_check_MemtoMem				(void);
TestStatus	Buffercmp									(const uint32_t* pBuffer, uint32_t* pBuffer1, uint16_t BufferLength);	
void 				DMA2_Stream0_IRQHandler		(void);
void 				DMA_STREAM_IRQHANDLER			(void);
void 				blink_LED									(void);
char * 			utoa_builtin_div					(uint32_t value, char *buffer);
void 				printf_ui32_buf						(uint16_t * buf, uint8_t size_buf);
void 				DMA_sendData_FSMC					(void);
/*******************************************************************************************************
SD card function
********************************************************************************************************/
void 				SD_LowLevel_Init					(void);
void 				SD_Init										(void);
void 				SD_WriteSingleBlock				(uint8_t *buf, uint32_t blk);
void 				SD_ReadSingleBlock				(uint8_t *buf, uint32_t blk);
//void SD_WaitReadWriteEnd(void);
void 				SD_WaitTransmissionEnd		(void);
void 				SD_StartMultipleBlockWrite(uint32_t blk);
void 				SD_WriteData							(uint8_t *buf, uint32_t cnt);
void 				SD_StopMultipleBlockWrite	(void);

static uint32_t SD_Command						(uint32_t cmd, uint32_t resp, uint32_t arg);
static uint32_t SD_Response						(uint32_t *response, uint32_t type);
static void 		SD_Panic							(uint32_t code, char *message);
static void 		SD_StartBlockTransfer	(uint8_t *buf, uint32_t cnt, uint32_t dir);

uint32_t		FT5216GM7_Read_XY(void);
uint32_t 		FT5216GM7_TIMEOUT_UserCallback(void);
uint16_t 		FT5216GM7_Read(uint8_t  DeviceAddr, 
													 uint8_t  RegAddr, 
													 uint8_t* pBuffer, 
													 uint16_t NumByteToRead);
#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
