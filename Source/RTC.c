#include "RTC.h"

date_s sys_date;
time_s sys_time;
time_s my_time;

uint8_t RTC_second_flag = 0;

char str_lcd[20] = {0};
char *pstr_lcd = str_lcd;

Graphics_Rectangle Clear_Rect_Time        = {530,0,780,59};
Graphics_Rectangle Clear_Rect_TimeSubMenu = {390,90,700,130};
Graphics_Rectangle Clear_Rect_izm         = {15,420,300,465};
TM_RTC_Time_t datatime;

extern TM_RTC_AlarmTime_t  AlarmTime;
extern __IO uint16_t uwADCxConvertedVoltage;

void RTC_init								(void)				{

	if (!TM_RTC_Init(TM_RTC_ClockSource_Internal)) {}
	//Set wakeup interrupt every 1 second
	TM_RTC_Interrupts(TM_RTC_Int_1s);
	
	/* Set date and time */
		datatime.date    = 6;
		datatime.day     = 3;
		datatime.month   = 11;
		datatime.year    = 19;
		datatime.hours   = 20;
		datatime.minutes = 0;
		datatime.seconds = 0;
		
		/* Set date and time */
		TM_RTC_SetDateTime(&datatime, TM_RTC_Format_BIN);
}
void TM_RTC_RequestHandler	(void) 				{
	
	char  strmonth[10] = {0};
	char  *pstrmonth = strmonth;
	
	char  strday  [10] = {0};	
	char  *pstrday   = strday;
	
	static uint32_t temp_uwADCxConvertedVoltage;
	TM_RTC_GetDateTime(&datatime, TM_RTC_Format_BIN);
	
	
	
	if((Select_item_menu == TIME_SUBITEM)|(Select_item_menu == DATA_SUBITEM)|(Select_item_menu == ALARM_SUBITEM)	)	{	
		
		Graphics_setForegroundColor(&g_sContext, 0x0);		
		Graphics_fillRectangle(&g_sContext, &Clear_Rect_TimeSubMenu);
	}		
		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLUE);		
		Graphics_fillRectangle(&g_sContext, &Clear_Rect_Time);
		
	
		switch (datatime.month){
		
			case 1		:	 pstrmonth = "Jan"; break;
			case 2		:	 pstrmonth = "Feb"; break;
			case 3		:	 pstrmonth = "Mar"; break;
			case 4		:	 pstrmonth = "Apr"; break;
			case 5		:	 pstrmonth = "May"; break;
			case 6		:	 pstrmonth = "Jun"; break;
			case 7		:	 pstrmonth = "Jul"; break;
			case 8		:	 pstrmonth = "Avg"; break;
			case 9		:	 pstrmonth = "Sep"; break;
			case 10		:	 pstrmonth = "Okt"; break;
			case 11		:  pstrmonth = "Nov"; break;
			case 12		:  pstrmonth = "Dec"; break;
			default 	:										break;
		}
		switch (datatime.day){
		
			case 1		:	 pstrday = "Mon"; break;
			case 2		:	 pstrday = "Tue"; break;
			case 3		:	 pstrday = "Wed"; break;
			case 4		:	 pstrday = "Thu"; break;
			case 5		:	 pstrday = "Fri"; break;
			case 6		:	 pstrday = "Sat"; break;
			case 7		:	 pstrday = "Sun"; break;
		
			default 	:										break;
		}	
		
		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
		
		Graphics_setFont(&g_sContext, &g_sFontCmss26b);
		
		sprintf(pstr_lcd, "%#d-%#s-%#d %#s" , datatime.date, pstrmonth, datatime.year + 2000, pstrday);
																						Graphics_drawString(&g_sContext, (int8_t *)pstr_lcd,   20, 540, 2,  	TRANSPARENT_TEXT);
		if(Select_item_menu == DATA_SUBITEM)   {Graphics_drawString(&g_sContext, (int8_t *)pstr_lcd,   20, 450, 100,  TRANSPARENT_TEXT);}
		
		sprintf(pstr_lcd, "%#d:%#d:%#d" , datatime.hours, datatime.minutes, datatime.seconds);
																					 Graphics_drawString(&g_sContext, (int8_t *)pstr_lcd,   10, 540, 31,  	TRANSPARENT_TEXT);
		if(Select_item_menu == TIME_SUBITEM)	{Graphics_drawString(&g_sContext, (int8_t *)pstr_lcd,   10, 500, 100,  	TRANSPARENT_TEXT);}


		
		sprintf(pstr_lcd, "%#d:%#d:%#d" , AlarmTime.hours, AlarmTime.minutes, AlarmTime.seconds);																				
		if(Select_item_menu == ALARM_SUBITEM)	{Graphics_drawString(&g_sContext, (int8_t *)pstr_lcd,   10, 500, 100,  	TRANSPARENT_TEXT);}				
		
		/*	ADC value  */
		
		if(uwADCxConvertedVoltage != temp_uwADCxConvertedVoltage){					// if measurement voltage is not equal for previous value
		
		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_BLUE);			// set color for rect	
		Graphics_fillRectangle(&g_sContext, &Clear_Rect_izm);	
		Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
			
			temp_uwADCxConvertedVoltage = uwADCxConvertedVoltage;
			Graphics_setFont(&g_sContext, &g_sFontCmss26);
			sprintf(pstr_lcd, "Vcc %2.2fV" , (((float)uwADCxConvertedVoltage/(float)4095)*(float)3.3)*(float)1.72);																				
			Graphics_drawString(&g_sContext, (int8_t *)pstr_lcd,   10, 30, 420,  	TRANSPARENT_TEXT);			
			sprintf(pstr_lcd, "Ivcc %2.2fmA" , 100+((float)uwADCxConvertedVoltage/(float)4095)*(float)5.0);																				
			Graphics_drawString(&g_sContext, (int8_t *)pstr_lcd,   13, 30, 440,  	TRANSPARENT_TEXT);			
	  }
		
		Graphics_setFont(&g_sContext, &g_sFontCmss36b);
		
		TIMESET_BUSY = RESET;
}
void Delay_ms_RCC						(uint32_t ms)	{
        volatile uint32_t nCount;
        RCC_ClocksTypeDef RCC_Clocks;
    RCC_GetClocksFreq (&RCC_Clocks);

        nCount=(RCC_Clocks.HCLK_Frequency/10000)*ms;
        for (; nCount!=0; nCount--);
}
void edit_RTC								(uint8_t *param, uint8_t incdec){

	TIMESET_BUSY = SET;
	TM_RTC_GetDateTime(&datatime, TM_RTC_Format_BIN);//Graphics_drawString(&g_sContext, "edit_RTC",   20, 360, 400,  TRANSPARENT_TEXT);
	
		if(incdec == INC_PARAM_TIME)			(*param)++;
		if(incdec == DEC_PARAM_TIME)			(*param)--;
	
	TM_RTC_SetDateTime(&datatime, TM_RTC_Format_BIN);
}
